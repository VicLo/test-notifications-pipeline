package org.fundacion_jala.identity_service.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Permission;
import org.fundacion_jala.identity_service.utils.constants.EndPoint;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class PermissionControllerTest extends ControllerTest {

  @Test
  public void testGetAllPermissions_thenReturnJsonArrayObjectOfPermissions() throws Exception {
    List<Permission> permissions = new ArrayList<>();
    permissions.add(buildPermission());
    permissions.add(buildPermission());

    when(entityService.getAll()).thenReturn(permissions);

    mockMvc
        .perform(
            MockMvcRequestBuilders.get(EndPoint.PERMISSIONS_TEST)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$", hasSize(2)))
        .andExpect(jsonPath("$[0].id", is(permissions.get(0).getId().toString())))
        .andExpect(jsonPath("$[0].name", is(permissions.get(0).getName())))
        .andExpect(jsonPath("$[0].description", is(permissions.get(0).getDescription())))
        .andExpect(jsonPath("$[1].id", is(permissions.get(1).getId().toString())))
        .andExpect(jsonPath("$[1].name", is(permissions.get(1).getName())))
        .andExpect(jsonPath("$[1].description", is(permissions.get(1).getDescription())));
    verify(entityService, times(1)).getAll();
  }

  private Permission buildPermission() {
    UUID uuid = UUID.randomUUID();
    Permission permission = new Permission();
    permission.setId(uuid);
    permission.setName("Name");
    permission.setDescription("Description");
    return permission;
  }
}
