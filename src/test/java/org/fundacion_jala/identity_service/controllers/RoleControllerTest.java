package org.fundacion_jala.identity_service.controllers;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.models.composite_key.RolePermissionPK;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.utils.constants.EndPoint;
import org.fundacion_jala.identity_service.utils.messages.MessageConstant;
import org.fundacion_jala.identity_service.utils.requests.BulkEditRolesRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.hamcrest.core.IsNull;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

public class RoleControllerTest extends ControllerTest {

  public static final String QUERY_PAGE = "page";
  public static final String QUERY_SIZE = "size";
  public static final String QUERY_SORT = "sort";

  public static final int PAGE = 2;
  public static final int SIZE = 5;
  public static final String SORT = "name";

  public static final int DEFAULT_PAGE = 0;
  public static final int DEFAULT_SIZE = 10;
  public static final String DEFAULT_SORT = "name";

  public static final String BODY_ROLE =
      "{\n"
          + "    \"name\": \"admin\",\n"
          + "    \"description\": \"System Administrator\"\n"
          + "}";

  public static final String BODY_ROLE_NULL_NAME =
      "{\n" + "    \"name\": null,\n" + "    \"description\": \"system administrator\"\n" + "}";

  public static final String BODY_ROLE_NULL_DESCRIPTION =
      "{\n" + "    \"name\": \"admin\",\n" + "    \"description\": null\n" + "}";

  public static final String BODY_ROLE_WITH_ACTIONS =
      "{\n"
          + "    \"name\": \"Admin\",\n"
          + "    \"description\": \"System admin\",\n"
          + "    \"actions\": [\n"
          + "        {\n"
          + "            \"permissions\": {\n"
          + "                \"id\": \"9da2b848-fe2d-11eb-9a03-0242ac130003\"\n"
          + "            },\n"
          + "            \"canRead\": true,\n"
          + "            \"canCreate\": true,\n"
          + "            \"canEdit\": true,\n"
          + "            \"canDelete\": false\n"
          + "        }\n"
          + "    ]\n"
          + "}";

  public static final String BODY_ROLE_UPDATE_WITH_ACTIONS =
      "{\n"
          + "    \"id\": \"bb5da46f-36fd-4d79-aba0-43b4e241db63\",\n"
          + "    \"name\": \"Admin\",\n"
          + "    \"description\": \"description upd\",\n"
          + "    \"actions\": [\n"
          + "        {\n"
          + "            \"id\": {\n"
          + "                \"roleId\": \"bb5da46f-36fd-4d79-aba0-43b4e241db63\",\n"
          + "                \"permissionId\": \"9da313ce-fe2d-11eb-9a03-0242ac130003\"\n"
          + "            },\n"
          + "            \"canView\": false,\n"
          + "            \"canAdd\": false,\n"
          + "            \"canEdit\": false,\n"
          + "            \"canDelete\": false\n"
          + "        }\n"
          + "    ]\n"
          + "}";

  public static final String ROLE_UUID = "48f93b8a-fbb9-11eb-9a03-0242ac130003";
  public static final String PERMISSION_UUID = "9da2b848-fe2d-11eb-9a03-0242ac130003";
  public static final String URL_ROLES_EDIT_IN_BULK = EndPoint.ROLES + EndPoint.BULK;

  @Test
  public void testSave_thenReturnJsonObjectOfRole() throws Exception {
    Role roleSaved = buildRole(UUID.fromString(ROLE_UUID));

    when(entityService.save(any(Role.class))).thenReturn(roleSaved);
    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.ROLES)
                .content(BODY_ROLE)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(roleSaved.getId().toString())))
        .andExpect(jsonPath("$.name", is(roleSaved.getName())))
        .andExpect(jsonPath("$.description", is(roleSaved.getDescription())));
    ArgumentCaptor<Role> roleCaptor = ArgumentCaptor.forClass(Role.class);
    verify(entityService, times(1)).save(roleCaptor.capture());
  }

  @Test
  public void testSaveWithAlreadyExistingException_thenCheckNoIterations() {
    when(entityService.save(any(Role.class))).thenThrow(DataIntegrityViolationException.class);

    verifyNoInteractions(entityService);
    assertThrows(
        DataIntegrityViolationException.class,
        () -> entityService.save(buildRole(UUID.fromString(ROLE_UUID))));
  }

  @Test
  public void testSaveWithNullName_thenReturnBadRequest() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.ROLES)
                .content(BODY_ROLE_NULL_NAME)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());

    verifyNoInteractions(entityService);
  }

  @Test
  public void testSaveWithNullDescription_thenReturnBadRequest() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.ROLES)
                .content(BODY_ROLE_NULL_DESCRIPTION)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest());

    verifyNoInteractions(entityService);
  }

  @Test
  public void testSaveWithNullName_thenReturnBadRequestWithCustomResponse() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.ROLES)
                .content(BODY_ROLE_NULL_NAME)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.errors", hasSize(1)))
        .andExpect(jsonPath("$.errors[0].field", is("name")))
        .andExpect(jsonPath("$.errors[0].code", is("NotBlank")))
        .andExpect(jsonPath("$.errors[0].message", is("role name can not be empty")));
    verifyNoInteractions(entityService);
  }

  @Test
  public void testSaveWithNullDescription_thenReturnBadRequestWithCustomResponse()
      throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.ROLES)
                .content(BODY_ROLE_NULL_DESCRIPTION)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.errors", hasSize(1)))
        .andExpect(jsonPath("$.errors[0].field", is("description")))
        .andExpect(jsonPath("$.errors[0].code", is("NotBlank")))
        .andExpect(jsonPath("$.errors[0].message", is("role description can not be empty")));
    verifyNoInteractions(entityService);
  }

  @Test
  public void testSaveWithActionsList_thenReturnJsonObjectOfRole() throws Exception {
    Role roleSaved = buildRoleWithPermissions(UUID.fromString(ROLE_UUID));

    when(entityService.save(any(Role.class))).thenReturn(roleSaved);
    mockMvc
        .perform(
            MockMvcRequestBuilders.post(EndPoint.ROLES)
                .content(BODY_ROLE_WITH_ACTIONS)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id", is(roleSaved.getId().toString())))
        .andExpect(jsonPath("$.name", is(roleSaved.getName())))
        .andExpect(jsonPath("$.actions", hasSize(1)))
        .andExpect(jsonPath("$.description", is(roleSaved.getDescription())));
    ArgumentCaptor<Role> roleCaptor = ArgumentCaptor.forClass(Role.class);
    ArgumentCaptor<RolePermission> rolePermissionCaptor =
        ArgumentCaptor.forClass(RolePermission.class);
    verify(entityService, times(1)).save(roleCaptor.capture());
    verify(entityService, times(1)).save(rolePermissionCaptor.capture());
  }

  @Test
  public void testDeleteUserById_expectedSuccess() throws Exception {
    final UUID roleId = UUID.randomUUID();

    mockMvc
        .perform(
            MockMvcRequestBuilders.delete(EndPoint.ROLES.concat("/").concat(String.valueOf(roleId)))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void testUpdateRoleById_expectedSuccess() throws Exception {
    Role updateRole = buildRoleWithPermissions(UUID.fromString(ROLE_UUID));

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(
                    EndPoint.ROLES.concat("/").concat(String.valueOf(updateRole.getId())))
                .content(BODY_ROLE_WITH_ACTIONS)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());
  }

  @Test
  public void testUpdateRoleById_expectedMethodNotAllowed() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.put(EndPoint.ROLES)
                .content(BODY_ROLE_UPDATE_WITH_ACTIONS)
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isMethodNotAllowed());
  }

  @Test
  public void testGetAllRoles_expectedPaginationNullGetAllRolesAndSuccess() throws Exception {
    final int QUANTITY = 100;
    List<Role> rolesList = new ArrayList<>();
    for (int i = 0; i < QUANTITY; i++) {
      rolesList.add(new Role());
    }
    DataAndPagination dataAndPagination = DataAndPagination.builder().data(rolesList).build();
    when(entityService.getAllPagination(any(int.class), any(int.class), any(String.class)))
        .thenReturn(dataAndPagination);
    mockMvc
        .perform(MockMvcRequestBuilders.get(EndPoint.ROLES))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.data", hasSize(QUANTITY)))
        .andExpect(jsonPath("$.pagination", IsNull.nullValue()));
  }

  @Test
  public void testJsonDataResponse_expectedSuccess() throws Exception {
    List<Role> rolesList = buildRoleList();
    DataAndPagination dataAndPagination = DataAndPagination.builder().data(rolesList).build();
    when(entityService.getAllPagination(any(int.class), any(int.class), any(String.class)))
        .thenReturn(dataAndPagination);
    mockMvc
        .perform(MockMvcRequestBuilders.get(EndPoint.ROLES))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.data[0].name", is(rolesList.get(0).getName())))
        .andExpect(jsonPath("$.data[1].name", is(rolesList.get(1).getName())))
        .andExpect(jsonPath("$.data[2].name", is(rolesList.get(2).getName())))
        .andExpect(jsonPath("$.data[0].description", is(rolesList.get(0).getDescription())))
        .andExpect(jsonPath("$.data[1].description", is(rolesList.get(1).getDescription())))
        .andExpect(jsonPath("$.data[2].description", is(rolesList.get(2).getDescription())));
  }

  @Test
  public void testParams_expectedSuccessAndEqualToCapture() throws Exception {
    mockMvc
        .perform(
            MockMvcRequestBuilders.get(EndPoint.ROLES)
                .param(QUERY_PAGE, String.valueOf(PAGE))
                .param(QUERY_SIZE, String.valueOf(SIZE))
                .param(QUERY_SORT, SORT))
        .andExpect(status().isOk());

    ArgumentCaptor<Integer> pageCaptor = ArgumentCaptor.forClass(Integer.class);
    ArgumentCaptor<Integer> sizeCaptor = ArgumentCaptor.forClass(Integer.class);
    ArgumentCaptor<String> sortCaptor = ArgumentCaptor.forClass(String.class);
    verify(entityService)
        .getAllPagination(pageCaptor.capture(), sizeCaptor.capture(), sortCaptor.capture());
    assertEquals(PAGE, pageCaptor.getValue().intValue());
    assertEquals(SIZE, sizeCaptor.getValue().intValue());
    assertEquals(SORT, sortCaptor.getValue());
  }

  @Test
  public void testDefaultParams_expectedSuccessAndDefaultParams() throws Exception {
    mockMvc.perform(MockMvcRequestBuilders.get(EndPoint.ROLES)).andExpect(status().isOk());

    ArgumentCaptor<Integer> pageCaptor = ArgumentCaptor.forClass(Integer.class);
    ArgumentCaptor<Integer> sizeCaptor = ArgumentCaptor.forClass(Integer.class);
    ArgumentCaptor<String> sortCaptor = ArgumentCaptor.forClass(String.class);
    verify(entityService)
        .getAllPagination(pageCaptor.capture(), sizeCaptor.capture(), sortCaptor.capture());
    assertEquals(DEFAULT_SIZE, sizeCaptor.getValue().intValue());
    assertEquals(DEFAULT_SORT, sortCaptor.getValue());
  }

  @Test
  public void testBulkEditRole_thenReturnSuccess() throws Exception {
    BulkEditRolesRequest request = buildBulkEditRolesRequest();

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(URL_ROLES_EDIT_IN_BULK)
                .content(objectToJson(request))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk());

    ArgumentCaptor<BulkEditRolesRequest> bulkEditRolesCaptor =
        ArgumentCaptor.forClass(BulkEditRolesRequest.class);
    verify(bulkEditEntityService, times(1)).editInBulk(bulkEditRolesCaptor.capture());
  }

  @Test
  public void testBulkEditRole_thenReturnMessageSuccess() throws Exception {
    BasicResponse response = new BasicResponse();
    response.setMessage(MessageConstant.BULK_EDIT_ROLES_SUCCESS);
    BulkEditRolesRequest request = buildBulkEditRolesRequest();
    when(bulkEditEntityService.editInBulk(any(BulkEditRolesRequest.class))).thenReturn(response);

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(URL_ROLES_EDIT_IN_BULK)
                .content(objectToJson(request))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$.message", is(MessageConstant.BULK_EDIT_ROLES_SUCCESS)));
  }

  @Test
  public void testBulkEditRoleWithEmptyRolesId_thenReturnBadRequestWithCustomResponse()
      throws Exception {
    BasicResponse response = new BasicResponse();
    response.setMessage(MessageConstant.BULK_EDIT_ROLES_SUCCESS);
    BulkEditRolesRequest request = buildBulkEditRolesRequest();
    request.setIdsOfRoles(Collections.EMPTY_LIST);

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(URL_ROLES_EDIT_IN_BULK)
                .content(objectToJson(request))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.errors", hasSize(1)))
        .andExpect(jsonPath("$.errors[0].field", is("idsOfRoles")))
        .andExpect(jsonPath("$.errors[0].code", is("NotEmpty")))
        .andExpect(
            jsonPath("$.errors[0].message", is("The list identifiers of roles can not be empty")));

    verifyNoInteractions(bulkEditEntityService);
  }

  @Test
  public void testBulkEditRoleWithEmptyActions_thenReturnBadRequestWithCustomResponse()
      throws Exception {
    BasicResponse response = new BasicResponse();
    response.setMessage(MessageConstant.BULK_EDIT_ROLES_SUCCESS);
    BulkEditRolesRequest request = buildBulkEditRolesRequest();
    request.setActions(Collections.EMPTY_LIST);

    mockMvc
        .perform(
            MockMvcRequestBuilders.put(URL_ROLES_EDIT_IN_BULK)
                .content(objectToJson(request))
                .contentType(MediaType.APPLICATION_JSON))
        .andExpect(status().isBadRequest())
        .andExpect(jsonPath("$.errors", hasSize(1)))
        .andExpect(jsonPath("$.errors[0].field", is("actions")))
        .andExpect(jsonPath("$.errors[0].code", is("NotEmpty")))
        .andExpect(jsonPath("$.errors[0].message", is("Actions can not be empty")));

    verifyNoInteractions(bulkEditEntityService);
  }

  @Test
  public void testBulkEditRoleWithException_thenCheckNoIterations() {
    BasicResponse response = new BasicResponse();
    response.setMessage(MessageConstant.BULK_EDIT_ROLES_SUCCESS);
    BulkEditRolesRequest request = buildBulkEditRolesRequest();
    when(bulkEditEntityService.editInBulk(any(BulkEditRolesRequest.class)))
        .thenThrow(IllegalArgumentException.class);

    verifyNoInteractions(bulkEditEntityService);
    assertThrows(IllegalArgumentException.class, () -> bulkEditEntityService.editInBulk(request));
  }

  private Role buildRole(UUID id) {
    Role role = new Role();
    role.setId(id);
    role.setName("admin");
    role.setDescription("Administrator System");
    return role;
  }

  private Role buildRoleWithPermissions(UUID id) {
    Role role = new Role();
    role.setId(id);
    role.setName("admin");
    role.setDescription("Administrator System");

    RolePermissionPK rolePermissionPK = new RolePermissionPK(id, UUID.fromString(PERMISSION_UUID));

    RolePermission rolePermission = new RolePermission();
    rolePermission.setId(rolePermissionPK);

    rolePermission.setCanView(true);
    rolePermission.setCanAdd(true);
    rolePermission.setCanEdit(true);
    rolePermission.setCanDelete(false);

    Set<RolePermission> actions = new HashSet<>();
    actions.add(rolePermission);
    role.setActions(actions);

    return role;
  }

  private List<Role> buildRoleList() {
    Role role1 = new Role(UUID.fromString(ROLE_UUID));
    Role role2 = new Role(UUID.fromString(ROLE_UUID));
    Role role3 = new Role(UUID.fromString(ROLE_UUID));
    role1.setName("trainer");
    role2.setName("admin");
    role3.setName("scholar");
    role1.setDescription("Trainer Dev31");
    role2.setDescription("Administrator System");
    role3.setDescription("Scholar DEV31");
    return new ArrayList<>(Arrays.asList(role1, role2, role3));
  }

  private BulkEditRolesRequest buildBulkEditRolesRequest() {
    BulkEditRolesRequest request = new BulkEditRolesRequest();
    List<UUID> idsOfRoles = Arrays.asList(UUID.randomUUID(), UUID.randomUUID());
    RolePermission rolePermission = buildRolePermission();
    RolePermission rolePermission2 = buildRolePermission();
    List<RolePermission> actions = Arrays.asList(rolePermission, rolePermission2);
    request.setIdsOfRoles(idsOfRoles);
    request.setActions(actions);
    return request;
  }

  private RolePermission buildRolePermission() {
    RolePermissionPK rolePermissionPK = new RolePermissionPK(UUID.randomUUID(), UUID.randomUUID());
    RolePermission rolePermission = new RolePermission();
    rolePermission.setId(rolePermissionPK);
    rolePermission.setCanView(true);
    rolePermission.setCanAdd(true);
    rolePermission.setCanEdit(true);
    rolePermission.setCanDelete(false);
    return rolePermission;
  }
}
