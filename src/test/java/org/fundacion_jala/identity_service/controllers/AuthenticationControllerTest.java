package org.fundacion_jala.identity_service.controllers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.text.ParseException;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.fundacion_jala.identity_service.configurations.security.CustomUserDetail;
import org.fundacion_jala.identity_service.configurations.security.JwtUtils;
import org.fundacion_jala.identity_service.models.TokenPermissions;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.models.security.DenyUserToken;
import org.fundacion_jala.identity_service.services.DenyUserTokenService;
import org.fundacion_jala.identity_service.services.UserService;
import org.fundacion_jala.identity_service.utils.constants.JwtConstant;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.messages.MessageConstant;
import org.fundacion_jala.identity_service.utils.requests.LoginRequest;
import org.fundacion_jala.identity_service.utils.responses.AuthResponse;
import org.fundacion_jala.identity_service.utils.responses.ValidateTokenResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;

@ExtendWith(MockitoExtension.class)
public class AuthenticationControllerTest {

  private static final String TOKEN = "Token";

  @Mock private UserService service;

  @Mock private AuthenticationManager authenticationManager;

  @Mock private JwtUtils jwtUtils;

  @InjectMocks private AuthenticationController controller;

  @Mock private DenyUserTokenService denyUserTokenService;

  Authentication authentication = Mockito.mock(Authentication.class);

  private Date tomorrow;

  private Date yesterday;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.openMocks(this);
    Date today = new Date();
    tomorrow = DateUtils.addDays(today, 1);
    yesterday = DateUtils.addDays(today, -1);
  }

  @AfterEach
  public void clear() {
    Mockito.reset(service);
    Mockito.reset(authenticationManager);
    Mockito.reset(jwtUtils);
    Mockito.reset(authentication);
    Mockito.reset(denyUserTokenService);
  }

  @Test
  public void shouldReturnTokenWhenTheUserCredentialsAreValid() {
    CustomUserDetail detail = new CustomUserDetail(getMockUser());
    when(authentication.getPrincipal()).thenReturn(detail);
    when(authenticationManager.authenticate(Mockito.isA(Authentication.class)))
        .thenReturn(authentication);
    when(jwtUtils.generateJwtToken(any(Authentication.class))).thenReturn(TOKEN);
    ResponseEntity<AuthResponse> response = controller.login(getLoginData());
    assertEquals(TOKEN, response.getBody().getData().get(0));
    assertTrue(response.getBody().isSuccess());
  }

  @Test
  public void shouldThrowBadCredentialsExceptionWhenTheCredentialsAreNotValid() {
    CustomUserDetail detail = new CustomUserDetail(getMockUser());
    when(authentication.getPrincipal()).thenReturn(detail);
    when(authenticationManager.authenticate(Mockito.isA(Authentication.class)))
        .thenThrow(BadCredentialsException.class);
    assertThrows(BadCredentialsException.class, () -> controller.login(getLoginData()));
  }

  @Test
  public void shouldThrowNotFoundExceptionWhenTheCredentialsAreNotValid() {
    CustomUserDetail detail = new CustomUserDetail(getMockUser());
    when(authentication.getPrincipal()).thenReturn(detail);
    when(authenticationManager.authenticate(Mockito.isA(Authentication.class)))
        .thenThrow(EntityNotFoundException.class);
    assertThrows(EntityNotFoundException.class, () -> controller.login(getLoginData()));
  }

  @Test
  void shouldReturnSuccessOnLogoutWhenTheTokenIsValid() throws Exception {
    DenyUserToken denyUserToken = new DenyUserToken();
    denyUserToken.setExpirationTime(new Date());
    denyUserToken.setJwtId(UUID.randomUUID());
    denyUserToken.setTokenType(JwtConstant.ACCESS_TOKEN);
    HttpServletRequest request = mock(HttpServletRequest.class);

    when(request.getHeader(any(String.class))).thenReturn("");
    when(jwtUtils.parseJwt(any(String.class))).thenReturn("");
    when(jwtUtils.getExpClaim(any(String.class))).thenReturn(new Date());
    when(jwtUtils.getJtiClaim(any(String.class))).thenReturn(UUID.randomUUID());
    when(denyUserTokenService.save(any(DenyUserToken.class))).thenReturn(denyUserToken);

    ResponseEntity<AuthResponse> response = controller.logout(request);
    assertEquals(MessageConstant.LOGOUT_SUCCESS, response.getBody().getMessage());
    assertTrue(response.getBody().isSuccess());
  }

  @Test
  public void shouldReturnSuccessWhenTheTokenIsValid() {
    HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getHeader(any(String.class))).thenReturn(StringUtils.EMPTY);
    when(jwtUtils.parseJwt(any(String.class))).thenReturn(StringUtils.EMPTY);
    when(jwtUtils.getExpClaim(any(String.class))).thenReturn(tomorrow);
    when(jwtUtils.getPermissionsFromJwtToken(any(String.class))).thenReturn(new TokenPermissions());
    when(jwtUtils.getSubClaim(any(String.class))).thenReturn(UUID.randomUUID());
    when(jwtUtils.getClaimFromJwtToken(any(String.class), any(String.class)))
        .thenReturn(StringUtils.EMPTY);
    ResponseEntity<ValidateTokenResponse> response = controller.validateToken(request);
    assertTrue(response.getBody().isSuccess());
  }

  @Test
  public void shouldReturnSuccessFalseWhenTheTokenIsNotValid() throws ParseException {
    HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getHeader(any(String.class))).thenReturn(StringUtils.EMPTY);
    when(jwtUtils.parseJwt(any(String.class))).thenReturn(StringUtils.EMPTY);
    when(jwtUtils.getExpClaim(any(String.class))).thenReturn(yesterday);
    when(jwtUtils.getPermissionsFromJwtToken(any(String.class))).thenReturn(new TokenPermissions());
    when(jwtUtils.getSubClaim(any(String.class))).thenReturn(UUID.randomUUID());
    when(jwtUtils.getClaimFromJwtToken(any(String.class), any(String.class)))
        .thenReturn(StringUtils.EMPTY);
    ResponseEntity<ValidateTokenResponse> response = controller.validateToken(request);
    assertFalse(response.getBody().isSuccess());
  }

  private User getMockUser() {
    User base = new User();
    base.setEmail("test@email.com");
    base.setPassword("TestPassword");
    return base;
  }

  private LoginRequest getLoginData() {
    LoginRequest response = new LoginRequest();
    response.setEmail("test@email.com");
    response.setPassword("TestPassword");
    return response;
  }
}
