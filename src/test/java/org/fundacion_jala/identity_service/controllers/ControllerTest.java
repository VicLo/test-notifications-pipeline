package org.fundacion_jala.identity_service.controllers;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.fundacion_jala.identity_service.configurations.security.AuthEntryPointJwt;
import org.fundacion_jala.identity_service.configurations.security.CustomUserDetailService;
import org.fundacion_jala.identity_service.configurations.security.JwtUtils;
import org.fundacion_jala.identity_service.repositories.UserRepository;
import org.fundacion_jala.identity_service.services.BulkEditEntityService;
import org.fundacion_jala.identity_service.services.DenyUserTokenService;
import org.fundacion_jala.identity_service.services.EntityService;
import org.fundacion_jala.identity_service.services.UserServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest
@AutoConfigureMockMvc
@WithMockUser
public abstract class ControllerTest {

  @Autowired protected MockMvc mockMvc;

  @MockBean protected EntityService entityService;
  @MockBean protected BulkEditEntityService bulkEditEntityService;

  @MockBean protected CustomUserDetailService customUserDetailService;

  @MockBean protected AuthEntryPointJwt authEntryPointJwt;

  @MockBean protected JwtUtils jwtUtils;

  @MockBean protected UserRepository repository;

  @MockBean protected DenyUserTokenService denyUserTokenService;

  @MockBean protected UserServiceImpl userService;

  @MockBean protected UserController userController;

  protected final ObjectMapper MAPPER = new ObjectMapper();

  protected String objectToJson(Object data) throws JsonProcessingException {
    return MAPPER.writeValueAsString(data);
  }
}
