package org.fundacion_jala.identity_service.controllers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;

import com.diffplug.common.collect.Iterators;
import org.fundacion_jala.identity_service.configurations.security.JwtUtils;
import org.fundacion_jala.identity_service.file_management.UsersFileResponse;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.services.ResetPasswordService;
import org.fundacion_jala.identity_service.services.UserServiceImpl;
import org.fundacion_jala.identity_service.utils.UserListGenerator;
import org.fundacion_jala.identity_service.utils.constants.Defaults;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.exceptions.FieldEntityNotLocatedException;
import org.fundacion_jala.identity_service.utils.helpers.EntityBuilderHelper;
import org.fundacion_jala.identity_service.utils.requests.ChangeUserPasswordRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.fundacion_jala.identity_service.utils.responses.CheckEmailResponse;
import org.fundacion_jala.identity_service.utils.responses.GenericDataResponse;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.multipart.MultipartFile;

@ExtendWith(MockitoExtension.class)
public class UserControllerTest {

  public static final String CODE = "1D2S2S";
  public static final String MESSAGE_CHECK_TOKEN = "Token validate, ready to reset password";
  public static final String MESSAGE_RESET_PASSWORD = "User Updated";
  private static final String INVALID_EMAIL = "invalid_email@test.com";
  private final UsersFileResponse fileStatus =
      UsersFileResponse.builder()
          .message("")
          .error("")
          .data(Collections.EMPTY_LIST)
          .success(true)
          .build();

  private static final InputStream MOCK_INPUT =
      new ByteArrayInputStream(
          ("User,"
                  + "User,"
                  + "test.test@fundacion-jala.org,"
                  + "10000000,"
                  + "lp,"
                  + "7777777,"
                  + "La Paz,"
                  + "Staff")
              .getBytes());

  private static final String TOKEN = "Token";
  public static final String EMAIL = "email.test@fundacion-jala.org";
  public static final UUID USER_ID = UUID.randomUUID();
  public static final UUID ROLE_ID = UUID.randomUUID();
  public static final String USER_FIRST_NAME = "UserTestName";
  public static final String USER_LAST_NAME = "UserTestLastName";
  public static final long USER_PHONE_NUMBER = 123456789;
  public static final String USER_CURRENT_CITY = "TestCurrentCity";
  public static final long USER_CI = 987654321;
  public static final String USER_ISSUED = "TestIssued";
  public static final String ROLE_NAME = "TestRoleName";

  public static final int PAGE = 1;
  public static final int SIZE = 10;
  public static final String SORT = "firstName";

  MockMultipartFile file = new MockMultipartFile("test.csv", "test.csv", "xlsx/csv", MOCK_INPUT);

  List<MultipartFile> files = new ArrayList<>();

  @InjectMocks UserController userController;

  @Mock private UserServiceImpl userService;

  @Mock private ResetPasswordService resetPasswordService;

  @Mock private JwtUtils jwtUtils;

  @Mock private EntityBuilderHelper entityBuilderHelper;

  private User user;

  private final ResetPasswordUser resetPasswordUser;

  private final ChangeUserPasswordRequest changeUserPasswordRequest;

  public UserControllerTest() throws IOException {
    user = buildUser();
    resetPasswordUser = new ResetPasswordUser();
    resetPasswordUser.setUserId(user.getId());
    resetPasswordUser.setEmail(EMAIL);
    changeUserPasswordRequest = new ChangeUserPasswordRequest();
    changeUserPasswordRequest.setCode(CODE);
    changeUserPasswordRequest.setNewPassword("NewPassword123");
  }

  @BeforeEach
  public void setup() {
    MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  public void clear() {
    Mockito.reset(userService);
  }

  @Test
  public void testDefaultParams_expectedSuccessAndDefaultParams() throws Exception {
    assertEquals(
        200,
        userController
            .getAllUsers(
                Defaults.EMPTY,
                Integer.parseInt(Defaults.PAGE_NUMBER),
                Integer.parseInt(Defaults.PAGE_SIZE),
                Defaults.USER_SORT,
                Defaults.FILTER_FIELD,
                Defaults.EMPTY)
            .getStatusCodeValue());
    ArgumentCaptor<String> criteriaCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<Integer> pageNumberCaptor = ArgumentCaptor.forClass(Integer.class);
    ArgumentCaptor<Integer> pageSizeCaptor = ArgumentCaptor.forClass(Integer.class);
    ArgumentCaptor<String> sortCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<String> filterOptionCaptor = ArgumentCaptor.forClass(String.class);
    ArgumentCaptor<String> filterValueCaptor = ArgumentCaptor.forClass(String.class);
    verify(userService)
        .getAll(
            criteriaCaptor.capture(),
            pageNumberCaptor.capture(),
            pageSizeCaptor.capture(),
            sortCaptor.capture(),
            filterOptionCaptor.capture(),
            filterValueCaptor.capture());
    assertEquals(Defaults.EMPTY, criteriaCaptor.getValue());
    assertEquals(Integer.parseInt(Defaults.PAGE_NUMBER), pageNumberCaptor.getValue().intValue());
    assertEquals(Integer.parseInt(Defaults.PAGE_SIZE), pageSizeCaptor.getValue().intValue());
    assertEquals(Defaults.USER_SORT, sortCaptor.getValue());
    assertEquals(Defaults.FILTER_FIELD, filterOptionCaptor.getValue());
    assertEquals(Defaults.EMPTY, filterValueCaptor.getValue());
  }

  @Test
  public void testGetAllRoles_expectedPaginationNullGetAllRolesAndSuccess() throws Exception {
    final int QUANTITY = 100;
    List<User> usersList = new ArrayList<>();
    for (int i = 0; i < QUANTITY; i++) {
      usersList.add(new User());
    }
    DataAndPagination dataAndPagination = DataAndPagination.builder().data(usersList).build();
    when(userService.getAll(
            any(String.class),
            any(int.class),
            any(int.class),
            any(String.class),
            any(String.class),
            any(String.class)))
        .thenReturn(dataAndPagination);

    ResponseEntity<DataAndPagination> response =
        userController.getAllUsers(
            Defaults.EMPTY,
            Integer.parseInt(Defaults.PAGE_NUMBER),
            Integer.parseInt(Defaults.PAGE_SIZE),
            Defaults.USER_SORT,
            Defaults.FILTER_FIELD,
            Defaults.EMPTY);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertThat(response.getBody()).isEqualTo(dataAndPagination);
    assertThat(Objects.requireNonNull(response.getBody()).getData()).hasSize(QUANTITY);
  }

  @Test
  public void shouldReturnStatusCodeWhenFileIsUploaded() {
    MockHttpServletRequest request = new MockHttpServletRequest();
    RequestContextHolder.setRequestAttributes(new ServletRequestAttributes(request));
    when(userService.buildFiles(any(List.class))).thenReturn(fileStatus);
    files.add(file);
    assertEquals(200, userController.uploadFile(files).getStatusCodeValue());
  }

  @Test
  public void shouldReturnOkStatusCodeWhenSearchAndFilterWithDefaultProperties() {
    List<User> usersList = new ArrayList<>();
    DataAndPagination dataAndPagination = DataAndPagination.builder().data(usersList).build();
    when(userService.getAll(
            any(String.class),
            any(int.class),
            any(int.class),
            any(String.class),
            any(String.class),
            any(String.class)))
        .thenReturn(dataAndPagination);
    when(userService.getAll(
            any(String.class),
            any(Integer.class),
            any(Integer.class),
            any(String.class),
            any(String.class),
            any(String.class)))
        .thenReturn(dataAndPagination);
    ResponseEntity<DataAndPagination> response =
        userController.getAllUsers(
            Defaults.EMPTY,
            Integer.parseInt(Defaults.PAGE_NUMBER),
            Integer.parseInt(Defaults.PAGE_SIZE),
            Defaults.USER_SORT,
            Defaults.EMPTY,
            Defaults.EMPTY);
    assertEquals(response.getStatusCode(), HttpStatus.OK);
  }

  @Test
  public void shouldReturnCorrectNumberOfUsersWhenSendCustomSearch() {
    int userAmount = 100;
    String customSearch = "test";
    List<User> usersList = UserListGenerator.get(userAmount);

    DataAndPagination dataAndPagination = DataAndPagination.builder().data(usersList).build();
    when(userService.getAll(
            any(String.class),
            any(int.class),
            any(int.class),
            any(String.class),
            any(String.class),
            any(String.class)))
        .thenReturn(dataAndPagination);
    when(userService.getAll(
            any(String.class),
            any(Integer.class),
            any(Integer.class),
            any(String.class),
            any(String.class),
            any(String.class)))
        .thenReturn(dataAndPagination);
    ResponseEntity<DataAndPagination> response =
        userController.getAllUsers(
            customSearch,
            Integer.parseInt(Defaults.PAGE_NUMBER),
            Integer.parseInt(Defaults.PAGE_SIZE),
            Defaults.USER_SORT,
            Defaults.EMPTY,
            Defaults.EMPTY);
    assertEquals(Iterators.size(response.getBody().getData().iterator()), userAmount);
  }

  @Test
  public void shouldThrowFieldEntityNotLocatedExceptionWhenSendAnNonExistentFilterField() {
    String nonExistentFilterField = "roless";
    when(userService.getAll(
            any(String.class),
            any(Integer.class),
            any(Integer.class),
            any(String.class),
            any(String.class),
            any(String.class)))
        .thenThrow(FieldEntityNotLocatedException.class);
    assertThrows(
        FieldEntityNotLocatedException.class,
        () ->
            userController.getAllUsers(
                Defaults.EMPTY,
                Integer.parseInt(Defaults.PAGE_NUMBER),
                Integer.parseInt(Defaults.PAGE_SIZE),
                Defaults.USER_SORT,
                nonExistentFilterField,
                Defaults.EMPTY));
  }

  @Test
  public void testCheckToken_thenReturnSuccess() {
    BasicResponse body = new BasicResponse();
    body.setMessage(MESSAGE_CHECK_TOKEN);

    when(resetPasswordService.validateToken(
            any(String.class)))
            .thenReturn(body);

    ResponseEntity<BasicResponse> response = userController.validateToken(TOKEN);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertThat(response.getBody().getMessage()).isNotBlank();
  }

  @Test
  public void testCheckToken_thenReturnErrorInvalidToken() {
    when(resetPasswordService.validateToken(any(String.class)))
          .thenThrow(EntityNotFoundException.class);

    assertThrows(EntityNotFoundException.class, () -> userController.validateToken(TOKEN));
  }

  @Test
  public void testCheckUserByEmail_thenReturnToken() {
    when(resetPasswordService.verifyEmail(any(String.class)))
        .thenReturn(CheckEmailResponse.builder().token(TOKEN).build());

    ResponseEntity<CheckEmailResponse> response = userController.checkUserByEmail(EMAIL);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertEquals(TOKEN, response.getBody().getToken());
  }

  @Test
  public void testCheckUserByEmail_thenReturnEntityNotFoundException() {
    when(resetPasswordService.verifyEmail(any(String.class)))
        .thenThrow(EntityNotFoundException.class);

    assertThrows(EntityNotFoundException.class, () -> userController.checkUserByEmail(EMAIL));
  }

  @Test
  public void testChangeUserPassword_thenReturnSuccess() {
    BasicResponse body = new BasicResponse();
    body.setMessage(MESSAGE_RESET_PASSWORD);

    when(resetPasswordService.resetPassword(
            any(String.class), any(ChangeUserPasswordRequest.class)))
        .thenReturn(body);

    ResponseEntity<BasicResponse> response =
        userController.changeUserPassword(TOKEN, changeUserPasswordRequest);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertThat(response.getBody().getMessage()).isNotBlank();
  }

  @Test
  public void testUpdateUser_thenReturnSuccess() {
    when(userService.update(any(UUID.class), any(User.class))).thenReturn(user);

    ResponseEntity<User> response = userController.updateUser(user, USER_ID);
    assertEquals(HttpStatus.OK, response.getStatusCode());
    assertThat(response.getBody()).isEqualTo(user);
    assertThat(Objects.requireNonNull(response.getBody()).getRoles()).isNotEmpty();
  }

  @Test
  public void testUpdateInvalidUser_thenReturnEntityNotFoundException() {
    when(userService.update(any(UUID.class), any(User.class)))
        .thenThrow(EntityNotFoundException.class);

    assertThrows(
        EntityNotFoundException.class,
        () -> {
          userController.updateUser(user, USER_ID);
        });
  }

  @Test
  public void testShouldReturnUserSearchingByEmail() {
    when(userService.getByEmail(any(String.class))).thenReturn(buildUser());
    when(entityBuilderHelper.buildUserWithBasicData(any(User.class))).thenReturn(buildUser());
    ResponseEntity<User> response = (ResponseEntity<User>) userController.getUserByInput(EMAIL);

    assertEquals(200, response.getStatusCodeValue());
    assertEquals(USER_FIRST_NAME, response.getBody().getFirstName());
    assertEquals(USER_LAST_NAME, response.getBody().getLastName());
    assertEquals(USER_ID, response.getBody().getId());
    assertEquals(EMAIL, response.getBody().getEmail());
  }

  @Test
  public void testShouldReturnUserSearchingById() {
    when(userService.getById(any(UUID.class))).thenReturn(buildUser());
    when(entityBuilderHelper.buildUserWithBasicData(any(User.class))).thenReturn(buildUser());
    ResponseEntity<User> response =
        (ResponseEntity<User>) userController.getUserByInput(USER_ID.toString());

    assertEquals(200, response.getStatusCodeValue());
    assertEquals(USER_FIRST_NAME, response.getBody().getFirstName());
    assertEquals(USER_LAST_NAME, response.getBody().getLastName());
    assertEquals(USER_ID, response.getBody().getId());
    assertEquals(EMAIL, response.getBody().getEmail());
  }

  @Test
  public void testShouldReturnNotFoundWhenInvalidInputEntered() {
    when(userService.getByEmail(any(String.class))).thenThrow(EntityNotFoundException.class);
    when(userService.getById(any(UUID.class))).thenThrow(EntityNotFoundException.class);

    assertThrows(
        EntityNotFoundException.class,
        () -> {
          userController.getUserByInput(INVALID_EMAIL);
        });
  }

  @Test
  public void testShouldReturnUserDataWithProfileEndpoint() {
    HttpServletRequest request = mock(HttpServletRequest.class);
    when(request.getHeader(any(String.class))).thenReturn("");
    when(jwtUtils.parseJwt(any(String.class))).thenReturn(TOKEN);
    when(jwtUtils.getUserNameFromJwtToken(any(String.class))).thenReturn(EMAIL);
    when(userService.getByEmail(any(String.class))).thenReturn(buildUser());

    ResponseEntity<GenericDataResponse> response = userController.getUserProfile(request);

    assertEquals(User.class, response.getBody().data.getClass());
    assertEquals("Success", response.getBody().message);
    assertNull(response.getBody().error);
    assertTrue(response.getBody().success);
  }

  private Role buildRole() {
    Role role = new Role();
    role.setName(ROLE_NAME);
    role.setDeleted(false);
    role.setId(ROLE_ID);
    return role;
  }

  private User buildUser() {
    user = new User();
    user.setId(USER_ID);
    user.setEmail(EMAIL);
    user.setFirstName(USER_FIRST_NAME);
    user.setLastName(USER_LAST_NAME);
    user.setPhoneNumber(USER_PHONE_NUMBER);
    user.setCurrentCity(USER_CURRENT_CITY);
    user.setCi(USER_CI);
    user.setIssued(USER_ISSUED);
    Role role = buildRole();
    Set<Role> roles = new HashSet<Role>();
    roles.add(role);
    user.setRoles(roles);
    return user;
  }
}
