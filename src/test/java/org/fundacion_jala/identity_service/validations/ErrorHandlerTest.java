package org.fundacion_jala.identity_service.validations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.Objects;
import org.fundacion_jala.identity_service.utils.responses.FieldErrorValidationModel;
import org.fundacion_jala.identity_service.utils.responses.ValidateTokenResponse;
import org.fundacion_jala.identity_service.utils.responses.ValidationModelErrorResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.MethodParameter;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

@ExtendWith(MockitoExtension.class)
public class ErrorHandlerTest {

  private static final String EXCEPTION_ERROR_400 = "400";
  private static final String DUPLICATE_KEY_EXCEPTION =
      "Duplicate entry '%s' for key 'roles.UKofx66keruapi6vyqpv6f2or37'";

  @InjectMocks private ErrorHandler errorHandler;

  @Test
  public void testHandleMethodArgumentNotValid_thenReturnBadRequestWithListFieldErrors()
      throws NoSuchMethodException {
    MethodParameter parameter =
        new MethodParameter(
            errorHandler
                .getClass()
                .getMethod("handleMethodArgumentNotValid", MethodArgumentNotValidException.class),
            0);
    BindingResult bindingResult = mock(BindingResult.class);
    MethodArgumentNotValidException exception =
        new MethodArgumentNotValidException(parameter, bindingResult);
    FieldError fieldError = new FieldError("test", "name", "should not be empty");
    when(bindingResult.getAllErrors()).thenReturn(Arrays.asList(fieldError));

    ResponseEntity<ValidationModelErrorResponse> errorResponse =
        errorHandler.handleMethodArgumentNotValid(exception);

    ValidationModelErrorResponse response = errorResponse.getBody();
    assertEquals(errorResponse.getStatusCode(), HttpStatus.BAD_REQUEST);
    assertEquals(response.getErrors().size(), 1);
    assertNotNull(response.getMessage());
    assertEquals(response.getErrors().get(0).getCode(), fieldError.getCode());
    assertEquals(response.getErrors().get(0).getField(), fieldError.getField());
    assertEquals(response.getErrors().get(0).getMessage(), fieldError.getDefaultMessage());
  }

  @Test
  public void testHandleMethodAlreadyExistException_thenReturnBadRequestWithListFieldErrors() {
    String value = "Trainer";

    String exceptionMessage = String.format(DUPLICATE_KEY_EXCEPTION, value);

    Throwable throwable = new Throwable(new Throwable(exceptionMessage));

    DataIntegrityViolationException exception =
        new DataIntegrityViolationException(exceptionMessage, throwable);

    ResponseEntity<FieldErrorValidationModel> errorResponse =
        errorHandler.handleDuplicateKeyException(exception);

    assertEquals(exceptionMessage, Objects.requireNonNull(errorResponse.getBody()).getMessage());
    assertEquals(EXCEPTION_ERROR_400, Objects.requireNonNull(errorResponse.getBody()).getCode());
    assertEquals(exceptionMessage, Objects.requireNonNull(errorResponse.getBody()).getMessage());
  }

  @Test
  public void testHandleIllegalArgumentException() {
    String message = "test message";
    Throwable throwable = new Throwable(new Throwable(message));
    IllegalArgumentException exception = new IllegalArgumentException(message, throwable);
    ResponseEntity<ValidateTokenResponse> errorResponse =
        errorHandler.handleIllegalArgumentException(exception);
    assertFalse(errorResponse.getBody().isSuccess());
    assertEquals(HttpStatus.BAD_REQUEST.value(), errorResponse.getStatusCodeValue());
  }
}
