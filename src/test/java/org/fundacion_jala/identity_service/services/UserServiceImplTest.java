package org.fundacion_jala.identity_service.services;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import org.fundacion_jala.identity_service.file_management.UsersFileData;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.repositories.UserRepository;
import org.fundacion_jala.identity_service.utils.constants.UserFileMessages;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.exceptions.InvalidUserPasswordFormatException;
import org.fundacion_jala.identity_service.utils.helpers.BodyGeneratorHelper;
import org.fundacion_jala.identity_service.utils.helpers.CSVHelper;
import org.fundacion_jala.identity_service.utils.helpers.EntityBuilderHelper;
import org.fundacion_jala.identity_service.utils.validations.UserValidationVerifier;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTest {

  private static final String MOCK_EMAIL = "test.test@fundacion-jala.org";
  private static final String MOCK_PASSWORD = "Password@12345";
  private static final String MOCK_FIRST_NAME = "testUserFirstName";
  private static final String MOCK_LAST_NAME = "testUserLastName";
  private static final long MOCK_CI = 12345;
  private static final String MOCK_ISSUED = "testUserLastName";
  private static final long MOCK_PHONE_NUMBER = 54321;
  private static final String MOCK_CURRENT_CITY = "testUserLastName";
  private static final String MOCK_ROLE_NAME = "testUserLastName";
  public static final String UUID1 = "4967e970-3f64-4308-85a3-d67b2b22f5f1";

  @Mock private CSVHelper csvHelper;

  @Mock private RestTemplate restTemplate;

  @Mock private BodyGeneratorHelper bodyGeneratorHelper;

  @Mock private UserValidationVerifier userValidationVerifier;

  private static final User mockUser =
      new User(
          "User",
          "User",
          "test.test@fundacion-jala.org",
          10000000,
          "lp",
          7777777,
          "La Paz",
          "Bolivia",
          buildRoles());

  private static final InputStream mockInput =
      new ByteArrayInputStream(
          ("firstName,lastName,email,ci,issued,phoneNumber,currentCity,roles\n"
                  + "User,"
                  + "User,"
                  + "test.test@fundacion-jala.org,"
                  + "10000000,"
                  + "lp,"
                  + "7777777,"
                  + "La Paz,"
                  + "Trainer")
              .getBytes());

  @Mock private UserRepository repository;

  @Mock private PasswordEncoder passwordEncoder;

  @Mock private EntityService<Role> roleService;

  @InjectMocks private UserServiceImpl userService;

  @Mock private EntityBuilderHelper entityBuilderHelper;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.openMocks(this);
  }

  @AfterEach
  public void clear() {
    Mockito.reset(repository);
  }

  @Test
  public void shouldReturnUserFoundWhenEmailEnteredExists() {
    User mockUser = new User();
    mockUser.setEmail(MOCK_EMAIL);
    mockUser.setPassword(MOCK_PASSWORD);
    when(repository.findByEmail(any(String.class))).thenReturn(mockUser);
    when(userValidationVerifier.validateEmail(any(String.class))).thenReturn(true);
    User response = userService.getByEmail(MOCK_EMAIL);
    assertNotNull(response);
    assertEquals(MOCK_EMAIL, response.getEmail());
  }

  @Test
  public void shouldReturnNullWhenEmailEnteredNotExists() {
    when(repository.findByEmail(any(String.class))).thenReturn(null);
    assertThrows(EntityNotFoundException.class, () -> userService.getByEmail(MOCK_EMAIL));
  }

  @Test
  public void shouldReturnUserFoundWhenIdEnteredExists() {
    User mockUser = new User();
    mockUser.setId(UUID.fromString(UUID1));
    mockUser.setEmail(MOCK_EMAIL);
    when(repository.findById(any(UUID.class))).thenReturn(Optional.of(mockUser));
    User response = userService.getById(UUID.fromString(UUID1));
    assertNotNull(response);
    assertEquals(MOCK_EMAIL, response.getEmail());
  }

  @Test
  public void shouldReturnNullWhenIdEnteredNotExists() {
    when(repository.findById(any(UUID.class))).thenReturn(Optional.empty());
    assertThrows(EntityNotFoundException.class, () -> userService.getById(UUID.fromString(UUID1)));
  }

  @Test
  public void shouldReturnUpdatedUser() {
    when(userValidationVerifier.validateUniqueRole(any())).thenReturn(true);
    User mockUser = new User();
    mockUser.setId(UUID.fromString(UUID1));
    mockUser.setEmail(MOCK_FIRST_NAME);
    mockUser.setEmail(MOCK_LAST_NAME);
    mockUser.setCi(MOCK_CI);
    mockUser.setIssued(MOCK_ISSUED);
    mockUser.setPhoneNumber(MOCK_PHONE_NUMBER);
    mockUser.setCurrentCity(MOCK_CURRENT_CITY);
    Set<Role> mockRoles = new HashSet<>();
    Role admin = new Role();
    admin.setName(MOCK_ROLE_NAME);
    mockRoles.add(admin);
    mockUser.setRoles(mockRoles);
    Optional<User> mockOptionalUser = Optional.of(mockUser);
    when(repository.save(any(User.class))).thenReturn(mockUser);
    when(repository.findById(any(UUID.class))).thenReturn(mockOptionalUser);
    User result = userService.update(UUID.fromString(UUID1), mockUser);
    assertEquals(mockUser.getFirstName(), result.getFirstName());
    assertEquals(mockUser.getLastName(), result.getLastName());
    assertEquals(mockUser.getEmail(), result.getEmail());
    assertEquals(mockUser.getCi(), result.getCi());
    assertEquals(mockUser.getIssued(), result.getIssued());
    assertEquals(mockUser.getPhoneNumber(), result.getPhoneNumber());
    assertEquals(mockUser.getCurrentCity(), result.getCurrentCity());
    assertEquals(mockUser.getRoles(), result.getRoles());
  }

  public void shouldReturnMessageWhenChangePassword() {
    Optional<User> userMock = Optional.of(new User());

    when(repository.findById(any(UUID.class))).thenReturn(userMock);
    when(passwordEncoder.encode(any(String.class))).thenReturn(MOCK_PASSWORD);
    when(repository.save(any(User.class))).thenReturn(new User());

    String response = userService.updatePassword(UUID.randomUUID(), MOCK_PASSWORD);
    assertThat(response).isNotBlank();
  }

  @Test
  public void shouldReturnEntityNotFoundExceptionWhenChangePassword() {
    Optional<User> userMock = Optional.empty();

    when(repository.findById(any(UUID.class))).thenReturn(userMock);

    assertThrows(
        EntityNotFoundException.class,
        () -> userService.updatePassword(UUID.randomUUID(), MOCK_PASSWORD));
  }

  @Test
  public void shouldReturnInvalidPasswordFormatExceptionWhenChangePassword() {
    Optional<User> userMock = Optional.empty();

    when(repository.findById(any(UUID.class))).thenReturn(userMock);

    assertThrows(
        InvalidUserPasswordFormatException.class,
        () -> userService.updatePassword(UUID.randomUUID(), "password"));
  }

  @Test
  public void shouldReturnValidFileStatusWhenImportUserAndSendEmail() throws IOException {
    List<User> users = new LinkedList<>();
    mockUser.setRoleName(MOCK_ROLE_NAME);
    users.add(mockUser);

    User userId = mockUser;
    userId.setId(UUID.randomUUID());

    MockMultipartFile file = new MockMultipartFile("test.csv", "test.csv", "xlsx/csv", mockInput);
    when(csvHelper.csvToUsers(any(InputStream.class))).thenReturn(users);

    when(repository.save(any(User.class))).thenReturn(userId);
    when(repository.existsById(any(UUID.class))).thenReturn(true);

    when(userValidationVerifier.validateUser(any(User.class))).thenReturn(true);

    when(roleService.getByName(any(String.class))).thenReturn(new Role());

    when(bodyGeneratorHelper.generateBodyEmailDefaultPassword(
            any(String.class), any(String[].class)))
        .thenReturn(new HashMap<>());

    when(restTemplate.postForEntity(any(String.class), any(Map.class), any()))
        .thenReturn(ResponseEntity.accepted().build());

    UsersFileData fileStatus = userService.importUsersByFile(file);

    assertNotNull(fileStatus);
    assertThat(fileStatus.getStatus()).isEqualTo(UserFileMessages.STATUS_SUCCESS);
    assertThat(fileStatus.getDataErrors()).isEmpty();
    assertThat(fileStatus.getValidData().size()).isEqualTo(1);
  }

  private static Set<Role> buildRoles() {
    Set<Role> roles = new HashSet<>();
    String uuid = "123e4567-e89b-12d3-a456-556642440000";
    Role role = new Role();
    role.setName("Trainer");
    role.setDeleted(false);
    role.setId(UUID.fromString(uuid));
    roles.add(role);
    return roles;
  }
}
