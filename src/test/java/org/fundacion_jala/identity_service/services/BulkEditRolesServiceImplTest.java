package org.fundacion_jala.identity_service.services;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Collection;
import java.util.Collections;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Permission;
import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.repositories.PermissionRepository;
import org.fundacion_jala.identity_service.repositories.RolePermissionRepository;
import org.fundacion_jala.identity_service.repositories.RoleRepository;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.requests.BulkEditRolesRequest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class BulkEditRolesServiceImplTest {

  @Mock private RoleRepository roleRepository;

  @Mock private PermissionRepository permissionRepository;

  @Mock private RolePermissionRepository rolePermissionRepository;

  @InjectMocks private BulkEditRolesServiceImpl bulkEditRolesService;

  private BulkEditRolesRequest request;

  @BeforeEach
  public void setup() {
    Permission permission = new Permission();
    permission.setId(UUID.randomUUID());
    request = new BulkEditRolesRequest();
    RolePermission rolePermission = new RolePermission();
    rolePermission.setPermissions(permission);
    request.setIdsOfRoles(Collections.singletonList((UUID.randomUUID())));
    request.setActions(Collections.singletonList(rolePermission));
  }

  @Test
  public void testBulkEditRoleWithRoleIdNonexistent_thenReturnEntityNotFoundException() {
    when(roleRepository.existsById(any(UUID.class))).thenReturn(false);

    assertThrows(EntityNotFoundException.class, () -> bulkEditRolesService.editInBulk(request));
    verify(roleRepository).existsById(any(UUID.class));
  }

  @Test
  public void testBulkEditRoleWithPermissionIdNonexistent_thenReturnEntityNotFoundException() {
    when(roleRepository.existsById(any(UUID.class))).thenReturn(true);
    when(permissionRepository.existsById(any(UUID.class))).thenReturn(false);

    assertThrows(EntityNotFoundException.class, () -> bulkEditRolesService.editInBulk(request));
    verify(permissionRepository).existsById(any(UUID.class));
  }

  @Test
  public void testBulkEditRoleWithPermissionIdNonexistent_thenReturnSqlException() {
    when(roleRepository.existsById(any(UUID.class))).thenReturn(true);
    when(permissionRepository.existsById(any(UUID.class))).thenReturn(true);
    when(rolePermissionRepository.saveAll(any(Collection.class)))
        .thenThrow(IllegalArgumentException.class);

    assertThrows(IllegalArgumentException.class, () -> bulkEditRolesService.editInBulk(request));
    verify(rolePermissionRepository).saveAll(any(Collection.class));
  }
}
