package org.fundacion_jala.identity_service.services;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.UUID;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.exceptions.InvalidTokenException;
import org.fundacion_jala.identity_service.utils.exceptions.ResetPasswordCodeUsedException;
import org.fundacion_jala.identity_service.utils.exceptions.SendResetPasswordCodeEmailServerException;
import org.fundacion_jala.identity_service.utils.exceptions.ValidationCodeIncorrectException;
import org.fundacion_jala.identity_service.utils.helpers.BodyGeneratorHelper;
import org.fundacion_jala.identity_service.utils.helpers.EntityBuilderHelper;
import org.fundacion_jala.identity_service.utils.requests.ChangeUserPasswordRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.fundacion_jala.identity_service.utils.responses.CheckEmailResponse;
import org.junit.Ignore;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.web.client.RestTemplate;

@ExtendWith(MockitoExtension.class)
class ResetPasswordServiceImplTest {
  private static final String EMAIL = "example@fundacion-jala.org";
  private static final String CODE = "1D2S2S";
  private static final String TOKEN = "Token";

  @Mock private RestTemplate restTemplate;

  @Mock private BodyGeneratorHelper bodyGeneratorHelper;

  @Mock private ResetPasswordUserService resetPasswordUserService;

  @Mock private UserServiceImpl userService;

  @Mock private EntityBuilderHelper entityBuilderHelper;

  @Mock private TokenService tokenService;

  @InjectMocks private ResetPasswordServiceImpl resetPasswordService;

  private User user;

  private ResetPasswordUser resetPasswordUser;

  private ChangeUserPasswordRequest changeUserPasswordRequest;

  @BeforeEach
  public void setupTest() {
    MockitoAnnotations.openMocks(this);
    user = new User();
    user.setId(UUID.randomUUID());
    user.setEmail(EMAIL);
    resetPasswordUser = new ResetPasswordUser();
    resetPasswordUser.setUserId(user.getId());
    resetPasswordUser.setEmail(EMAIL);
    changeUserPasswordRequest = new ChangeUserPasswordRequest();
    changeUserPasswordRequest.setCode(CODE);
    changeUserPasswordRequest.setNewPassword("NewPassword123");
  }

  @Test
  void testValidateToken_thenBasicResponseObject() {
    BasicResponse basicResponse = new BasicResponse();

    when(resetPasswordUserService.getByToken(any(String.class))).thenReturn(resetPasswordUser);
    when(tokenService.isValidToken(any(String.class), any(ResetPasswordUser.class)))
            .thenReturn(true);
    when(entityBuilderHelper.buildBasicResponse(any(String.class))).thenReturn(basicResponse);

    BasicResponse response = resetPasswordService.validateToken(TOKEN);
    assertNotNull(response);
  }

  @Test
  void testValidateToken_thenThrowInvalidTokenException() {
    when(resetPasswordUserService.getByToken(any(String.class))).thenReturn(resetPasswordUser);
    when(tokenService.isValidToken(any(String.class), any(ResetPasswordUser.class))).thenThrow(InvalidTokenException.class);

    assertThrows(InvalidTokenException.class, () -> resetPasswordService.validateToken(TOKEN));
  }

  @Test
  void testValidateToken_usedCode_thenThrowResetPasswordCodeUsedException() {
    when(resetPasswordUserService.getByToken(any(String.class))).thenReturn(resetPasswordUser);
    resetPasswordUser.setUsed(true);
    when(tokenService.isValidToken(any(String.class), any(ResetPasswordUser.class)))
            .thenReturn(true);
    assertThrows(ResetPasswordCodeUsedException.class, () -> resetPasswordService.validateToken(TOKEN));
  }

  @Test
  void testVerifyEmail_thenReturnEntityNotFoundException() {
    when(userService.getByEmail(any(String.class))).thenThrow(EntityNotFoundException.class);

    assertThrows(EntityNotFoundException.class, () -> resetPasswordService.verifyEmail(EMAIL));
  }

  /**
   * To Do: This test should be re-written now that Kafka broker communicates to
   * Notifications Service
   */
/*  @Ignore
  @Test
  void testVerifyEmail_thenReturnMicroserviceConnectionException() {
    when(userService.getByEmail(any(String.class))).thenReturn(user);
    when(entityBuilderHelper.buildResetPasswordUser(any(User.class))).thenReturn(resetPasswordUser);
    when(restTemplate.postForEntity(any(String.class), any(Object.class), any()))
        .thenThrow(IllegalStateException.class);

    assertThrows(
        SendResetPasswordCodeEmailServerException.class,
        () -> resetPasswordService.verifyEmail(EMAIL));
  }*/

  /**
   * To Do: This test should be re-written now that Kafka broker communicates to
   * Notifications Service
   */
/*  @Ignore
  @Test
  public void testVerifyEmail_thenReturnCheckEmailResponseObject() {
    ResetPasswordUser resetPasswordUserSaved = resetPasswordUser;
    resetPasswordUserSaved.setId(UUID.randomUUID());
    CheckEmailResponse expectedResponse =
        CheckEmailResponse.builder().token(resetPasswordUser.getToken()).build();

    when(userService.getByEmail(any(String.class))).thenReturn(user);
    when(entityBuilderHelper.buildResetPasswordUser(any(User.class))).thenReturn(resetPasswordUser);
    when(resetPasswordUserService.save(any(ResetPasswordUser.class)))
        .thenReturn(resetPasswordUserSaved);

    assertEquals(expectedResponse.getToken(), resetPasswordService.verifyEmail(EMAIL).getToken());
  }*/

  @Test
  void testResetPassword_thenReturnBasicResponseObject() {
    resetPasswordUser.setCode(CODE);
    BasicResponse basicResponse = new BasicResponse();

    when(resetPasswordUserService.getByToken(any(String.class))).thenReturn(resetPasswordUser);
    when(tokenService.isValidToken(any(String.class), any(ResetPasswordUser.class)))
        .thenReturn(true);
    when(userService.updatePassword(any(UUID.class), any(String.class))).thenReturn("");
    when(entityBuilderHelper.buildBasicResponse(any(String.class))).thenReturn(basicResponse);

    BasicResponse response = resetPasswordService.resetPassword(TOKEN, changeUserPasswordRequest);
    assertNotNull(response);
  }

  @Test
  public void testResetPassword_thenReturnInvalidTokenException() {
    when(resetPasswordUserService.getByToken(any(String.class))).thenReturn(null);

    assertThrows(
        InvalidTokenException.class,
        () -> resetPasswordService.resetPassword(TOKEN, changeUserPasswordRequest));
  }

  @Test
  public void testChangeUserPassword_thenReturnValidationCodeIncorrectException() {
    when(resetPasswordUserService.getByToken(any(String.class))).thenReturn(resetPasswordUser);
    when(tokenService.isValidToken(any(String.class), any(ResetPasswordUser.class)))
        .thenReturn(true);

    assertThrows(
        ValidationCodeIncorrectException.class,
        () -> resetPasswordService.resetPassword(TOKEN, changeUserPasswordRequest));
  }

  @Test
  public void testChangeUserPassword_thenReturnResetPasswordCodeUsedException() {
    resetPasswordUser.setCode(CODE);
    resetPasswordUser.setUsed(true);

    when(resetPasswordUserService.getByToken(any(String.class))).thenReturn(resetPasswordUser);
    when(tokenService.isValidToken(any(String.class), any(ResetPasswordUser.class)))
        .thenReturn(true);

    assertThrows(
        ResetPasswordCodeUsedException.class,
        () -> resetPasswordService.resetPassword(TOKEN, changeUserPasswordRequest));
  }
}
