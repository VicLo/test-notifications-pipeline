package org.fundacion_jala.identity_service.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.pagination.Pagination;
import org.fundacion_jala.identity_service.repositories.RoleRepository;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@ExtendWith(MockitoExtension.class)
@Transactional(propagation = Propagation.NOT_SUPPORTED)
public class RoleServiceImplTest {

  private final String GET_ROLE_BY_NAME_ERROR = "ROLE with role_name: %s not found";

  private final String GET_ROLE_BY_ID_ERROR = "ROLE with id:%s not found";

  @Mock RoleRepository roleRepository;

  @InjectMocks RoleServiceImpl roleService;

  @Test
  public void testShouldResponseCorrectDelete() {
    when(roleRepository.existsById(any(UUID.class))).thenReturn(true);
    roleService.delete(UUID.randomUUID());
    verify(roleRepository).deleteById(any(UUID.class));
  }

  @Test
  public void testShouldResponseCorrectWithSoftDelete() {
    Role role = buildRole();
    UUID roleUUID = role.getId();
    List<Role> roles = new ArrayList<>();
    role.setDeleted(true);
    roles.add(role);
    when(roleRepository.existsById(any(UUID.class))).thenReturn(true);
    when(roleRepository.findByDeleted(any(boolean.class))).thenReturn(roles);

    roleService.delete(roleUUID);
    List<Role> deletedRoles = roleRepository.findByDeleted(true);

    assertThrows(
        EntityNotFoundException.class,
        () -> {
          roleService.getById(roleUUID);
        });
    assertEquals(deletedRoles.get(0).getId(), roleUUID);
    assertTrue(deletedRoles.get(0).isDeleted());
  }

  @Test
  public void testShouldThrowAnExceptionAndReturnAMessage() {
    UUID uuid = UUID.randomUUID();
    when(roleRepository.existsById(any(UUID.class))).thenReturn(false);
    Exception exception =
        assertThrows(
            EntityNotFoundException.class,
            () -> {
              roleService.delete(uuid);
            });
    assertEquals(String.format(GET_ROLE_BY_ID_ERROR, uuid), exception.getMessage());
  }

  @Test
  public void testShouldReturnAllRolesList() {
    List<Role> rolesList = new ArrayList<>();
    rolesList.addAll(Arrays.asList(new Role(), new Role(), new Role()));
    when(roleRepository.findAll()).thenReturn(rolesList);
    Iterable<Role> roles = roleService.getAll();
    assertEquals(rolesList, roles);
  }

  @Test
  public void testShouldReturnARoleByName() {
    Role role = buildRole();

    when(roleRepository.existsByName(any(String.class))).thenReturn(true);
    when(roleRepository.findRoleByName(any(String.class))).thenReturn(role);
    Role result = roleService.getByName(role.getName());

    verify(roleRepository, times(1)).findRoleByName(any(String.class));
    assertEquals(role, result);
  }

  @Test
  public void testShouldThrowAnExceptionWhenGetRoleByUnknownNameAndReturnCustomResponse() {
    Role role = buildRole();

    when(roleRepository.existsByName(any(String.class))).thenReturn(false);

    Exception exception =
        assertThrows(
            EntityNotFoundException.class,
            () -> {
              roleService.getByName(role.getName());
            });
    assertEquals(String.format(GET_ROLE_BY_NAME_ERROR, role.getName()), exception.getMessage());

    verify(roleRepository, times(0)).findRoleByName(any(String.class));
  }

  @Test
  public void testShouldReturnRolesListUsingPagination() {
    final int PAGE = 1;
    final int SIZE = 3;
    final String SORT = "Name";
    Pagination pagination = Pagination.builder().currentPage(PAGE).pageSize(SIZE).build();
    DataAndPagination expected = DataAndPagination.builder().pagination(pagination).build();
    Page<Role> page = (Page<Role>) mock(Page.class);
    when(roleRepository.findByDeleted(
            ArgumentMatchers.isA(Pageable.class), ArgumentMatchers.isA(Boolean.class)))
        .thenReturn(page);
    DataAndPagination actual = roleService.getAllPagination(PAGE, SIZE, SORT);
    assertEquals(expected.getPagination(), actual.getPagination());
    assertEquals(actual.getPagination().getPageSize(), SIZE);
    assertEquals(actual.getPagination().getCurrentPage(), PAGE);
  }

  private Role buildRole() {
    String uuid = "123e4567-e89b-12d3-a456-556642440000";
    Role role = new Role();
    role.setName("Staff");
    role.setDeleted(false);
    role.setId(UUID.fromString(uuid));
    return role;
  }
}
