package org.fundacion_jala.identity_service.services;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.UUID;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ResetPasswordTokenServiceTest {

  private ResetPasswordTokenService resetPasswordTokenHelper =
      new ResetPasswordTokenService("Secret", 180000);
  private ResetPasswordUser data;

  @BeforeEach
  public void setup() {
    data = new ResetPasswordUser();
    data.setUserId(UUID.randomUUID());
    data.setEmail("email@fundacion-jala.org");
  }

  @Test
  void testGenerateToken_thenReturnToken() {
    String token = resetPasswordTokenHelper.generateToken(data);

    assertNotNull(token);
  }

  @Test
  void testIsValidToken_thenReturnTrue() {
    String token = resetPasswordTokenHelper.generateToken(data);

    assertTrue(resetPasswordTokenHelper.isValidToken(token, data));
  }

  @Test
  void testIsValidToken_thenReturnFalse() {
    String token =
        "ZW1haWxAZnVuZGFjaW9uLWphbGEub3JnOjE2MzIyMzY1MjEzMzc6ZTc2YmY1MTkwN2EwY2VhZWJlYzQ4MGM2MWMwYmQ2YzZkMjljYzEyZmU2MDU0M2IwNGExNzZkMjdiMDIxYzhiMw==";

    assertFalse(resetPasswordTokenHelper.isValidToken(token, data));
  }
}
