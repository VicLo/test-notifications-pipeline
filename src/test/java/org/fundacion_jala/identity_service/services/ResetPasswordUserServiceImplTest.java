package org.fundacion_jala.identity_service.services;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.fundacion_jala.identity_service.repositories.ResetPasswordUserRepository;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class ResetPasswordUserServiceImplTest {
  public static final String TOKEN = "Token";

  @Mock private ResetPasswordUserRepository repository;

  @InjectMocks private ResetPasswordUserServiceImpl resetPasswordUserService;

  private ResetPasswordUser resetPasswordUser;

  @BeforeEach
  public void setup() {
    resetPasswordUser = new ResetPasswordUser();
    resetPasswordUser.setId(UUID.randomUUID());
    resetPasswordUser.setUserId(UUID.randomUUID());
    resetPasswordUser.setToken(TOKEN);
    resetPasswordUser.setCode("1A2SW4");
    resetPasswordUser.setUsed(false);
    resetPasswordUser.setEmail("example@fundacion-jala.org");
  }

  @AfterEach
  public void tearDown() {
    resetPasswordUser = null;
  }

  @Test
  public void testGetByToken_thenReturnResetPasswordUser() {
    Optional<ResetPasswordUser> resetPasswordUser = Optional.of(new ResetPasswordUser());

    when(repository.findByToken(any(String.class))).thenReturn(resetPasswordUser);

    ResetPasswordUser resetPasswordUserFounded = resetPasswordUserService.getByToken(TOKEN);
    assertNotNull(resetPasswordUserFounded);
    verify(repository, times(1)).findByToken(any());
  }

  @Test
  public void testGetByToken_thenReturnNull() {
    Optional<ResetPasswordUser> resetPasswordUser = Optional.empty();

    when(repository.findByToken(any(String.class))).thenReturn(resetPasswordUser);

    ResetPasswordUser resetPasswordUserFounded = resetPasswordUserService.getByToken(TOKEN);
    assertNull(resetPasswordUserFounded);
    verify(repository, times(1)).findByToken(any());
  }

  @Test
  public void testSave_thenReturnResetPasswordUserSaved() {
    when(repository.save(any(ResetPasswordUser.class))).thenReturn(resetPasswordUser);

    ResetPasswordUser resetPasswordUserCreated = resetPasswordUserService.save(resetPasswordUser);
    assertNotNull(resetPasswordUserCreated);
    assertEquals(resetPasswordUserCreated, resetPasswordUser);
    verify(repository, times(1)).save(any());
  }

  @Test
  public void testInvalidateTokenCode_thenReturnResetPasswordUserWithIsUsedTrue() {
    Optional<ResetPasswordUser> resetPasswordUserFounded = Optional.of(resetPasswordUser);

    when(repository.findById(any(UUID.class))).thenReturn(resetPasswordUserFounded);
    when(repository.save(any(ResetPasswordUser.class))).thenReturn(resetPasswordUser);

    ResetPasswordUser resetPasswordUserCreated =
        resetPasswordUserService.invalidateTokenCode(UUID.randomUUID());
    assertTrue(resetPasswordUserCreated.isUsed());
    verify(repository, times(1)).save(any());
  }

  @Test
  public void testInvalidateTokenCode_thenReturnEntityNotFoundException() {
    Optional<ResetPasswordUser> resetPasswordUser = Optional.empty();

    when(repository.findById(any(UUID.class))).thenReturn(resetPasswordUser);

    assertThrows(
        EntityNotFoundException.class,
        () -> resetPasswordUserService.invalidateTokenCode(UUID.randomUUID()));
    verify(repository, times(0)).save(any());
  }
}
