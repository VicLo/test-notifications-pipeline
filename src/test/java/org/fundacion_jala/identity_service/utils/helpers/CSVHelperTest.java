package org.fundacion_jala.identity_service.utils.helpers;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.services.RoleServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class CSVHelperTest {

  private static List<User> mockUsers = new LinkedList<>();

  private Role role = new Role();

  private static final User mockUser =
      new User(
          "User",
          "User",
          "test.test@fundacion-jala.org",
          10000000,
          "lp",
          7777777,
          "La Paz",
          "Bolivia",
          buildRoles());

  private static final InputStream mockInput =
      new ByteArrayInputStream(
          ("firstName,lastName,email,ci,issued,phoneNumber,currentCity,roles\n"
                  + "User,"
                  + "User,"
                  + "test.test@fundacion-jala.org,"
                  + "10000000,"
                  + "lp,"
                  + "7777777,"
                  + "La Paz,"
                  + "Bolivia,"
                  + "Trainer")
              .getBytes());

  @InjectMocks private CSVHelper csvHelper;

  @InjectMocks private PasswordGenerator passwordGenerator;

  @Mock RoleServiceImpl roleService;

  @BeforeEach
  public void setup() {
    MockitoAnnotations.openMocks(this);
    mockUser.setPassword(passwordGenerator.generateRandomPassword());
    mockUsers.add(mockUser);
    role.setName("Trainer");
    role.setDeleted(false);
    role.setId(UUID.fromString("123e4567-e89b-12d3-a456-556642440000"));
  }

  @Test
  public void shouldReturnFileStatusWhenFileIsProvided() throws IOException {
    List<User> response = csvHelper.csvToUsers(mockInput);
    assertNotNull(response);
    assertEquals(mockUsers, response);
  }

  private static Set<Role> buildRoles() {
    Set<Role> roles = new HashSet<>();
    String uuid = "123e4567-e89b-12d3-a456-556642440000";
    Role role = new Role();
    role.setName("Trainer");
    role.setDeleted(false);
    role.setId(UUID.fromString(uuid));
    roles.add(role);
    return roles;
  }
}
