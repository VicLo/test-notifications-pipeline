package org.fundacion_jala.identity_service.utils.validations;

import static org.junit.Assert.assertFalse;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.repositories.RoleRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class UserValidationVerifierTest {

  private static final String TRAINER = "Trainer";
  private static final String COORDINATOR = "Coordinator";
  private static final String INVALID_ROLE = "Professor";
  private static final String ERROR_ROLE_MESSAGE = "Error in line 2: The given role 'Professor' does not exist";
  private static final String ALLOWED_ROLE_MESSAGE = "Allowed roles are: Coordinator, Trainer.";

  @Mock
  RoleRepository roleRepository;
  @InjectMocks
  UserValidationVerifier userValidationVerifier;

  private List<Role> roles;

  @BeforeEach
  void setUp() {
    roles = new ArrayList<>();
    Role coordinator = new Role();
    coordinator.setName(COORDINATOR);
    Role trainer = new Role();
    trainer.setName(TRAINER);
    roles.add(coordinator);
    roles.add(trainer);
  }

  @Test
  void testShouldReturnTrueIfRoleNameIsValid() {
    when(roleRepository.findAll()).thenReturn(roles);

    assertTrue(userValidationVerifier.validateRole(TRAINER));
  }

  @Test
  void testShouldReturnFalseIfRoleNameIsInvalid() {
    when(roleRepository.findAll()).thenReturn(roles);

    assertFalse(userValidationVerifier.validateRole(INVALID_ROLE));
  }

  @Test
  void testShouldReturnRolesSuggestionsIfDataErrorsContainRoleError() {
    List<String> dataErrors = new ArrayList<>();
    dataErrors.add(ERROR_ROLE_MESSAGE);
    when(roleRepository.findAll()).thenReturn(roles);
    Set<String> allowedValues = userValidationVerifier.getAllowedValues(dataErrors);

    assertEquals(ALLOWED_ROLE_MESSAGE, allowedValues.iterator().next());
  }
}