package org.fundacion_jala.identity_service.utils;

import java.util.ArrayList;
import java.util.List;
import org.fundacion_jala.identity_service.models.User;

public class UserListGenerator {

  private static final String MOCK_FIRSTNAME = "testFirstName";
  private static final String MOCK_LASTNAME = "testLastName";
  private static final String MOCK_EMAIL = "email@test.com";

  public static List<User> get(int amount) {
    List<User> userList = new ArrayList<>();
    while (amount > 0) {
      User user = new User();
      user.setFirstName(MOCK_FIRSTNAME);
      user.setLastName(MOCK_LASTNAME);
      user.setEmail(MOCK_EMAIL);
      userList.add(user);
      amount--;
    }
    return userList;
  }
}
