/*
package org.fundacion_jala.identity_service.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Role;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;

@DataJpaTest
public class RoleRepositoryTest {

  @Autowired RoleRepository roleRepository;

  @Autowired private TestEntityManager entityManager;

  @Test
  public void shouldFindNoRolesIfRepositoryIsEmpty() {
    Iterable<Role> roles = roleRepository.findAll();

    assertThat(roles).isEmpty();
  }

  @Test
  public void shouldStoreARole() {
    Role tutorial = roleRepository.save(buildRole());

    assertThat(tutorial).hasFieldOrPropertyWithValue("name", "Staff");
    assertThat(tutorial).hasFieldOrPropertyWithValue("deleted", false);
  }

  @Test
  public void shouldGetARoleByName() {
    Role role = new Role();
    role.setName("Staff");
    role.setDeleted(false);
    role.setDescription("Should get this role");
    entityManager.persist(role);

    Role result = roleRepository.findRoleByName(role.getName());

    assertThat(result).hasFieldOrPropertyWithValue("name", "Staff");
    assertThat(result).hasFieldOrPropertyWithValue("deleted", false);
  }

  @Test
  public void shouldFindAllRolesAndCountThem() {
    Role role1 = new Role();
    role1.setName("Staff");
    role1.setDeleted(false);
    role1.setDescription("this is a description");
    entityManager.persist(role1);
    Role role2 = new Role();
    role2.setName("Trainer");
    role2.setDeleted(false);
    role2.setDescription("this is a second description");
    entityManager.persist(role2);

    Iterable<Role> roles = roleRepository.findAll();

    assertThat(roles).hasSize(2).contains(role1, role2);
  }

  @Test
  public void testShouldResponseCorrectWithSoftDelete() {
    Role role1 = new Role();
    role1.setName("Staff");
    role1.setDeleted(false);
    role1.setDescription("this is a description");
    entityManager.persist(role1);
    UUID roleId = role1.getId();

    roleRepository.deleteById(roleId);
    Iterable<Role> roles = roleRepository.findAll();

    assertThat(roles).hasSize(1);
  }

  @Test
  public void testShouldResponseCorrectWithSoftDeleteAndChangedColumnDeleted() {
    Role role1 = new Role();
    role1.setName("Staff");
    role1.setDeleted(false);
    role1.setDescription("this is a description");
    entityManager.persist(role1);
    UUID roleId = role1.getId();

    roleRepository.deleteById(roleId);

    List<Role> foundRoles = roleRepository.findByDeleted(true);

    assertThat(foundRoles.isEmpty()).isFalse();
    assertThat(foundRoles).hasSize(1).contains(role1);
    assertThat(foundRoles.get(0).isDeleted()).isTrue();
  }

  private Role buildRole() {
    String uuid = "123e4567-e89b-12d3-a456-556642440000";
    Role role = new Role();
    role.setName("Staff");
    role.setDeleted(false);
    role.setId(UUID.fromString(uuid));
    return role;
  }
}
*/
