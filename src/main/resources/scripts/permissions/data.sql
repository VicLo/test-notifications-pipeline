--Static permissions (Bind to application functionalities or modules)--
INSERT IGNORE INTO permissions (id, name, description) VALUES('9da2b848-fe2d-11eb-9a03-0242ac130003', 'Programs', 'This functionality allow us to access to programs');
INSERT IGNORE INTO permissions (id, name, description) VALUES('9da30f28-fe2d-11eb-9a03-0242ac130003', 'Subject', 'This functionality allow us to access to Subject');
INSERT IGNORE INTO permissions (id, name, description) VALUES('9da31234-fe2d-11eb-9a03-0242ac130003', 'Scholar-profile', 'This functionality allow us to access to Scholar-profile');
INSERT IGNORE INTO permissions (id, name, description) VALUES('9da313ce-fe2d-11eb-9a03-0242ac130003', 'Scholar-evaluation', 'This functionality allow us to access to Scholar-evaluation');
INSERT IGNORE INTO permissions (id, name, description) VALUES('9da315a4-fe2d-11eb-9a03-0242ac130003', 'Scholar-events', 'This functionality allow us to access to Scholar-events');
INSERT IGNORE INTO permissions (id, name, description) VALUES('9da3166c-fe2d-11eb-9a03-0242ac130003', 'Scholar-status', 'This functionality allow us to access to Scholar-status');
INSERT IGNORE INTO permissions (id, name, description) VALUES('9da317ac-fe2d-11eb-9a03-0242ac130003', 'logs', 'This functionality allow us to access to logs');

--Default roles--
INSERT IGNORE INTO roles (id, description, name, deleted) VALUES ('045e9370-0dc9-4665-9a89-1435eb7071b5', 'Trainer role', 'Trainer', 0);
INSERT IGNORE INTO roles (id, description, name, deleted) VALUES ('22f95beb-b955-41a5-b667-5cc2b8341b90', 'Secretary role', 'Secretary', 0);
INSERT IGNORE INTO roles (id, description, name, deleted) VALUES ('2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 'Role for programs coordinator', 'Coordinator', 0);
INSERT IGNORE INTO roles (id, description, name, deleted) VALUES ('6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 'Administrator role', 'Administrator', 0);
INSERT IGNORE INTO roles (id, description, name, deleted) VALUES ('bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 'Role for program monitor', 'Monitor', 0);

--Default roles permissions--

INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da2b848-fe2d-11eb-9a03-0242ac130003', '045e9370-0dc9-4665-9a89-1435eb7071b5', 0, 0, 0, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da2b848-fe2d-11eb-9a03-0242ac130003', '22f95beb-b955-41a5-b667-5cc2b8341b90', 0, 0, 0, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da2b848-fe2d-11eb-9a03-0242ac130003', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 1, 0, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da2b848-fe2d-11eb-9a03-0242ac130003', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da2b848-fe2d-11eb-9a03-0242ac130003', 'bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 0, 0, 0, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da30f28-fe2d-11eb-9a03-0242ac130003', '045e9370-0dc9-4665-9a89-1435eb7071b5', 1, 0, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da30f28-fe2d-11eb-9a03-0242ac130003', '22f95beb-b955-41a5-b667-5cc2b8341b90', 1, 0, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da30f28-fe2d-11eb-9a03-0242ac130003', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da30f28-fe2d-11eb-9a03-0242ac130003', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da30f28-fe2d-11eb-9a03-0242ac130003', 'bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 1, 0, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da31234-fe2d-11eb-9a03-0242ac130003', '045e9370-0dc9-4665-9a89-1435eb7071b5', 1, 0, 0, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da31234-fe2d-11eb-9a03-0242ac130003', '22f95beb-b955-41a5-b667-5cc2b8341b90', 1, 0, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da31234-fe2d-11eb-9a03-0242ac130003', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da31234-fe2d-11eb-9a03-0242ac130003', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da31234-fe2d-11eb-9a03-0242ac130003', 'bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da313ce-fe2d-11eb-9a03-0242ac130003', '045e9370-0dc9-4665-9a89-1435eb7071b5', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da313ce-fe2d-11eb-9a03-0242ac130003', '22f95beb-b955-41a5-b667-5cc2b8341b90', 1, 0, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da313ce-fe2d-11eb-9a03-0242ac130003', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da313ce-fe2d-11eb-9a03-0242ac130003', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da313ce-fe2d-11eb-9a03-0242ac130003', 'bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da315a4-fe2d-11eb-9a03-0242ac130003', '045e9370-0dc9-4665-9a89-1435eb7071b5', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da315a4-fe2d-11eb-9a03-0242ac130003', '22f95beb-b955-41a5-b667-5cc2b8341b90', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da315a4-fe2d-11eb-9a03-0242ac130003', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da315a4-fe2d-11eb-9a03-0242ac130003', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da315a4-fe2d-11eb-9a03-0242ac130003', 'bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da3166c-fe2d-11eb-9a03-0242ac130003', '045e9370-0dc9-4665-9a89-1435eb7071b5', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da3166c-fe2d-11eb-9a03-0242ac130003', '22f95beb-b955-41a5-b667-5cc2b8341b90', 1, 0, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da3166c-fe2d-11eb-9a03-0242ac130003', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da3166c-fe2d-11eb-9a03-0242ac130003', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da3166c-fe2d-11eb-9a03-0242ac130003', 'bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da317ac-fe2d-11eb-9a03-0242ac130003', '045e9370-0dc9-4665-9a89-1435eb7071b5', 0, 0, 0, 0);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da317ac-fe2d-11eb-9a03-0242ac130003', '22f95beb-b955-41a5-b667-5cc2b8341b90', 0, 0, 0, 0);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da317ac-fe2d-11eb-9a03-0242ac130003', '2e29afd4-a41a-43a7-9f30-df4df4f3dce3', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da317ac-fe2d-11eb-9a03-0242ac130003', '6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7', 1, 1, 1, 1);
INSERT IGNORE INTO role_permission (permission_id, role_id, can_add, can_delete, can_edit, can_view) VALUES ('9da317ac-fe2d-11eb-9a03-0242ac130003', 'bd087dc6-af7c-4133-b0f5-7eb02e650ae7', 1, 1, 1, 1);

-- Default users --

INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('21ffc534-1d83-44d5-b264-1e17feabd322', 'Dev33', 'Level2', 'admin.dev33@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('22ffc534-1d83-44d5-b264-1e17feabd322', 'Fabricio', 'Campero', 'fabricio.campero@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('23ffc534-1d83-44d5-b264-1e17feabd322', 'Osmar', 'Ugarte', 'osmar.ugarte@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('24ffc534-1d83-44d5-b264-1e17feabd322', 'Perca', 'Perkins', 'perca.perkins@fundacion-jala.org', 'PercaPerkins');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('25ffc534-1d83-44d5-b264-1e17feabd322', 'Jose', 'Ecos', 'jose.ecos@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('26ffc534-1d83-44d5-b264-1e17feabd322', 'Martin', 'Callejas', 'martin.callejas@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('27ffc534-1d83-44d5-b264-1e17feabd322', 'Felipe', 'Canaviri', 'felipe.canaviri@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('28ffc534-1d83-44d5-b264-1e17feabd322', 'Alejandra', 'Neolopan', 'alejandra.neolopan@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('29ffc534-1d83-44d5-b264-1e17feabd322', 'Denis', 'Salazar', 'denis.salazar@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('30ffc534-1d83-44d5-b264-1e17feabd322', 'Alberth', 'Condori', 'alberth.condori@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('31ffc534-1d83-44d5-b264-1e17feabd322', 'Luis', 'Herbas', 'luis.herbas@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('32ffc534-1d83-44d5-b264-1e17feabd322', 'Lina', 'Ramirez', 'lina.ramirez@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('33ffc534-1d83-44d5-b264-1e17feabd322', 'Carla', 'Guzman', 'carla.guzman@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('3fffc534-1d83-44d5-b264-1e17feabd322', 'Alejandro', 'Ruiz', 'alejandro.ruiz@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');
INSERT IGNORE INTO users (id, first_name, last_name, email, password) VALUES ('4fffc534-1d83-44d5-b264-1e17feabd322', 'Admin', 'Root', 'mentor.dev31@fundacion-jala.org', '$2a$10$O6p8KIbvQGeZmOn10I59M.aOs9njqBoj2/57RfCiXiNTdnAoujNl6');

-- Default user roles --

INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('21ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('22ffc534-1d83-44d5-b264-1e17feabd322','045e9370-0dc9-4665-9a89-1435eb7071b5');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('23ffc534-1d83-44d5-b264-1e17feabd322','045e9370-0dc9-4665-9a89-1435eb7071b5');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('24ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('25ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('26ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('27ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('28ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('29ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('30ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('31ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('32ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('33ffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('3fffc534-1d83-44d5-b264-1e17feabd322','2e29afd4-a41a-43a7-9f30-df4df4f3dce3');
INSERT IGNORE INTO user_role (user_id, role_id) VALUES ('4fffc534-1d83-44d5-b264-1e17feabd322','6026f8d5-7ffe-4ca1-a4a1-628f2cfa1cf7');