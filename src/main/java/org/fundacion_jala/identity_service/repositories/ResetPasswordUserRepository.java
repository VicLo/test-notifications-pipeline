package org.fundacion_jala.identity_service.repositories;

import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ResetPasswordUserRepository extends JpaRepository<ResetPasswordUser, UUID> {
  Optional<ResetPasswordUser> findByToken(String token);
}
