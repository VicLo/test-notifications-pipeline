package org.fundacion_jala.identity_service.repositories;

import java.util.UUID;
import org.fundacion_jala.identity_service.models.User;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
    extends PagingAndSortingRepository<User, UUID>, JpaSpecificationExecutor<User> {

  User findByEmail(String username);
}
