package org.fundacion_jala.identity_service.repositories;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Role;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleRepository
    extends JpaRepository<Role, UUID>, PagingAndSortingRepository<Role, UUID> {

  List<Role> findByDeleted(boolean deleted);

  Page<Role> findByDeleted(Pageable pageable, boolean deleted);

  boolean existsByName(String name);

  Role findRoleByName(String name);
}
