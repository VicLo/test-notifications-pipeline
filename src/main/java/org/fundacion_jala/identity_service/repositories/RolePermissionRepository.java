package org.fundacion_jala.identity_service.repositories;

import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.models.composite_key.RolePermissionPK;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RolePermissionRepository extends JpaRepository<RolePermission, RolePermissionPK> {}
