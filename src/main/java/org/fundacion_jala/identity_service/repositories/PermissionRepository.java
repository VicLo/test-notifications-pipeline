package org.fundacion_jala.identity_service.repositories;

import java.util.UUID;
import org.fundacion_jala.identity_service.models.Permission;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PermissionRepository extends JpaRepository<Permission, UUID> {}
