package org.fundacion_jala.identity_service.repositories;

import java.util.Date;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.security.DenyUserToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DenyUserTokenRepository extends JpaRepository<DenyUserToken, UUID> {
  void deleteAllByExpirationTimeLessThanEqual(Date date);
}
