package org.fundacion_jala.identity_service.controllers;

import io.swagger.annotations.ApiOperation;
import javax.validation.Valid;
import org.fundacion_jala.identity_service.models.Permission;
import org.fundacion_jala.identity_service.services.EntityService;
import org.fundacion_jala.identity_service.utils.constants.EndPoint;
import org.fundacion_jala.identity_service.utils.constants.SwaggerConstants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndPoint.PERMISSIONS)
@CrossOrigin(origins = "${react.origin.url}")
public class PermissionController {

  private static final Logger logger = LoggerFactory.getLogger(PermissionController.class);

  @Autowired private EntityService<Permission> permissionService;

  @GetMapping
  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_ALL_PERMISSIONS)
  public ResponseEntity<Iterable<Permission>> getAllPermissions() {
    logger.info("Get all permissions");
    return ResponseEntity.ok(permissionService.getAll());
  }

  @PostMapping
  @ApiOperation(value = SwaggerConstants.API_OPERATION_SAVE_PERMISSION)
  public ResponseEntity<Permission> savePermission(@Valid @RequestBody Permission savePermission) {
    logger.info("Inserting new Permission {}", savePermission.getName());
    return ResponseEntity.ok(permissionService.save(savePermission));
  }
}
