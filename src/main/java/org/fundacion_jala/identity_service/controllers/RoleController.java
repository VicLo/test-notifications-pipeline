package org.fundacion_jala.identity_service.controllers;

import io.swagger.annotations.ApiOperation;
import java.util.UUID;
import javax.validation.Valid;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.services.BulkEditEntityService;
import org.fundacion_jala.identity_service.services.EntityService;
import org.fundacion_jala.identity_service.utils.constants.EndPoint;
import org.fundacion_jala.identity_service.utils.constants.Parameter;
import org.fundacion_jala.identity_service.utils.constants.Path;
import org.fundacion_jala.identity_service.utils.constants.SwaggerConstants;
import org.fundacion_jala.identity_service.utils.requests.BulkEditRolesRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(EndPoint.ROLES)
@CrossOrigin(origins = "${react.origin.url}")
public class RoleController {

  private static final Logger logger = LoggerFactory.getLogger(RoleController.class);

  @Autowired private EntityService<Role> roleService;

  @Autowired private BulkEditEntityService<BulkEditRolesRequest> bulkEditRolesService;

  @GetMapping
  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_ALL_ROLES)
  public ResponseEntity<DataAndPagination> getAllRoles(
      @RequestParam(defaultValue = "1") Integer page,
      @RequestParam(defaultValue = "10") Integer size,
      @RequestParam(defaultValue = "name") String sort) {
    return ResponseEntity.ok(roleService.getAllPagination(page, size, sort));
  }

  @GetMapping(Path.ROLE_ID)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_ROLE_BY_ID)
  public ResponseEntity<Role> getRoleByID(@PathVariable(Parameter.ROLE_ID) UUID roleId) {
    return ResponseEntity.ok(roleService.getById(roleId));
  }

  @PostMapping
  @ApiOperation(value = SwaggerConstants.API_OPERATION_SAVE_ROLE)
  public ResponseEntity<Role> saveRole(@Valid @RequestBody Role saveRole) {
    logger.info("Inserting new Role {}", saveRole.getName());
    return ResponseEntity.ok(roleService.save(saveRole));
  }

  @PutMapping(EndPoint.ID)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_UPDATE_ROLE_BY_ID)
  public ResponseEntity<Role> updateRole(
      @PathVariable UUID id, @Valid @RequestBody Role updateRole) {
    logger.info("Updating Role {}", updateRole.getName());
    return ResponseEntity.ok(roleService.update(id, updateRole));
  }

  @DeleteMapping(EndPoint.ID)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_DELETE_ROLE_BY_ID)
  public ResponseEntity<BasicResponse> deleteRole(@Valid @PathVariable UUID id) {
    logger.info("Deleting role with id {}", id);
    return ResponseEntity.ok(roleService.delete(id));
  }

  @PutMapping(EndPoint.BULK)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_BULK_EDIT_ROLES)
  public ResponseEntity<BasicResponse> editInBulk(
      @Valid @RequestBody BulkEditRolesRequest bulkEditRolesRequest) {
    logger.info("Edit roles in bulk");
    return ResponseEntity.ok(bulkEditRolesService.editInBulk(bulkEditRolesRequest));
  }
}
