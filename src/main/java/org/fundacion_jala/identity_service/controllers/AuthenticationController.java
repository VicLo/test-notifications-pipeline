package org.fundacion_jala.identity_service.controllers;

import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.apache.http.HttpHeaders;
import org.fundacion_jala.identity_service.configurations.security.JwtUtils;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTClaims;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTConstants;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTMessages;
import org.fundacion_jala.identity_service.models.TokenPermissions;
import org.fundacion_jala.identity_service.models.security.DenyUserToken;
import org.fundacion_jala.identity_service.services.DenyUserTokenService;
import org.fundacion_jala.identity_service.utils.constants.EndPoint;
import org.fundacion_jala.identity_service.utils.constants.JwtConstant;
import org.fundacion_jala.identity_service.utils.constants.Path;
import org.fundacion_jala.identity_service.utils.constants.SwaggerConstants;
import org.fundacion_jala.identity_service.utils.messages.MessageConstant;
import org.fundacion_jala.identity_service.utils.requests.LoginRequest;
import org.fundacion_jala.identity_service.utils.responses.AuthResponse;
import org.fundacion_jala.identity_service.utils.responses.ValidateTokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(EndPoint.AUTH)
public class AuthenticationController {

  private static final Logger logger = LoggerFactory.getLogger(AuthenticationController.class);

  @Autowired private AuthenticationManager authenticationManager;

  @Autowired private JwtUtils jwtUtils;

  @Autowired private DenyUserTokenService denyUserTokenService;

  @PostMapping(Path.LOGIN)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_LOG_IN)
  public ResponseEntity<AuthResponse> login(@RequestBody LoginRequest request) {
    logger.info("Starting session as {}", request.getEmail());
    Authentication authentication =
        authenticationManager.authenticate(
            new UsernamePasswordAuthenticationToken(request.getEmail(), request.getPassword()));
    SecurityContextHolder.getContext().setAuthentication(authentication);
    String jwt = jwtUtils.generateJwtToken(authentication);
    logger.info("The user {} has successfully logged in", request.getEmail());
    return ResponseEntity.ok(
        AuthResponse.builder()
            .message("OK")
            .success(true)
            .data(new ArrayList<>(Arrays.asList(jwt)))
            .build());
  }

  @PostMapping(Path.LOGOUT)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_LOG_OUT)
  public ResponseEntity<AuthResponse> logout(HttpServletRequest request) {
    String token = jwtUtils.parseJwt(request.getHeader(HttpHeaders.AUTHORIZATION));
    DenyUserToken denyUserToken = new DenyUserToken();
    denyUserToken.setJwtId(jwtUtils.getJtiClaim(token));
    denyUserToken.setExpirationTime(jwtUtils.getExpClaim(token));
    denyUserToken.setTokenType(JwtConstant.ACCESS_TOKEN);
    denyUserTokenService.save(denyUserToken);
    AuthResponse body =
        AuthResponse.builder().message(MessageConstant.LOGOUT_SUCCESS).success(true).build();
    logger.info("The user has successfully logout");
    return ResponseEntity.ok(body);
  }

  @PostMapping(Path.AUTH)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_VALIDATE_TOKEN)
  public ResponseEntity<ValidateTokenResponse> validateToken(HttpServletRequest request) {
    String token = jwtUtils.parseJwt(request.getHeader(HttpHeaders.AUTHORIZATION));
    Date expires = jwtUtils.getExpClaim(token);
    Date currentTime = new Date();
    long expirationDifference = expires.getTime() - currentTime.getTime();
    TokenPermissions permissions = jwtUtils.getPermissionsFromJwtToken(token);
    UUID userId = jwtUtils.getSubClaim(token);
    String role = jwtUtils.getClaimFromJwtToken(token, JWTClaims.ROLE);

    return expirationDifference < 0
        ? ResponseEntity.status(HttpStatus.FORBIDDEN)
            .body(
                ValidateTokenResponse.builder()
                    .accessToken(token)
                    .tokenType(JWTConstants.BEARER)
                    .message(JWTMessages.VALID_TOKEN)
                    .expiresIn(expirationDifference)
                    .success(false)
                    .build())
        : ResponseEntity.ok(
            ValidateTokenResponse.builder()
                .accessToken(token)
                .tokenType(JWTConstants.BEARER)
                .message(JWTMessages.VALID_TOKEN)
                .expiresIn(expirationDifference)
                .success(true)
                .permissions(permissions)
                .userId(userId)
                .role(role)
                .build());
  }
}
