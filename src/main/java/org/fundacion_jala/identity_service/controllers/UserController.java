package org.fundacion_jala.identity_service.controllers;

import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.apache.http.HttpHeaders;
import org.fundacion_jala.identity_service.configurations.security.JwtUtils;
import org.fundacion_jala.identity_service.file_management.UsersFileResponse;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.services.ResetPasswordService;
import org.fundacion_jala.identity_service.services.UserServiceImpl;
import org.fundacion_jala.identity_service.utils.constants.Defaults;
import org.fundacion_jala.identity_service.utils.constants.EndPoint;
import org.fundacion_jala.identity_service.utils.constants.Parameter;
import org.fundacion_jala.identity_service.utils.constants.Path;
import org.fundacion_jala.identity_service.utils.constants.SwaggerConstants;
import org.fundacion_jala.identity_service.utils.helpers.EntityBuilderHelper;
import org.fundacion_jala.identity_service.utils.requests.ChangeUserPasswordRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.fundacion_jala.identity_service.utils.responses.CheckEmailResponse;
import org.fundacion_jala.identity_service.utils.responses.GenericDataResponse;
import org.fundacion_jala.identity_service.utils.validations.UUIDValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(EndPoint.USERS)
@CrossOrigin(origins = "${react.origin.url}")
public class UserController {

  private static final Logger logger = LoggerFactory.getLogger(PermissionController.class);

  @Autowired private UserServiceImpl userService;

  @Autowired private ResetPasswordService resetPasswordService;

  @Autowired private JwtUtils jwtUtils;

  @Autowired private EntityBuilderHelper entityBuilderHelper;

  @GetMapping
  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_ALL_USERS)
  public ResponseEntity<DataAndPagination> getAllUsers(
      @RequestParam(defaultValue = Defaults.EMPTY) String criteria,
      @RequestParam(defaultValue = Defaults.PAGE_NUMBER) Integer pageNumber,
      @RequestParam(defaultValue = Defaults.PAGE_SIZE) Integer pageSize,
      @RequestParam(defaultValue = Defaults.USER_SORT) String sort,
      @RequestParam(defaultValue = Defaults.EMPTY) String filterOption,
      @RequestParam(defaultValue = Defaults.EMPTY) String filterValue) {
    logger.info("Filter user by {} and looking for user with match: {}", filterOption, criteria);
    return ResponseEntity.ok(
        userService.getAll(criteria, pageNumber, pageSize, sort, filterOption, filterValue));
  }

  @PostMapping
  public ResponseEntity<UsersFileResponse> uploadFile(
      @RequestParam(Parameter.FILES) List<MultipartFile> files) {
    logger.info("Uploading {} files", files.size());
    return ResponseEntity.ok(userService.buildFiles(files));
  }

  @GetMapping(EndPoint.CHECK_EMAIL)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_CHECK_USER_BY_EMAIL)
  public ResponseEntity<CheckEmailResponse> checkUserByEmail(@RequestParam String email) {
    return ResponseEntity.ok(resetPasswordService.verifyEmail(email));
  }

  @PutMapping(Path.USER_ID)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_UPDATE_USER)
  public ResponseEntity<User> updateUser(
      @Valid @RequestBody User updateUser, @PathVariable UUID userId) {
    logger.info("Updating user with id: {}", userId);
    return ResponseEntity.ok(userService.update(userId, updateUser));
  }

  @PutMapping(EndPoint.CHANGE_PASSWORD)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_CHANGE_USER_PASSWORD)
  public ResponseEntity<BasicResponse> changeUserPassword(
      @RequestParam String token,
      @Valid @RequestBody ChangeUserPasswordRequest changeUserPasswordRequest) {
    return ResponseEntity.ok(resetPasswordService.resetPassword(token, changeUserPasswordRequest));
  }

  @GetMapping(EndPoint.INPUT_VARIABLE)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_USER_BY_INPUT)
  public ResponseEntity<?> getUserByInput(@PathVariable @NotNull String input) {
    User userFound;
    if (UUIDValidator.isUUID(input)) {
      logger.info("Searching user with id: {}", input);
      userFound = userService.getById(UUID.fromString(input));
    } else {
      logger.info("Searching user with the email: {}", input);
      userFound = userService.getByEmail(input);
    }
    return ResponseEntity.ok(entityBuilderHelper.buildUserWithBasicData(userFound));
  }

  @GetMapping(EndPoint.PROFILE)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_GET_USER_PROFILE)
  public ResponseEntity<GenericDataResponse> getUserProfile(HttpServletRequest request) {
    String token = jwtUtils.parseJwt(request.getHeader(HttpHeaders.AUTHORIZATION));
    logger.info("Validating token: {}", token);
    User user = userService.getByEmail(jwtUtils.getUserNameFromJwtToken(token));
    return ResponseEntity.ok(
        GenericDataResponse.builder().message("Success").success(true).data(user).build());
  }

  @GetMapping(EndPoint.CHECK_TOKEN)
  @ApiOperation(value = SwaggerConstants.API_OPERATION_VALIDATE_TOKEN)
  public ResponseEntity<BasicResponse> validateToken(@RequestParam String token) {
    return ResponseEntity.ok(resetPasswordService.validateToken(token));
  }
}
