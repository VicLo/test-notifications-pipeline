package org.fundacion_jala.identity_service.utils.helpers;

import java.security.SecureRandom;
import org.fundacion_jala.identity_service.utils.constants.DefaultPassword;
import org.fundacion_jala.identity_service.utils.validations.PasswordValidationVerifier;
import org.springframework.stereotype.Service;

@Service
public class PasswordGenerator {

  public static String generateRandomPassword() {
    SecureRandom secureRandom = new SecureRandom();
    String password = "";
    do {
      password =
          secureRandom
              .ints(DefaultPassword.ASCII_START, DefaultPassword.ASCII_BOUND + 1)
              .limit(DefaultPassword.PASSWORD_LENGTH)
              .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
              .toString();
    } while (!PasswordValidationVerifier.validatePassword(password));
    return password;
  }
}
