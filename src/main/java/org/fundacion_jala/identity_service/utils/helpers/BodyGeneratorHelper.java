package org.fundacion_jala.identity_service.utils.helpers;

import java.util.HashMap;
import java.util.Map;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class BodyGeneratorHelper {

  @Value("${applicant-tracking.reset-password.token.expiration-in-milliseconds}")
  private int expirationTime;

  /**
   * Gets a Structure to use the notifications services like body working with default params like
   * subject, type and hasTemplate
   *
   * @param ownerId the id of the email owner
   * @param code the secret code to validate the reset password
   * @param emails a list of email where the email will be send
   * @return a map with the body structure to send emails.
   */
  public Map<String, Object> generateBodyEmailResetPassword(
      String ownerId, String code, String[] emails) {
    Map<String, Object> body = new HashMap<>();
    body.put("emailTargets", emails);
    body.put("owner", ownerId);
    body.put("subject", "Reset Password Request");
    body.put(
        "body",
        String.format(
            "Your secret code is: %s " + "remember that this code is valid for %d hours.",
            code, ConverterTimeHelper.millisecondsToHours(expirationTime)));
    body.put("type", "RESET_PASSWORD");
    body.put("hasTemplate", true);
    return body;
  }

  public Map<String, Object> generateBodyEmailDefaultPassword(String password, String[] emails) {
    Map<String, Object> body = new HashMap<>();
    body.put("emailTargets", emails);
    body.put("subject", "Generated Password");
    body.put("body", String.format("Your generated password for your account is: %s ", password));
    body.put("type", "GENERATED_DEFAULT_PASSWORD");
    body.put("hasTemplate", true);
    return body;
  }
}
