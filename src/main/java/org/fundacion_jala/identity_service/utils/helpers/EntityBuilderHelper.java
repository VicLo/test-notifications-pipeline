package org.fundacion_jala.identity_service.utils.helpers;

import java.util.Date;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.fundacion_jala.identity_service.services.ResetPasswordTokenService;
import org.fundacion_jala.identity_service.services.TokenService;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EntityBuilderHelper {

  @Autowired private TokenService tokenService;

  /**
   * Builds a ResetPasswordUser object from a User object
   *
   * @param user object of a user
   * @return object built
   */
  public ResetPasswordUser buildResetPasswordUser(User user) {
    ResetPasswordUser resetPasswordUser = new ResetPasswordUser();
    resetPasswordUser.setUsed(false);
    resetPasswordUser.setUserId(user.getId());
    resetPasswordUser.setEmail(user.getEmail());

    String token = tokenService.generateToken(resetPasswordUser);
    String code = CodeGeneratorHelper.generateRandomCode();

    resetPasswordUser.setCode(code);
    resetPasswordUser.setToken(token);
    resetPasswordUser.setExpirationTime(
        (Date) tokenService.getPayload(token).get(ResetPasswordTokenService.PAYLOAD_FIELDS[1]));
    return resetPasswordUser;
  }

  /**
   * Builds a BasicResponse object
   *
   * @param message attribute for basic response object
   * @return object built
   */
  public BasicResponse buildBasicResponse(String message) {
    BasicResponse basicResponse = new BasicResponse();
    basicResponse.setMessage(message);
    return basicResponse;
  }

  /**
   * Builds a User object
   *
   * @param userFound with the all data
   * @return object built
   */
  public User buildUserWithBasicData(User userFound) {
    User userResponse = new User();
    userResponse.setId(userFound.getId());
    userResponse.setEmail(userFound.getEmail());
    userResponse.setFirstName(userFound.getFirstName());
    userResponse.setLastName(userFound.getLastName());
    userResponse.setRoles(userFound.getRoles());
    return userResponse;
  }
}
