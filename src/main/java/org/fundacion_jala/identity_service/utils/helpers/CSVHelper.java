package org.fundacion_jala.identity_service.utils.helpers;

import java.io.*;
import java.util.*;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.services.EntityService;
import org.fundacion_jala.identity_service.utils.constants.UserFileFields;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CSVHelper {

  @Autowired private EntityService<Role> roleService;

  public boolean hasCSVFormat(MultipartFile file) {
    return (UserFileFields.CSV_TYPE.equals(file.getContentType())
        || UserFileFields.XLSX_TYPE.equals(file.getContentType()));
  }

  public List<User> csvToUsers(InputStream input) throws IOException {
    BufferedReader fileReader =
        new BufferedReader(new InputStreamReader(input, UserFileFields.CHARSET_NAME));
    CSVParser csvParser =
        new CSVParser(fileReader, CSVFormat.DEFAULT.withHeader(UserFileFields.HEADERS));
    List<CSVRecord> csvRecords = csvParser.getRecords();
    List<User> users = new LinkedList<>();

    for (CSVRecord csvRecord : csvRecords.subList(1, csvRecords.size())) {
      if (!csvRecord.get(0).isEmpty()) {
        Set<Role> roles = new HashSet<>();
        User user =
            new User(
                csvRecord.get(UserFileFields.HEADERS[0]),
                csvRecord.get(UserFileFields.HEADERS[1]),
                csvRecord.get(UserFileFields.HEADERS[2]),
                Double.valueOf(csvRecord.get(UserFileFields.HEADERS[3])).longValue(),
                csvRecord.get(UserFileFields.HEADERS[4]),
                Double.valueOf(csvRecord.get(UserFileFields.HEADERS[5])).longValue(),
                csvRecord.get(UserFileFields.HEADERS[6]),
                csvRecord.get(UserFileFields.HEADERS[7]),
                roles);
        user.setRoleName(csvRecord.get(UserFileFields.HEADERS[8]));
        users.add(user);
      }
    }

    return users;
  }
}
