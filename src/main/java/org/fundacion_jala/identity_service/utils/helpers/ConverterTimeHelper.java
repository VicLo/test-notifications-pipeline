package org.fundacion_jala.identity_service.utils.helpers;

import java.util.concurrent.TimeUnit;

public class ConverterTimeHelper {
  public static int millisecondsToHours(int milliseconds) {
    return (int) TimeUnit.MILLISECONDS.toHours(milliseconds);
  }
}
