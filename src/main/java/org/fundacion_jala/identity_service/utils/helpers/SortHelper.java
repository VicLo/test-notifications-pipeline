package org.fundacion_jala.identity_service.utils.helpers;

import java.util.regex.Pattern;
import org.springframework.data.domain.Sort;

public class SortHelper {
  private static final String descendingRegex = "^[-]{1}+[a-zA-Z]{1,}$";
  private static Pattern descendingPattern = Pattern.compile(descendingRegex);

  public static Sort sortSetter(String sortField) {
    if (sortAscendingVerifier(sortField)) {
      return Sort.by(sortField).ascending();
    }
    return Sort.by(sortField.substring(1)).descending();
  }

  private static boolean sortAscendingVerifier(String sortField) {
    return !descendingPattern.matcher(sortField).matches();
  }
}
