package org.fundacion_jala.identity_service.utils.helpers;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbookFactory;
import org.fundacion_jala.identity_service.utils.constants.UserFileFields;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class XLSXHelper {
  public static InputStream xlsxToCsv(MultipartFile file) throws IOException {
    InputStream inputFile = file.getInputStream();
    Workbook workbook = XSSFWorkbookFactory.create(inputFile);
    InputStream inputStream = csvConverter(workbook.getSheetAt(0));
    return inputStream;
  }

  private static InputStream csvConverter(Sheet sheet) throws IOException {
    StringBuilder data = new StringBuilder();
    for (int rowNumber = 0; rowNumber <= sheet.getLastRowNum(); rowNumber++) {
      Row row = sheet.getRow(rowNumber);
      StringBuilder userData = new StringBuilder();
      int cellNumber = 0;
      if (row.getCell(cellNumber).toString().isEmpty()) {
        break;
      }
      for (cellNumber = 0; cellNumber < row.getLastCellNum(); cellNumber++) {
        if (row.getCell(cellNumber) != null) {
          userData.append(row.getCell(cellNumber)).append(",");
        }
      }
      data.append(userData).append("\n");
    }
    return new ByteArrayInputStream(data.toString().getBytes(UserFileFields.CHARSET_NAME));
  }
}
