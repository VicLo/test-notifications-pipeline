package org.fundacion_jala.identity_service.utils.helpers;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import org.fundacion_jala.identity_service.models.Permission;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.models.composite_key.RolePermissionPK;

public class RolePermissionHelper {
  public static RolePermission buildRolePermission(UUID roleId, RolePermission entity) {
    Permission permission = new Permission(entity.getPermissions().getId());
    RolePermissionPK rolePermissionPK = new RolePermissionPK(roleId, permission.getId());
    RolePermission rolePermission = new RolePermission();

    rolePermission.setCanAdd(entity.getCanAdd());
    rolePermission.setCanEdit(entity.getCanEdit());
    rolePermission.setCanDelete(entity.getCanDelete());
    rolePermission.setCanView(entity.getCanView());

    rolePermission.setPermissions(permission);
    rolePermission.setId(rolePermissionPK);
    rolePermission.setRoles(new Role(roleId));
    return rolePermission;
  }

  public static List<RolePermission> buildRolePermissions(
      UUID roleId, List<RolePermission> rolePermissions) {
    return rolePermissions.stream()
        .map(rolePermission -> RolePermissionHelper.buildRolePermission(roleId, rolePermission))
        .collect(Collectors.toList());
  }
}
