package org.fundacion_jala.identity_service.utils.helpers;

import org.apache.commons.lang.RandomStringUtils;

public class CodeGeneratorHelper {

  public static final int NUMBER_OF_CHARACTERS = 6;

  /** Generates a random 6 characters in Upper Case */
  public static String generateRandomCode() {
    return RandomStringUtils.randomAlphanumeric(NUMBER_OF_CHARACTERS).toUpperCase();
  }
}
