package org.fundacion_jala.identity_service.utils.constants;

public class Defaults {
  public static final String EMPTY = "";
  public static final String PAGE_NUMBER = "1";
  public static final String PAGE_SIZE = "10";
  public static final String USER_SORT = "firstName";
  public static final String FILTER_FIELD = "";
  public static final String FILTER_OPTION = "";
  public static final String POINT = ".";
  public static final String SPACE = " ";
  public static final String COMMA = ",";
}
