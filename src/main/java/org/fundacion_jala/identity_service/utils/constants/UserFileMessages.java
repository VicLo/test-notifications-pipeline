package org.fundacion_jala.identity_service.utils.constants;

public class UserFileMessages {
  public static final String STATUS_SUCCESS = "Success";
  public static final String STATUS_ERROR = "Error";
  public static final String MESSAGE_SUCCESS = "Success";
  public static final String MESSAGE_ERROR = "Error";
  public static final String FILE_DATA_ERROR = "Fail to store csv data";
  public static final String FILE_ERROR = "File has not CSV format";
}
