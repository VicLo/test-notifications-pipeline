package org.fundacion_jala.identity_service.utils.constants;

public class TableConstant {
  public static final String TYPE_UUID = "uuid-char";
  public static final String TYPE_BOOLEAN = "boolean";
  public static final String TABLE_NAME_PERMISSIONS = "permissions";
  public static final String TABLE_NAME_ROLES = "roles";
  public static final String TABLE_NAME_USERS = "users";
  public static final String COLUMN_PERMISSION_ID = "permission_id";
  public static final String COLUMN_ROLE_ID = "role_id";
  public static final String MAP_PERMISSION_ID = "permissionId";
  public static final String MAP_ROLE_ID = "roleId";
  public static final String COLUMN_DEFINITION_DELETED = "boolean default false";

  public static final String QUERY_DELETE_SOFT = "UPDATE roles SET deleted = true WHERE id=?";
  public static final String COLUMN_NAME_SOFT_DELETE = "deleted";
  public static final String FILTER_NAME_SOFT_DELETE = "deletedRoleFilter";
  public static final String TABLE_NAME_DENY_USER_TOKENS = "deny_user_tokens";
}
