package org.fundacion_jala.identity_service.utils.constants;

public class SwaggerConstants {
  public static final String API_OPERATION_LOG_IN = "Log In";
  public static final String API_OPERATION_LOG_OUT = "Log Out";
  public static final String API_OPERATION_VALIDATE_TOKEN = "Validate token";

  public static final String API_OPERATION_GET_ALL_PERMISSIONS = "Get all permissions";
  public static final String API_OPERATION_SAVE_PERMISSION = "Save a permission";

  public static final String API_OPERATION_GET_ALL_ROLES = "Get all roles";
  public static final String API_OPERATION_GET_ROLE_BY_ID = "Get role by ID";
  public static final String API_OPERATION_SAVE_ROLE = "Save role";
  public static final String API_OPERATION_UPDATE_ROLE_BY_ID = "Update role by ID";
  public static final String API_OPERATION_DELETE_ROLE_BY_ID = "Delete role by ID";
  public static final String API_OPERATION_BULK_EDIT_ROLES = "Bulk edit for Roles";

  public static final String API_OPERATION_GET_ALL_USERS = "Get all Users";
  public static final String API_OPERATION_CHECK_USER_BY_EMAIL = "Check User By Email";
  public static final String API_OPERATION_UPDATE_USER = "Update a User";
  public static final String API_OPERATION_CHANGE_USER_PASSWORD = "Change User Password";
  public static final String API_OPERATION_GET_USER_BY_INPUT = "Get User by Input";
  public static final String API_OPERATION_GET_USER_PROFILE = "Get User Profile";
}
