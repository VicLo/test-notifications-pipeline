package org.fundacion_jala.identity_service.utils.constants;

public class UserFileFields {
  public static final String CSV_TYPE = "text/csv";
  public static final String XLSX_TYPE =
      "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
  public static final String[] HEADERS = {
    "firstName", "lastName", "email", "ci", "issued", "phoneNumber", "currentCity", "country", "roles"
  };
  public static final String CHARSET_NAME = "UTF-8";
}
