package org.fundacion_jala.identity_service.utils.constants;

public class EndPoint {
  public static final String IDENTITY_SERVICE = "/api/v1";
  public static final String ROLES = IDENTITY_SERVICE + "/roles";
  public static final String ROLES_TEST = IDENTITY_SERVICE + "/roles";

  public static final String PERMISSIONS = IDENTITY_SERVICE + "/permissions";
  public static final String PERMISSIONS_TEST = IDENTITY_SERVICE + "/permissions";
  public static final String AUTH = IDENTITY_SERVICE + "/authentication";
  public static final String USERS = IDENTITY_SERVICE + "/users";
  public static final String SEARCH = "/search";
  public static final String ID = "/{id}";
  public static final String BULK = "/bulk";
  public static final String IMPORT_FILE = "/import";
  public static final String CHECK_EMAIL = "/check-email";
  public static final String CHECK_TOKEN = "/check-token";
  public static final String CHANGE_PASSWORD = "/change-password";
  public static final String EMAIL = "/email";
  public static final String INPUT_VARIABLE = "/{input}";
  public static final String PROFILE = "/profile";
}
