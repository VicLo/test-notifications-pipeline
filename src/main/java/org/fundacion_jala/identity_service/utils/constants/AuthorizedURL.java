package org.fundacion_jala.identity_service.utils.constants;

public class AuthorizedURL {
  public static final String LOGIN = EndPoint.AUTH + Path.LOGIN;
  public static final String CHECK_EMAIL = EndPoint.USERS + EndPoint.CHECK_EMAIL;
  public static final String CHECK_TOKEN = EndPoint.USERS + EndPoint.CHECK_TOKEN;
  public static final String CHANGE_PASSWORD = EndPoint.USERS + EndPoint.CHANGE_PASSWORD;
  // TODO: Delete this when the integration with the C# team microservices is finished
  public static final String ROLES = EndPoint.ROLES + "/**";
  public static final String SWAGGER_UI = "/swagger-ui/**";
  public static final String SWAGGER_HTML = "/swagger-ui.html";
  public static final String SWAGGER_VERSION = "/v2/**";
  public static final String SWAGGER_RESOURCES = "/swagger-resources/**";
}
