package org.fundacion_jala.identity_service.utils.constants;

import java.util.Arrays;
import java.util.List;

public class UserFields {
  public static final List<String> FIELD_LIST =
      Arrays.asList("firstName", "lastName", "email", "currentCity", "ci", "phoneNumber");
}
