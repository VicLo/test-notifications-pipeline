package org.fundacion_jala.identity_service.utils.constants;

public class ExternalEndpoint {

  public static final String SEND_EMAIL_SERVICE =
      "http://notifications-service/api/v1/notifications/email";
}
