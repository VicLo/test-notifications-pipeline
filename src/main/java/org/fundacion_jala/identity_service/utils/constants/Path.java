package org.fundacion_jala.identity_service.utils.constants;

public class Path {
  public static final String PORT = "/port";
  public static final String ROLE_ID = "{roleId}";
  public static final String LOGIN = "/login";
  public static final String LOGOUT = "/logout";
  public static final String AUTH = "/auth";
  public static final String USER_ID = "/{userId}";
}
