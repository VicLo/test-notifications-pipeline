package org.fundacion_jala.identity_service.utils.constants;

public class DefaultPassword {
  public static final int PASSWORD_LENGTH = 10;
  public static final int ASCII_START = 42;
  public static final int ASCII_BOUND = 122;
}
