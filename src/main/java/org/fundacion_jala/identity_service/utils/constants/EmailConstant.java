package org.fundacion_jala.identity_service.utils.constants;

public class EmailConstant {

  public static final String RESET_PASSWORD_CODE_KEY = "code";
  public static final String RESET_PASSWORD_LINK_KEY = "link";
}
