package org.fundacion_jala.identity_service.utils.exceptions;

public class ResetPasswordCodeUsedException extends RuntimeException {

  /** Throw when the code has already been used. */
  public ResetPasswordCodeUsedException() {
    super("The code sent to change the password has already been used.");
  }
}
