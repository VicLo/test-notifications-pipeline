package org.fundacion_jala.identity_service.utils.exceptions;

import org.fundacion_jala.identity_service.utils.messages.MessageConstant;

public class InvalidTokenException extends RuntimeException {
  /** Throw when the token is invalid (different digests, token expired). */
  public InvalidTokenException() {
    super(MessageConstant.INVALID_TOKEN_ERROR);
  }
}
