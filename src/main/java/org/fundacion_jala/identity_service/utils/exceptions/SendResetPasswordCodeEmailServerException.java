package org.fundacion_jala.identity_service.utils.exceptions;

public class SendResetPasswordCodeEmailServerException extends RuntimeException {
  public SendResetPasswordCodeEmailServerException() {
    super("An error occurred while sending the security code to reset the password");
  }
}
