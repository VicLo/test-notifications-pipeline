package org.fundacion_jala.identity_service.utils.exceptions;

public class MACAlgorithmInitException extends RuntimeException {
  /**
   * Thrown when the MAC algorithm could not be configured correctly.
   *
   * @param message message you get where the exception occurred
   */
  public MACAlgorithmInitException(String message) {
    super(message);
  }
}
