package org.fundacion_jala.identity_service.utils.exceptions;

public class UnsupportedMultipleRoleException extends RuntimeException {
  public UnsupportedMultipleRoleException(String type, String name) {
    super(String.format("The %s with name: %s only must have one role", type, name));
  }
}
