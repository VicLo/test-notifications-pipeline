package org.fundacion_jala.identity_service.utils.exceptions;

public class FieldEntityNotLocatedException extends RuntimeException {
  /**
   * Custom exception that throw when a field entity is not located.
   *
   * @param field field name of an entity
   */
  public FieldEntityNotLocatedException(String field) {
    super(String.format("Unable to locate field entity with name: %s", field));
  }
}
