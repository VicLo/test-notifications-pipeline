package org.fundacion_jala.identity_service.utils.exceptions;

public class ValidationCodeIncorrectException extends RuntimeException {

  /** throw when the code is different from the one sent. */
  public ValidationCodeIncorrectException() {
    super("The security code is incorrect");
  }
}
