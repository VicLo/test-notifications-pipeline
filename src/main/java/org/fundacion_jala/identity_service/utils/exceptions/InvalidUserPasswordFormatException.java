package org.fundacion_jala.identity_service.utils.exceptions;

public class InvalidUserPasswordFormatException extends RuntimeException {
  public InvalidUserPasswordFormatException() {
    super("Password is not in the correct format");
  }
}
