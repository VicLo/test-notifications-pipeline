package org.fundacion_jala.identity_service.utils.exceptions;

import java.util.UUID;

public class EntityNotFoundException extends RuntimeException {

  private String field;

  /**
   * Custom exception that throw when a entity is not found.
   *
   * @param type name of the type of model such as User.
   * @param id entity id.
   */
  public EntityNotFoundException(String type, UUID id) {
    super(String.format("%s with id:%s not found", type, id));
  }

  public EntityNotFoundException(String type, String name) {
    super(String.format("%s with name:%s not found", type, name));
  }

  public EntityNotFoundException(String type, String field, String value) {
    super(String.format("%s with %s: %s not found", type, field, value));
    this.field = field;
  }

  public String getField() {
    return this.field;
  }
}
