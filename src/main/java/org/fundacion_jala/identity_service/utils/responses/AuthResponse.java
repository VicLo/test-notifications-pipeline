package org.fundacion_jala.identity_service.utils.responses;

import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class AuthResponse {
  public String message;
  public List<String> data;
  public List<String> error;
  public boolean success;
}
