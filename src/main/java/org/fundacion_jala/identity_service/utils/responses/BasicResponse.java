package org.fundacion_jala.identity_service.utils.responses;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BasicResponse {
  private String message;
}
