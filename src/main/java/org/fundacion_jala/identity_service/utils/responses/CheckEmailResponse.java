package org.fundacion_jala.identity_service.utils.responses;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Builder
@Getter
@Setter
public class CheckEmailResponse {

  private String token;
}
