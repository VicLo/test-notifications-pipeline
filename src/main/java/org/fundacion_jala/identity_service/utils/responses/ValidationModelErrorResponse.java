package org.fundacion_jala.identity_service.utils.responses;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationModelErrorResponse extends BasicResponse {

  private List<FieldErrorValidationModel> errors;

  public ValidationModelErrorResponse() {
    this.errors = new ArrayList<>();
  }
}
