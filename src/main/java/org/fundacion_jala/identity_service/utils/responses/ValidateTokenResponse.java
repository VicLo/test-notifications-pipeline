package org.fundacion_jala.identity_service.utils.responses;

import java.util.UUID;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.identity_service.models.TokenPermissions;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;
import org.hibernate.annotations.Type;

@Getter
@Setter
@Builder
public class ValidateTokenResponse {

  public String accessToken;

  public String message;

  public long expiresIn;

  public String tokenType;

  public boolean success;

  @Type(type = TableConstant.TYPE_UUID)
  public UUID userId;

  public String role;

  public TokenPermissions permissions;
}
