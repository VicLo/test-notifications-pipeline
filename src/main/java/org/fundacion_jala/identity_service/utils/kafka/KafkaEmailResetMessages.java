package org.fundacion_jala.identity_service.utils.kafka;

public class KafkaEmailResetMessages {
  public static final String EMAIL_SUBJECT = "Email reset request";
  public static final String EMAIL_TYPE = "RESET_PASSWORD";
  public static final String EMAIL_LINK = "http://localhost:3000/reset-password/";
}
