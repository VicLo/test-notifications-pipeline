package org.fundacion_jala.identity_service.utils.speficitations.user;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.criteria.*;
import org.springframework.data.jpa.domain.Specification;

public class SpecificationUtil {

  private static final String FORMAT_PATTER = "%%%s%%";
  private static final String ROL_NAME_PROPERTY = "name";

  public static <T> Specification<T> getFilter(String filterField, String filterValue) {
    return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
      if (!filterField.isEmpty() && !filterValue.isEmpty()) {
        Join<Object, Object> joinParent = root.join(filterField);
        Expression<String> expression = joinParent.get(ROL_NAME_PROPERTY).as(String.class);
        return criteriaBuilder.equal(expression, filterValue);
      } else {
        return criteriaBuilder.conjunction();
      }
    };
  }

  public static <T> Specification<T> getSearch(String searchValue, List<String> fields) {
    return (Root<T> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) -> {
      List<Predicate> predicates = new ArrayList<>();

      for (String field : fields) {
        Expression<String> expression = root.get(field).as(String.class);
        predicates.add(criteriaBuilder.like(expression, String.format(FORMAT_PATTER, searchValue)));
      }

      if (predicates.isEmpty()) {
        return criteriaBuilder.conjunction();
      }

      Predicate[] predicatesList = new Predicate[predicates.size()];
      return criteriaBuilder.or(predicates.toArray(predicatesList));
    };
  }
}
