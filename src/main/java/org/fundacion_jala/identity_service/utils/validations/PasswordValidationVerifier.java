package org.fundacion_jala.identity_service.utils.validations;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.springframework.stereotype.Service;

@Service
public class PasswordValidationVerifier {

  public static boolean validatePassword(String password) {
    Pattern pattern = Pattern.compile(UserValidatorMessages.PASSWORD_PATTERN);
    Matcher matcher = pattern.matcher(password);
    return matcher.matches();
  }
}
