package org.fundacion_jala.identity_service.utils.validations;

public class BulkEditRolesValidator {

  public static final String EMPTY_IDS_OF_ROLES =
      "{validation.bulk_edit_roles_request.ids_of_roles.not_empty}";
  public static final String EMPTY_ACTIONS =
      "{validation.bulk_edit_roles_request.actions.not_empty}";
}
