package org.fundacion_jala.identity_service.utils.validations;

public class UserValidatorMessages {
  public static final String BLANK_FIRST_NAME = "{validation.user.firstName.not_blank}";
  public static final String BLANK_LAST_NAME = "{validation.user.lastName.not_blank}";
  public static final String LENGTH_NAME = "{validation.user.name.min_max}";
  public static final String INVALID_EMAIL = "{validation.user.email.invalid_format}";
  public static final String INVALID_PASSWORD = "{validation.user.password.invalid_format}";
  public static final String EMAIL_DOMAIN = "@(fundacion\\-jala\\.org)$";
  public static final String EMAIL_PATTERN = "^[A-Za-z]+\\.[a-zA-Z0-9]+" + EMAIL_DOMAIN;
  public static final String NAME_PATTERN = "^[a-zA-Z]{2,}$";
  public static final String PASSWORD_PATTERN =
      "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[*+,-./:;<=>@\\[\\]^_`])[A-Za-z0-9 *+,-./:;<=>@\\[\\]^_`]{10,}$";
}
