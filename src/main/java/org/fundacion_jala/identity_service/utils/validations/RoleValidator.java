package org.fundacion_jala.identity_service.utils.validations;

public class RoleValidator {
  public static final String BLANK_NAME = "{validation.role.name.not_blank}";
  public static final String LENGTH_NAME = "{validation.role.name.min_max}";
  public static final String BLANK_DESCRIPTION = "{validation.role.description.not_blank}";
  public static final String LENGTH_DESCRIPTION = "{validation.role.description.min_max}";
}
