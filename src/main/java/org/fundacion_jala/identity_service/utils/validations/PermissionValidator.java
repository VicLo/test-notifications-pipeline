package org.fundacion_jala.identity_service.utils.validations;

public class PermissionValidator {
  public static final String BLANK_NAME = "{validation.permission.name.not_blank}";
  public static final String LENGTH_NAME = "{validation.permission.name.min_max}";
  public static final String BLANK_DESCRIPTION = "{validation.permission.description.not_blank}";
  public static final String LENGTH_DESCRIPTION = "{validation.permission.description.min_max}";
}
