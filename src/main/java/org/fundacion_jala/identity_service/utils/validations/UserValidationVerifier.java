package org.fundacion_jala.identity_service.utils.validations;

import static org.fundacion_jala.identity_service.utils.constants.Defaults.COMMA;
import static org.fundacion_jala.identity_service.utils.constants.Defaults.POINT;
import static org.fundacion_jala.identity_service.utils.constants.Defaults.SPACE;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.repositories.RoleRepository;
import org.fundacion_jala.identity_service.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserValidationVerifier {

  private static final String NAME_FIELD_ERROR = "Incorrect name format.";
  private static final String EMAIL_FIELD_ERROR = "Incorrect email format.";
  private static final String UNIQUE_EMAIL_ERROR = "Email is already registered.";
  private static final String UNIQUE_ROLE_ERROR = "The user only must to have a role.";
  private static final String ROLE_FIELD_ERROR = "The given role '%s' does not exist.";
  private static final String ROLE_FIELD = "role";
  private static final String ERROR_MESSAGE = "Error in line %d: %s";
  private static final String ALLOWED_ROLES = "Allowed roles are:";
  private static final String EMAIL_FORMAT_SUGGESTION = "Allowed email format is: Name.Lastname@fundacion-jala.org";

  @Autowired
  private UserRepository userRepository;
  @Autowired
  private RoleRepository roleRepository;

  public boolean validateUser(User user) {
    return validateNames(user.getFirstName(), user.getLastName())
        && validateUniqueEmail(user.getEmail())
        && validateEmail(user.getEmail())
        && validateRole(user.getRoleName());
  }

  public boolean validateEmail(String email) {
    Pattern pattern = Pattern.compile(UserValidatorMessages.EMAIL_PATTERN);
    Matcher matcher = pattern.matcher(email);
    return matcher.matches();
  }

  public boolean validateNames(String firstName, String lastName) {
    Pattern pattern = Pattern.compile(UserValidatorMessages.NAME_PATTERN);
    Matcher matcherFirstName = pattern.matcher(firstName);
    Matcher matcherLastName = pattern.matcher(lastName);
    return matcherFirstName.matches() && matcherLastName.matches();
  }

  public boolean validateRole(String roleName) {
    return roleRepository.findAll().stream().anyMatch(role -> roleName.equals(role.getName()));
  }

  public boolean validateUniqueEmail(String email) {
    return userRepository.findByEmail(email) == null;
  }

  //TODO:From now on each user should only have one role, this for requirement of PO.
  public boolean validateUniqueRole(Set<Role> role) {
    return role.size() == 1;
  }

  public String buildInvalidUserMessage(User user, int userRow) {
    StringBuilder errors = new StringBuilder();
    if (!validateUniqueEmail(user.getEmail())) {
      errors.append(UNIQUE_EMAIL_ERROR).append(SPACE);
    }
    if (!validateEmail(user.getEmail())) {
      errors.append(EMAIL_FIELD_ERROR).append(SPACE);
    }
    if (!validateNames(user.getFirstName(), user.getLastName())) {
      errors.append(NAME_FIELD_ERROR).append(SPACE);
    }
    if (!validateRole(user.getRoleName())) {
      errors.append(String.format(ROLE_FIELD_ERROR, user.getRoleName()));
    }
    return dataError(userRow, errors.toString().trim());
  }

  public static String dataError(int userRow, String errorField) {
    StringBuilder errorMessage = new StringBuilder();
    errorMessage.append(String.format(ERROR_MESSAGE, userRow + 2, errorField));
    return errorMessage.toString();
  }

  public Set<String> getAllowedValues(List<String> dataErrors) {
    Set<String> suggestions = new HashSet<>();
    String roleSuggestions = buildRoleSuggestions();
    dataErrors.forEach(error -> {
      if (error.contains(ROLE_FIELD)) {
        suggestions.add(roleSuggestions);
      }
      if (error.contains(EMAIL_FIELD_ERROR)) {
        suggestions.add(EMAIL_FORMAT_SUGGESTION);
      }
    });
    return suggestions;
  }

  private String buildRoleSuggestions() {
    StringBuilder suggestions = new StringBuilder(ALLOWED_ROLES);
    suggestions.append(SPACE);
    roleRepository.findAll()
        .forEach(role -> suggestions.append(role.getName()).append(COMMA).append(SPACE));
    int lengthMessage = suggestions.length();
    suggestions.replace(lengthMessage - 2, lengthMessage, POINT);
    return suggestions.toString();
  }
}
