package org.fundacion_jala.identity_service.utils.validations;

import java.util.UUID;

public class UUIDValidator {

  public static boolean isUUID(String input) {
    try {
      UUID.fromString(input);
      return true;
    } catch (IllegalArgumentException exception) {
      return false;
    }
  }
}
