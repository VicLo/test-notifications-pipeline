package org.fundacion_jala.identity_service.utils.messages;

public class MessageConstant {

  public static final String REMOVE_SUCCESS = "Successfully removed";
  public static final String BULK_EDIT_ROLES_SUCCESS = "Roles edited in bulk successfully";
  public static final String BAD_CREDENTIALS_ERROR = "Email or password incorrect";
  public static final String ROLE_NOT_FOUND_ERROR = "Role not found";
  public static final String INVALID_TOKEN_ERROR = "Invalid token";
  public static final String LOGOUT_SUCCESS = "Logout successfully";
  public static final String INVALID_GET_BY_VALUE =
      "Invalid value entered, user id or fundacion-jala.org email supported";
}
