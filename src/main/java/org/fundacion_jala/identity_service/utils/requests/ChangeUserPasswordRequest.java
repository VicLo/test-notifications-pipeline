package org.fundacion_jala.identity_service.utils.requests;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ChangeUserPasswordRequest {

  private String code;

  private String newPassword;
}
