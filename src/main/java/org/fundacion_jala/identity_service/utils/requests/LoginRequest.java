package org.fundacion_jala.identity_service.utils.requests;

import javax.validation.constraints.NotNull;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {
  @NotNull(message = "{validation.login-request.email.not-blank}")
  public String email;

  @NotNull(message = "{validation.login-request.password.not-blank}")
  public String password;
}
