package org.fundacion_jala.identity_service.utils.requests;

import java.util.List;
import java.util.UUID;
import javax.validation.constraints.NotEmpty;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.utils.validations.BulkEditRolesValidator;

@Getter
@Setter
public class BulkEditRolesRequest {

  @NotEmpty(message = BulkEditRolesValidator.EMPTY_IDS_OF_ROLES)
  private List<UUID> idsOfRoles;

  @NotEmpty(message = BulkEditRolesValidator.EMPTY_ACTIONS)
  private List<RolePermission> actions;
}
