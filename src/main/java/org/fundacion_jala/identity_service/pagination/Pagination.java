package org.fundacion_jala.identity_service.pagination;

import java.util.Objects;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class Pagination {

  private int currentPage;
  private int totalPages;
  private int pageSize;
  private int totalCount;
  private boolean hasPreviousPage;
  private boolean hasNextPage;
  private int nextPageNumber;
  private int previousPageNumber;
  private String filterOption;
  private String filterValue;
  private String criteria;
  private String sort;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Pagination that = (Pagination) o;
    return currentPage == that.currentPage
        && totalPages == that.totalPages
        && pageSize == that.pageSize
        && totalCount == that.totalCount
        && hasPreviousPage == that.hasPreviousPage
        && hasNextPage == that.hasNextPage
        && nextPageNumber == that.nextPageNumber
        && previousPageNumber == that.previousPageNumber
        && Objects.equals(filterOption, that.filterOption)
        && Objects.equals(criteria, that.criteria)
        && Objects.equals(sort, that.sort);
  }

  @Override
  public int hashCode() {
    return Objects.hash(
        currentPage,
        totalPages,
        pageSize,
        totalCount,
        hasPreviousPage,
        hasNextPage,
        nextPageNumber,
        previousPageNumber,
        filterOption,
        criteria,
        sort);
  }
}
