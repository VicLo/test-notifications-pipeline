package org.fundacion_jala.identity_service.pagination;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class DataAndPagination {
  private Pagination pagination;
  private Iterable<?> data;
}
