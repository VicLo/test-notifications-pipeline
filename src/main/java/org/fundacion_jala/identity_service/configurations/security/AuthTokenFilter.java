package org.fundacion_jala.identity_service.configurations.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.UnsupportedJwtException;
import io.jsonwebtoken.security.SignatureException;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.http.HttpHeaders;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTConstants;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTMessages;
import org.fundacion_jala.identity_service.services.DenyUserTokenService;
import org.fundacion_jala.identity_service.utils.responses.ValidateTokenResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

public class AuthTokenFilter extends OncePerRequestFilter {

  private Logger logger = LoggerFactory.getLogger(AuthTokenFilter.class);

  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Autowired private JwtUtils jwtUtils;

  @Autowired private CustomUserDetailService userDetailsService;

  @Autowired private DenyUserTokenService denyUserTokenService;

  @Override
  protected void doFilterInternal(
      HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
      throws ServletException, IOException {
    String headerAuth = request.getHeader(HttpHeaders.AUTHORIZATION);
    String jwt = jwtUtils.parseJwt(headerAuth);
    try {
      if (jwt != null
          && jwtUtils.validateJwtToken(jwt)
          && !denyUserTokenService.existsById(jwtUtils.getJtiClaim(jwt))) {
        String username = jwtUtils.getUserNameFromJwtToken(jwt);
        UserDetails userDetails = userDetailsService.loadUserByUsername(username);
        UsernamePasswordAuthenticationToken authentication =
            new UsernamePasswordAuthenticationToken(
                userDetails, null, userDetails.getAuthorities());
        authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
        SecurityContextHolder.getContext().setAuthentication(authentication);
      }
    } catch (MalformedJwtException e) {
      logger.error("Invalid JWT token: {}", e.getMessage());
      setResponseForErrors(JWTMessages.INVALID_TOKEN, jwt, HttpStatus.FORBIDDEN.value(), response);
    } catch (ExpiredJwtException e) {
      logger.error("JWT token is expired: {}", e.getMessage());
      setResponseForErrors(
          JWTMessages.EXPIRED_TOKEN, jwt, HttpStatus.UNAUTHORIZED.value(), response);
    } catch (UnsupportedJwtException e) {
      logger.error("JWT token is unsupported: {}", e.getMessage());
      setResponseForErrors(
          JWTMessages.UNSUPPORTED_TOKEN, jwt, HttpStatus.FORBIDDEN.value(), response);
    } catch (SignatureException e) {
      logger.error("Invalid Signature: {}", e.getMessage());
      setResponseForErrors(
          JWTMessages.INVALID_SIGNATURE, jwt, HttpStatus.FORBIDDEN.value(), response);
    } catch (UsernameNotFoundException e) {
      logger.error("User not found: {}", e.getMessage());
      setResponseForErrors("User not found", jwt, HttpStatus.UNAUTHORIZED.value(), response);
    } finally {
      filterChain.doFilter(request, response);
    }
  }

  private void setResponseForErrors(
      String message, String token, int statusCode, HttpServletResponse response)
      throws IOException {
    response.setContentType(MediaType.APPLICATION_JSON.toString());
    response
        .getWriter()
        .write(
            MAPPER.writeValueAsString(
                ValidateTokenResponse.builder()
                    .accessToken(token)
                    .tokenType(JWTConstants.BEARER)
                    .expiresIn(0)
                    .success(false)
                    .message(message)
                    .build()));
    response.setStatus(statusCode);
  }
}
