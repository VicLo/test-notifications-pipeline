package org.fundacion_jala.identity_service.configurations.security.utils;

public class JWTPermissionsClaims {
  public static final String ADD = "add";
  public static final String EDIT = "edit";
  public static final String DELETE = "delete";
  public static final String VIEW = "view";
}
