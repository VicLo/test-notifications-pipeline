package org.fundacion_jala.identity_service.configurations.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.util.Arrays;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.fundacion_jala.identity_service.utils.messages.MessageConstant;
import org.fundacion_jala.identity_service.utils.responses.AuthResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

@Component
public class AuthEntryPointJwt implements AuthenticationEntryPoint {

  private static final ObjectMapper MAPPER = new ObjectMapper();

  @Override
  public void commence(
      HttpServletRequest request,
      HttpServletResponse response,
      AuthenticationException authException)
      throws IOException, ServletException {
    AuthResponse body =
        AuthResponse.builder()
            .message(authException.getMessage())
            .error(Arrays.asList(MessageConstant.INVALID_TOKEN_ERROR))
            .success(false)
            .build();
    response.setContentType(MediaType.APPLICATION_JSON.toString());
    int currentStatus = response.getStatus();
    if (currentStatus == 200) {
      response.setStatus(HttpStatus.UNAUTHORIZED.value());
      response.getWriter().write(MAPPER.writeValueAsString(body));
    }
  }
}
