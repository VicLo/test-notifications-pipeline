package org.fundacion_jala.identity_service.configurations.security.utils;

public class JWTClaims {
  public static final String NAME = "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name";
  public static final String NAME_IDENTIFIER =
      "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier";
  public static final String JTI = "jti";
  public static final String ROLE = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role";
  public static final String TYP = "typ";
}
