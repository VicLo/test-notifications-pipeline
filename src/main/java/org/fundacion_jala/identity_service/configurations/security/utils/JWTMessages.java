package org.fundacion_jala.identity_service.configurations.security.utils;

public class JWTMessages {
  public static final String VALID_TOKEN = "Token validation has been successfully";
  public static final String INVALID_TOKEN = "Invalid token";
  public static final String EXPIRED_TOKEN = "Token expired";
  public static final String UNSUPPORTED_TOKEN = "Unsupported token";
  public static final String EMPTY_CLAIMS = "Claims are empty";
  public static final String NULL_TOKEN = "Token cannot be null";
  public static final String INVALID_SIGNATURE = "Invalid token signature";
}
