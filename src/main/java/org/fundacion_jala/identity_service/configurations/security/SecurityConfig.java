package org.fundacion_jala.identity_service.configurations.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.fundacion_jala.identity_service.utils.constants.AuthorizedURL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Service;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

@Service
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

  @Value("${react.origin.url}")
  private String reactOriginUrl;

  public static final List<String> METHODS =
      new ArrayList<>(
          Arrays.asList(
              HttpMethod.GET.name(),
              HttpMethod.POST.name(),
              HttpMethod.PUT.name(),
              HttpMethod.DELETE.name(),
              HttpMethod.PATCH.name()));

  public static final String ALL_HEADERS = "*";

  public static final String REGISTER_CORS_CONFIGURATION_PATTERN = "/**";

  @Autowired private CustomUserDetailService userDetailService;

  @Autowired private AuthEntryPointJwt unauthorizedHandler;

  @Bean
  public AuthTokenFilter authenticationJwtTokenFilter() {
    return new AuthTokenFilter();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.authenticationProvider(authenticationProvider());
  }

  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }

  @Override
  @Bean
  public AuthenticationManager authenticationManagerBean() throws Exception {
    return super.authenticationManagerBean();
  }

  @Bean
  public DaoAuthenticationProvider authenticationProvider() {
    DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
    provider.setPasswordEncoder(passwordEncoder());
    provider.setUserDetailsService(userDetailService);
    return provider;
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.cors()
        .and()
        .csrf()
        .disable()
        .exceptionHandling()
        .authenticationEntryPoint(unauthorizedHandler)
        .and()
        .sessionManagement()
        .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
        .and()
        .authorizeRequests()
        .antMatchers(
            AuthorizedURL.LOGIN,
            AuthorizedURL.CHECK_EMAIL,
            AuthorizedURL.CHANGE_PASSWORD,
            AuthorizedURL.CHECK_TOKEN,
            AuthorizedURL.ROLES)
        .permitAll()
        .antMatchers(
            AuthorizedURL.SWAGGER_UI,
            AuthorizedURL.SWAGGER_HTML,
            AuthorizedURL.SWAGGER_VERSION,
            AuthorizedURL.SWAGGER_RESOURCES)
        .permitAll()
        .anyRequest()
        .authenticated();

    http.addFilterBefore(
        authenticationJwtTokenFilter(), UsernamePasswordAuthenticationFilter.class);
  }

  @Bean
  public CorsConfigurationSource corsConfigurationSource() {
    CorsConfiguration configuration = new CorsConfiguration();
    configuration.addAllowedOrigin(reactOriginUrl);
    configuration.addAllowedHeader(ALL_HEADERS);
    configuration.setAllowedMethods(METHODS);
    UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
    source.registerCorsConfiguration(REGISTER_CORS_CONFIGURATION_PATTERN, configuration);
    return source;
  }
}
