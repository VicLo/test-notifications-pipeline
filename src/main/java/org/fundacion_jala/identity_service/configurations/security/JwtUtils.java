package org.fundacion_jala.identity_service.configurations.security;

import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.crypto.SecretKey;
import org.apache.commons.lang.StringUtils;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTClaims;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTPermissionsClaims;
import org.fundacion_jala.identity_service.models.TokenPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class JwtUtils {

  private static final String PROGRAMS = "Programs";
  private static final String LOGS = "logs";
  private static final String SCHOLAR_EVENTS = "Scholar-events";
  private static final String SCHOLAR_EVALUATION = "Scholar-evaluation";
  private static final String SCHOLAR_PROFILE = "Scholar-profile";
  private static final String SCHOLAR_STATUS = "Scholar-status";
  private static final String SUBJECT = "Subject";

  public static final String BEARER = "Bearer ";

  private static final String TYPE_JWT = "JWT";

  @Value("${applicant-tracking.jwt.secret}")
  private String jwtSecret;

  @Value("${applicant-tracking.jwt.expiration-in-milliseconds}")
  private int jwtExpirationMs;

  @Value("${applicant-tracking.jwt.issuer}")
  private String jwtIssuer;

  @Autowired private TokenPermissions tokenPermissions;

  public String generateJwtToken(Authentication authentication) {
    CustomUserDetail userPrincipal = (CustomUserDetail) authentication.getPrincipal();
    Date date = new Date();
    Iterator<? extends GrantedAuthority> roleIterable = userPrincipal.getAuthorities().iterator();
    String role = roleIterable.hasNext() ? roleIterable.next().getAuthority() : StringUtils.EMPTY;
    Map<String, Object> permissionsClaims = getPermissions(userPrincipal);
    SecretKey key = getSecretKey();
    return Jwts.builder()
        .setHeaderParam(JWTClaims.TYP, TYPE_JWT)
        .setSubject(userPrincipal.getId().toString())
        .claim(JWTClaims.NAME, userPrincipal.getUsername())
        .claim(JWTClaims.JTI, UUID.randomUUID().toString())
        .claim(JWTClaims.NAME_IDENTIFIER, userPrincipal.getId().toString())
        .claim(JWTClaims.ROLE, role)
        .addClaims(permissionsClaims)
        .setIssuer(jwtIssuer)
        .setAudience(jwtIssuer)
        .setExpiration(new Date(date.getTime() + jwtExpirationMs))
        .signWith(key, SignatureAlgorithm.HS256)
        .compact();
  }

  public Map<String, Object> getPermissions(CustomUserDetail userPrincipal) {
    Map<String, Object> response = new HashMap<>();
    userPrincipal.getRoles().stream()
        .findFirst()
        .get()
        .getActions()
        .forEach(
            action -> {
              List<String> permissionsList = new ArrayList<>();
              String claimType = action.getPermissions().getName();
              if (action.getCanView()) {
                permissionsList.add(JWTPermissionsClaims.VIEW);
              }
              if (action.getCanAdd()) {
                permissionsList.add(JWTPermissionsClaims.ADD);
              }
              if (action.getCanEdit()) {
                permissionsList.add(JWTPermissionsClaims.EDIT);
              }
              if (action.getCanDelete()) {
                permissionsList.add(JWTPermissionsClaims.DELETE);
              }
              response.put(claimType, permissionsList);
            });
    return response;
  }

  public String getUserNameFromJwtToken(String token) {
    return (String)
        Jwts.parserBuilder()
            .setSigningKey(getSecretKey())
            .build()
            .parseClaimsJws(token)
            .getBody()
            .get(JWTClaims.NAME);
  }

  public UUID getJtiClaim(String token) {
    return UUID.fromString(
        Jwts.parserBuilder()
            .setSigningKey(getSecretKey())
            .build()
            .parseClaimsJws(token)
            .getBody()
            .getId());
  }

  public Date getExpClaim(String token) {
    return Jwts.parserBuilder()
        .setSigningKey(getSecretKey())
        .build()
        .parseClaimsJws(token)
        .getBody()
        .getExpiration();
  }

  public UUID getSubClaim(String token) {
    return UUID.fromString(
        Jwts.parserBuilder()
            .setSigningKey(getSecretKey())
            .build()
            .parseClaimsJws(token)
            .getBody()
            .getSubject());
  }

  public List<String> getActionFromJwtToken(String token, String key) {
    return (List<String>)
        Jwts.parserBuilder()
            .setSigningKey(getSecretKey())
            .build()
            .parseClaimsJws(token)
            .getBody()
            .get(key);
  }

  public String getClaimFromJwtToken(String token, String key) {
    return (String)
        Jwts.parserBuilder()
            .setSigningKey(getSecretKey())
            .build()
            .parseClaimsJws(token)
            .getBody()
            .get(key);
  }

  public boolean validateJwtToken(String authToken) {
    SecretKey key = getSecretKey();
    Jwts.parserBuilder().setSigningKey(key).build().parseClaimsJws(authToken);
    return true;
  }

  private SecretKey getSecretKey() {
    return Keys.hmacShaKeyFor(jwtSecret.getBytes(StandardCharsets.UTF_8));
  }

  public String parseJwt(String token) {
    if (org.springframework.util.StringUtils.hasText(token) && token.startsWith(BEARER)) {
      return token.substring(7);
    }
    return null;
  }

  public TokenPermissions getPermissionsFromJwtToken(String token) {
    tokenPermissions.setPrograms(getActionFromJwtToken(token, PROGRAMS));
    tokenPermissions.setLogs(getActionFromJwtToken(token, LOGS));
    tokenPermissions.setScholarEvaluation(getActionFromJwtToken(token, SCHOLAR_EVALUATION));
    tokenPermissions.setScholarEvents(getActionFromJwtToken(token, SCHOLAR_EVENTS));
    tokenPermissions.setScholarProfile(getActionFromJwtToken(token, SCHOLAR_PROFILE));
    tokenPermissions.setScholarStatus(getActionFromJwtToken(token, SCHOLAR_STATUS));
    tokenPermissions.setSubject(getActionFromJwtToken(token, SUBJECT));

    return tokenPermissions;
  }
}
