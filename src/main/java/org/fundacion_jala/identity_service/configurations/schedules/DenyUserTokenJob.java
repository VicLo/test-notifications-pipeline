package org.fundacion_jala.identity_service.configurations.schedules;

import lombok.extern.slf4j.Slf4j;
import org.fundacion_jala.identity_service.services.DenyUserTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class DenyUserTokenJob {

  @Autowired private DenyUserTokenService denyUserTokenService;

  public void execute() {
    denyUserTokenService.deleteAll();
    log.debug("Deleting user tokens expired");
  }
}
