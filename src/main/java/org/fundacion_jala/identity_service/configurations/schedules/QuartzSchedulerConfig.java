package org.fundacion_jala.identity_service.configurations.schedules;

import lombok.extern.slf4j.Slf4j;
import org.quartz.Trigger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.quartz.*;

@Slf4j
@Configuration
public class QuartzSchedulerConfig {

  private static final String JOB_DETAIL_NAME = "denyUserTokenDeleteJob";
  private static final String TRIGGER_NAME = "denyUserTokenDeleteTrigger";
  public static final String JOB_DETAIL_TARGET_METHOD = "execute";

  @Value("${org.quartz.trigger.cron-expression}")
  private String CRON_EXPRESSION;

  @Bean
  public MethodInvokingJobDetailFactoryBean jobDetail(DenyUserTokenJob denyUserTokenJob) {
    MethodInvokingJobDetailFactoryBean jobDetail = new MethodInvokingJobDetailFactoryBean();
    jobDetail.setConcurrent(false);
    jobDetail.setName(JOB_DETAIL_NAME);
    jobDetail.setTargetObject(denyUserTokenJob);
    jobDetail.setTargetMethod(JOB_DETAIL_TARGET_METHOD);
    log.info("Job detail of user token deny initialized");
    return jobDetail;
  }

  @Bean
  public CronTriggerFactoryBean jobTrigger(MethodInvokingJobDetailFactoryBean jobDetail) {
    CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
    trigger.setJobDetail(jobDetail.getObject());
    trigger.setCronExpression(CRON_EXPRESSION);
    trigger.setName(TRIGGER_NAME);
    log.info("Job trigger of user token deny initialized");
    return trigger;
  }

  @Bean
  public SchedulerFactoryBean scheduler(Trigger trigger) {
    SchedulerFactoryBean schedulerFactoryBean = new SchedulerFactoryBean();
    schedulerFactoryBean.setOverwriteExistingJobs(true);
    schedulerFactoryBean.setStartupDelay(1);
    schedulerFactoryBean.setTriggers(trigger);
    log.info("Scheduler of user token deny initialized");
    return schedulerFactoryBean;
  }
}
