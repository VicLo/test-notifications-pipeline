package org.fundacion_jala.identity_service.configurations;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.ApiKey;
import springfox.documentation.service.AuthorizationScope;
import springfox.documentation.service.SecurityReference;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {

  private static final String API_KEY_NAME = "Bearer";
  private static final String KEY_NAME = "Authorization";
  private static final String PASS_AS = "header";

  private static final String CONTROLLERS_PACKAGE =
      "org.fundacion_jala.identity_service.controllers";
  private static final String TITLE = "Identity Service";
  private static final String DESCRIPTION = "Applicants tracking from Foundation Jala";
  private static final String VERSION = "v1.0";

  private static final String SCOPE_AUTH = "global";
  private static final String DESCRIPTION_AUTH = "accessEverything";
  private static final String SECURITY_REFERENCE = "JWT";

  private ApiKey apiKey() {
    return new ApiKey(API_KEY_NAME, KEY_NAME, PASS_AS);
  }

  private SecurityContext securityContext() {
    return SecurityContext.builder().securityReferences(defaultAuth()).build();
  }

  private List<SecurityReference> defaultAuth() {
    AuthorizationScope authorizationScope = new AuthorizationScope(SCOPE_AUTH, DESCRIPTION_AUTH);
    AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
    authorizationScopes[0] = authorizationScope;
    return Arrays.asList(new SecurityReference(SECURITY_REFERENCE, authorizationScopes));
  }

  private ApiInfo apiInfo() {
    return new ApiInfo(
        TITLE, DESCRIPTION, VERSION, null, null, null, null, Collections.emptyList());
  }

  @Bean
  public Docket api() {
    return new Docket(DocumentationType.SWAGGER_2)
        .securityContexts(Arrays.asList(securityContext()))
        .securitySchemes(Arrays.asList(apiKey()))
        .select()
        .apis(RequestHandlerSelectors.basePackage(CONTROLLERS_PACKAGE))
        .paths(PathSelectors.any())
        .build()
        .apiInfo(apiInfo());
  }
}
