package org.fundacion_jala.identity_service.configurations.kafka_config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class KafkaTopicConfig {

  @Value("${spring.kafka.topic.producer.name}")
  private String emailResetTopicName;

  @Bean
  public NewTopic topicBuilder() {
    return TopicBuilder.name(emailResetTopicName).build();
  }
}
