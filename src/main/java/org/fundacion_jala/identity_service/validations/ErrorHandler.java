package org.fundacion_jala.identity_service.validations;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.apache.commons.lang.StringUtils;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTConstants;
import org.fundacion_jala.identity_service.configurations.security.utils.JWTMessages;
import org.fundacion_jala.identity_service.utils.exceptions.*;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.messages.MessageConstant;
import org.fundacion_jala.identity_service.utils.responses.AuthResponse;
import org.fundacion_jala.identity_service.utils.responses.FieldErrorValidationModel;
import org.fundacion_jala.identity_service.utils.responses.ValidateTokenResponse;
import org.fundacion_jala.identity_service.utils.responses.ValidationModelErrorResponse;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
public class ErrorHandler {

  private static final String CLEAN_MESSAGE_FIELD_ERROR_REGEX = "\"";
  private static final String ATTRIBUTE_NAME_MESSAGE_ERROR = "error";
  private static final String ATTRIBUTE_EMAIL = "email";
  private static final String ATTRIBUTE_ROLE = "role_name";

  @ExceptionHandler(MethodArgumentNotValidException.class)
  public ResponseEntity<ValidationModelErrorResponse> handleMethodArgumentNotValid(
      MethodArgumentNotValidException exception) {
    List<FieldErrorValidationModel> errors =
        exception.getBindingResult().getAllErrors().stream()
            .map(
                objectError -> {
                  FieldErrorValidationModel fieldError = new FieldErrorValidationModel();
                  fieldError.setField(((FieldError) objectError).getField());
                  fieldError.setCode(objectError.getCode());
                  fieldError.setMessage(
                      objectError
                          .getDefaultMessage()
                          .replaceAll(CLEAN_MESSAGE_FIELD_ERROR_REGEX, StringUtils.EMPTY));
                  return fieldError;
                })
            .collect(Collectors.toList());
    ValidationModelErrorResponse body = new ValidationModelErrorResponse();
    body.setMessage(exception.getMessage());
    body.setErrors(errors);
    return ResponseEntity.badRequest().body(body);
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public ResponseEntity<Map<String, Object>> handleEntityNotFoundException(
      EntityNotFoundException exception) {
    Map<String, Object> body = new HashMap<>();
    if (exception.getField() != null && exception.getField().equals(ATTRIBUTE_EMAIL)) {
      body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, MessageConstant.BAD_CREDENTIALS_ERROR);
      return ResponseEntity.badRequest().body(body);
    }
    if (exception.getField() != null && exception.getField().equals(ATTRIBUTE_ROLE)) {
      body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, MessageConstant.ROLE_NOT_FOUND_ERROR);
      return ResponseEntity.badRequest().body(body);
    }
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
  }

  @ExceptionHandler(UnsupportedMultipleRoleException.class)
  public ResponseEntity<Map<String, Object>> handleUnsupportedMultipleRoleException(
      UnsupportedMultipleRoleException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(javax.persistence.EntityNotFoundException.class)
  public ResponseEntity<Map<String, Object>> handleJavaxEntityNotFoundException(
      javax.persistence.EntityNotFoundException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.NOT_FOUND).body(body);
  }

  @ExceptionHandler(SQLException.class)
  public ResponseEntity<Map<String, Object>> handleSQLException(SQLException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(BadCredentialsException.class)
  public ResponseEntity<AuthResponse> handleBadCredentialsException(
      BadCredentialsException exception) {
    return ResponseEntity.status(HttpStatus.BAD_REQUEST)
        .body(
            AuthResponse.builder()
                .message(exception.getMessage())
                .error(Arrays.asList(MessageConstant.BAD_CREDENTIALS_ERROR))
                .success(false)
                .build());
  }

  @ExceptionHandler(DataIntegrityViolationException.class)
  public ResponseEntity<FieldErrorValidationModel> handleDuplicateKeyException(
      DataIntegrityViolationException exception) {
    FieldErrorValidationModel body = new FieldErrorValidationModel();

    if (exception.getClass().isAssignableFrom(DuplicateKeyException.class)) {
      body.setMessage(exception.getCause().getCause().getMessage());
      body.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
      body.setField("name");
    } else {
      body.setMessage(exception.getMessage());
      body.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
    }
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(IllegalArgumentException.class)
  public ResponseEntity<ValidateTokenResponse> handleIllegalArgumentException(
      IllegalArgumentException exception) {
    return ResponseEntity.badRequest()
        .body(
            ValidateTokenResponse.builder()
                .accessToken(null)
                .tokenType(JWTConstants.BEARER)
                .message(JWTMessages.NULL_TOKEN)
                .expiresIn(0)
                .success(false)
                .build());
  }

  @ExceptionHandler(ValidationCodeIncorrectException.class)
  public ResponseEntity<Map<String, Object>> handleValidationCodeIncorrectException(
      ValidationCodeIncorrectException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(InvalidTokenException.class)
  public ResponseEntity<Map<String, Object>> handleInvalidTokenException(
      InvalidTokenException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(ResetPasswordCodeUsedException.class)
  public ResponseEntity<Map<String, Object>> handleResetPasswordCodeUsedException(
      ResetPasswordCodeUsedException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(InvalidUserPasswordFormatException.class)
  public ResponseEntity<Map<String, Object>> handleInvalidPasswordFormatException(
      InvalidUserPasswordFormatException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(body);
  }

  @ExceptionHandler(SendResetPasswordCodeEmailServerException.class)
  public ResponseEntity<Map<String, Object>> handleMicroserviceConnectionException(
      SendResetPasswordCodeEmailServerException exception) {
    Map<String, Object> body = new HashMap<>();
    body.put(ATTRIBUTE_NAME_MESSAGE_ERROR, exception.getMessage());
    return ResponseEntity.status(HttpStatus.SERVICE_UNAVAILABLE).body(body);
  }
}
