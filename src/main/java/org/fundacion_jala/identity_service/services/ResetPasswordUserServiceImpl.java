package org.fundacion_jala.identity_service.services;

import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.fundacion_jala.identity_service.repositories.ResetPasswordUserRepository;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ResetPasswordUserServiceImpl implements ResetPasswordUserService {

  @Autowired private ResetPasswordUserRepository repository;

  @Override
  public ResetPasswordUser getByToken(String token) {
    return repository.findByToken(token).orElse(null);
  }

  @Override
  public ResetPasswordUser save(ResetPasswordUser resetPasswordUser) {
    return repository.save(resetPasswordUser);
  }

  @Override
  public ResetPasswordUser invalidateTokenCode(UUID id) {
    Optional<ResetPasswordUser> resetPasswordUserFound = repository.findById(id);
    if (resetPasswordUserFound.isPresent()) {
      ResetPasswordUser updatedPasswordUser = resetPasswordUserFound.get();
      updatedPasswordUser.setUsed(true);
      return repository.save(updatedPasswordUser);
    }
    throw new EntityNotFoundException("Reset password user", id);
  }
}
