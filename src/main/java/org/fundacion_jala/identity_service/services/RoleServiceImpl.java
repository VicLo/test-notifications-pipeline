package org.fundacion_jala.identity_service.services;

import java.util.Collections;
import java.util.Optional;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.pagination.Pagination;
import org.fundacion_jala.identity_service.repositories.RoleRepository;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.messages.MessageConstant;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements EntityService<Role> {

  private final String TYPE = "ROLE";
  private final String NAME_FIELD = "role_name";

  @Autowired private RoleRepository roleRepository;

  @Autowired private RolePermissionService rolePermissionService;

  @Override
  public DataAndPagination getAllPagination(Integer pageNo, Integer pageSize, String sortBy) {
    int pageNoCorrection = pageNo - 1;
    Pageable pageable = PageRequest.of(pageNoCorrection, pageSize, Sort.by(sortBy));
    Page<Role> page = roleRepository.findByDeleted(pageable, false);
    int totalPages = page.getTotalPages();
    int nextPageNumber = page.hasNext() ? pageNo + 1 : totalPages;
    int previousPageNumber = page.hasPrevious() ? pageNo - 1 : 0;
    Pagination generatedPagination =
        Pagination.builder()
            .currentPage(pageNo)
            .pageSize(pageSize)
            .totalPages(page.getTotalPages())
            .totalCount((int) page.getTotalElements())
            .hasPreviousPage(page.hasPrevious())
            .hasNextPage(page.hasNext())
            .nextPageNumber(nextPageNumber)
            .previousPageNumber(previousPageNumber)
            .build();
    if (page.hasContent()) {
      return DataAndPagination.builder()
          .pagination(generatedPagination)
          .data(page.getContent())
          .build();
    }
    return DataAndPagination.builder()
        .pagination(generatedPagination)
        .data(Collections.EMPTY_LIST)
        .build();
  }

  @Override
  public Iterable<Role> getAll() {
    return roleRepository.findAll();
  }

  @Override
  public Role save(Role entity) {
    Role newRole = roleRepository.save(entity);

    for (RolePermission rolePermission : entity.getActions()) {
      rolePermissionService.save(newRole.getId(), rolePermission);
    }
    return newRole;
  }

  @Override
  public Role update(UUID id, Role entity) {
    Optional<Role> currentRole = roleRepository.findById(id);
    if (currentRole.isPresent()) {
      Role updatedRole = currentRole.get();
      updatedRole.setName(entity.getName());
      updatedRole.setDescription(entity.getDescription());
      rolePermissionService.saveAll(entity.getActions());
      return roleRepository.save(updatedRole);
    }
    throw new EntityNotFoundException(TYPE, id);
  }

  @Override
  public BasicResponse delete(UUID id) {
    if (roleRepository.existsById(id)) {
      roleRepository.deleteById(id);
      BasicResponse response = new BasicResponse();
      response.setMessage(MessageConstant.REMOVE_SUCCESS);
      return response;
    }
    throw new EntityNotFoundException(TYPE, id);
  }

  @Override
  public Role getById(UUID id) {
    Optional<Role> role = roleRepository.findById(id);
    if (role.isPresent()) {
      return role.get();
    }
    throw new EntityNotFoundException(TYPE, id);
  }

  @Override
  public Role getByName(String name) {
    if (roleRepository.existsByName(name)) {
      return roleRepository.findRoleByName(name);
    }
    throw new EntityNotFoundException(TYPE, NAME_FIELD, name);
  }
}
