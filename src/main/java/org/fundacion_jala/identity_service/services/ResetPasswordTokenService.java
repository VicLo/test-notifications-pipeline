package org.fundacion_jala.identity_service.services;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;
import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import lombok.extern.slf4j.Slf4j;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.fundacion_jala.identity_service.utils.exceptions.MACAlgorithmInitException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.codec.Hex;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ResetPasswordTokenService implements TokenService<ResetPasswordUser> {

  public static final String HMAC_SHA_256 = "HmacSHA256";
  public static final String[] PAYLOAD_FIELDS = new String[] {"email", "expirationTime"};
  public static final String SEPARATOR = ":";

  private String secret;
  private int expirationTime;
  private Mac mac;
  private SecretKeySpec secretKeySpec;

  public ResetPasswordTokenService(
      @Value("${applicant-tracking.reset-password.token.secret}") String secret,
      @Value("${applicant-tracking.reset-password.token.expiration-in-milliseconds}")
          int expirationTime) {
    this.secret = secret;
    this.expirationTime = expirationTime;
    try {
      this.mac = Mac.getInstance(HMAC_SHA_256);
      this.secretKeySpec = new SecretKeySpec(secret.getBytes(StandardCharsets.UTF_8), HMAC_SHA_256);
      this.mac.init(this.secretKeySpec);
    } catch (NoSuchAlgorithmException | InvalidKeyException exception) {
      throw new MACAlgorithmInitException(exception.getMessage());
    }
  }

  @Override
  public String generateToken(ResetPasswordUser data) {
    Date currentDate = new Date();
    Date expirationDate = new Date(currentDate.getTime() + expirationTime);
    log.info("Generating reset password token to {}", data.getEmail());

    String hash = getHash(data.getUserId().toString() + data.getEmail() + expirationDate.getTime());
    return Base64.getEncoder()
        .encodeToString(
            new StringBuilder()
                .append(data.getEmail())
                .append(SEPARATOR)
                .append(expirationDate.getTime())
                .append(SEPARATOR)
                .append(hash)
                .toString()
                .getBytes(StandardCharsets.UTF_8));
  }

  @Override
  public boolean isValidToken(String token, ResetPasswordUser data) {
    try {
      Map<String, Object> payload = buildPayload(token);
      log.info("Validating reset password token of {}", payload.get(PAYLOAD_FIELDS[0]));
      return isValidToken(token, data, payload);
    } catch (NumberFormatException exception) {
      log.warn(
          "Error during the validation of token with error message {}",
          exception.getMessage(),
          exception);
      return false;
    }
  }

  @Override
  public Map<String, Object> getPayload(String token) {
    return buildPayload(token);
  }

  private boolean isValidToken(String token, ResetPasswordUser data, Map<String, Object> payload) {
    String hashToken = decodeBase64Token(token).split(SEPARATOR)[2];
    Date now = new Date();
    Date expirationDate = (Date) payload.get(PAYLOAD_FIELDS[1]);
    String hash = getHash(data.getUserId().toString() + data.getEmail() + expirationDate.getTime());
    return MessageDigest.isEqual(
            hash.getBytes(StandardCharsets.UTF_8), hashToken.getBytes(StandardCharsets.UTF_8))
        && now.before(expirationDate);
  }

  private Map<String, Object> buildPayload(String token) {
    String[] tokenParts = decodeBase64Token(token).split(SEPARATOR);
    Map<String, Object> payload = new HashMap<>();
    payload.put(PAYLOAD_FIELDS[0], tokenParts[0]);
    payload.put(PAYLOAD_FIELDS[1], new Date(Long.parseLong(tokenParts[1])));
    return payload;
  }

  private String getHash(String payload) {
    return new String(Hex.encode(this.mac.doFinal(payload.getBytes())));
  }

  private String decodeBase64Token(String token) {
    return new String(Base64.getDecoder().decode(token));
  }
}
