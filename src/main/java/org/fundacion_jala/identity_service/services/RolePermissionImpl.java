package org.fundacion_jala.identity_service.services;

import java.util.Set;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.repositories.RolePermissionRepository;
import org.fundacion_jala.identity_service.utils.helpers.RolePermissionHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RolePermissionImpl implements RolePermissionService {

  @Autowired RolePermissionRepository rolePermissionRepository;

  @Override
  public RolePermission save(UUID roleId, RolePermission entity) {
    return rolePermissionRepository.save(RolePermissionHelper.buildRolePermission(roleId, entity));
  }

  @Override
  public void saveAll(Set<RolePermission> actions) {
    rolePermissionRepository.saveAll(actions);
  }
}
