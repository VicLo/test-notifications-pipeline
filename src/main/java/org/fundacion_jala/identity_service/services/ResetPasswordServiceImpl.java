package org.fundacion_jala.identity_service.services;

import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.models.kafka.EmailNotificationDTO;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;
import org.fundacion_jala.identity_service.utils.constants.EmailConstant;
import org.fundacion_jala.identity_service.utils.exceptions.InvalidTokenException;
import org.fundacion_jala.identity_service.utils.exceptions.ResetPasswordCodeUsedException;
import org.fundacion_jala.identity_service.utils.exceptions.ValidationCodeIncorrectException;
import org.fundacion_jala.identity_service.utils.helpers.BodyGeneratorHelper;
import org.fundacion_jala.identity_service.utils.helpers.EntityBuilderHelper;
import org.fundacion_jala.identity_service.utils.kafka.KafkaEmailResetMessages;
import org.fundacion_jala.identity_service.utils.requests.ChangeUserPasswordRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.fundacion_jala.identity_service.utils.responses.CheckEmailResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaProducerException;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ResetPasswordServiceImpl implements ResetPasswordService {

  @Autowired private BodyGeneratorHelper bodyGeneratorHelper;

  @Autowired private ResetPasswordUserService resetPasswordUserService;

  @Autowired private UserServiceImpl userService;

  @Autowired private EntityBuilderHelper entityBuilderHelper;

  @Autowired private TokenService tokenService;

  @Autowired private KafkaProducerService kafkaProducerService;

  @Value("${notifications-service.url}")
  private String notificationsServiceUrl;

  @Value("${notifications-service.name}")
  private String notificationsServiceName;

  @Override
  public CheckEmailResponse verifyEmail(String email) {
    log.info("Searching user with email {}", email);
    User userFound = userService.getByEmail(email);
    ResetPasswordUser resetPasswordUser = entityBuilderHelper.buildResetPasswordUser(userFound);
    sendCodeByEmail(resetPasswordUser);
    resetPasswordUserService.save(resetPasswordUser);
    log.info("Token generated and sending email for the user {}", userFound.getId());
    return CheckEmailResponse.builder().token(resetPasswordUser.getToken()).build();
  }

  @Override
  public BasicResponse resetPassword(
      String token, ChangeUserPasswordRequest changeUserPasswordRequest) {
    ResetPasswordUser resetPasswordUser = resetPasswordUserService.getByToken(token);
    validateTokenCode(resetPasswordUser, token, changeUserPasswordRequest.getCode());
    log.info("Valid to change Password of the user with email {}", resetPasswordUser.getEmail());
    String message =
        userService.updatePassword(
            resetPasswordUser.getUserId(), changeUserPasswordRequest.getNewPassword());
    resetPasswordUserService.invalidateTokenCode(resetPasswordUser.getId());
    return entityBuilderHelper.buildBasicResponse(message);
  }

  private void sendCodeByEmail(ResetPasswordUser resetPasswordUser) {
    log.info("Attempting to send Email Notification to Kafka Broker");
    try {
      kafkaProducerService.publishMessage(buildEmailNotificationDTO(resetPasswordUser));
    } catch (KafkaProducerException exception) {
      log.info("Kafka Producer Exception: ");
      exception.printStackTrace();
    }
    log.info("Email Notification sent to Kafka Broker successfully");
  }

  /**
   * Validate data for the reset password checking sent code and token
   *
   * @param resetPasswordUser object with the data to validate
   * @param token token sent by the user
   * @param code user sent code
   */
  private void validateTokenCode(ResetPasswordUser resetPasswordUser, String token, String code) {
    log.info("Checking if the token is valid");
    if (resetPasswordUser == null || !tokenService.isValidToken(token, resetPasswordUser)) {
      throw new InvalidTokenException();
    }
    log.info("Checking if the user sent code with email {} is valid", resetPasswordUser.getEmail());
    if (!code.equals(resetPasswordUser.getCode())) {
      throw new ValidationCodeIncorrectException();
    }
    log.info("Checking if the user sent code has already been used");
    if (resetPasswordUser.isUsed()) {
      throw new ResetPasswordCodeUsedException();
    }
  }

  /**
   * Validate user token to show the pertinent error messages
   *
   * @param token token sent by the user
   * @return BasicResponse, the response with the validated message
   */
  @Override
  public BasicResponse validateToken(String token) {
    log.info("Checking if the the token is valid");
    ResetPasswordUser resetPasswordUser = resetPasswordUserService.getByToken(token);
    if (!tokenService.isValidToken(token, resetPasswordUser)) {
      throw new InvalidTokenException();
    }
    log.info("Checking if the user sent code has already been used");
    if (resetPasswordUser.isUsed()) {
      throw new ResetPasswordCodeUsedException();
    }
    return entityBuilderHelper.buildBasicResponse("Token validate, ready to reset password");
  }

  private EmailNotificationDTO buildEmailNotificationDTO(ResetPasswordUser resetPasswordUser) {
    log.info("Building Email Notification DTO for user password reset");
    EmailNotificationDTO emailResetProcess = new EmailNotificationDTO();
    emailResetProcess.setSubject(KafkaEmailResetMessages.EMAIL_SUBJECT);
    emailResetProcess.setEmail(resetPasswordUser.getEmail());
    emailResetProcess.setBody(buildEmailNotificationDtoBody(resetPasswordUser).toString());
    emailResetProcess.setOwner(resetPasswordUser.getUserId().toString());
    emailResetProcess.setHasTemplate(true);
    emailResetProcess.setType(KafkaEmailResetMessages.EMAIL_TYPE);
    log.info("Email Notification DTO built successfully");
    return emailResetProcess;
  }

  private Map<String, Object> buildEmailNotificationDtoBody(ResetPasswordUser resetPasswordUser) {
    log.info("Building Email body with code, link and expiration time");
    Map<String, Object> emailBody = new HashMap<>();
    emailBody.put(EmailConstant.RESET_PASSWORD_CODE_KEY, resetPasswordUser.getCode());
    emailBody.put(EmailConstant.RESET_PASSWORD_LINK_KEY,
        KafkaEmailResetMessages.EMAIL_LINK + resetPasswordUser.getToken());
    return emailBody;
  }
}
