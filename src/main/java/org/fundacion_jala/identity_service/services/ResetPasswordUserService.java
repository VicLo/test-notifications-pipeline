package org.fundacion_jala.identity_service.services;

import java.util.UUID;
import org.fundacion_jala.identity_service.models.security.ResetPasswordUser;

public interface ResetPasswordUserService {
  ResetPasswordUser getByToken(String token);

  ResetPasswordUser save(ResetPasswordUser resetPasswordUser);

  ResetPasswordUser invalidateTokenCode(UUID id);
}
