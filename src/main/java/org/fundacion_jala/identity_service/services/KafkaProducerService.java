package org.fundacion_jala.identity_service.services;

import org.fundacion_jala.identity_service.models.kafka.EmailNotificationDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Service
public class KafkaProducerService {

  @Value("${spring.kafka.topic.producer.name}")
  private String emailResetTopicName;

  @Autowired private KafkaTemplate<String, EmailNotificationDTO> kafkaTemplate;

  public void publishMessage(EmailNotificationDTO emailNotificationDTO) {
    kafkaTemplate.send(emailResetTopicName, emailNotificationDTO);
  }
}
