package org.fundacion_jala.identity_service.services;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.Permission;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.RolePermission;
import org.fundacion_jala.identity_service.repositories.PermissionRepository;
import org.fundacion_jala.identity_service.repositories.RolePermissionRepository;
import org.fundacion_jala.identity_service.repositories.RoleRepository;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.helpers.RolePermissionHelper;
import org.fundacion_jala.identity_service.utils.messages.MessageConstant;
import org.fundacion_jala.identity_service.utils.requests.BulkEditRolesRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class BulkEditRolesServiceImpl implements BulkEditEntityService<BulkEditRolesRequest> {

  @Autowired private RolePermissionRepository rolePermissionRepository;

  @Autowired private RoleRepository roleRepository;

  @Autowired private PermissionRepository permissionRepository;

  @Override
  @Transactional(rollbackFor = {Exception.class})
  public BasicResponse editInBulk(BulkEditRolesRequest bulkEditRolesRequest) {
    checkIfExistsIdsOfRoles(bulkEditRolesRequest.getIdsOfRoles());
    checkIfExistsIdsOfPermissions(bulkEditRolesRequest.getActions());

    for (UUID roleId : bulkEditRolesRequest.getIdsOfRoles()) {
      List<RolePermission> rolePermissions =
          RolePermissionHelper.buildRolePermissions(roleId, bulkEditRolesRequest.getActions());
      rolePermissionRepository.saveAll(rolePermissions);
    }

    BasicResponse response = new BasicResponse();
    response.setMessage(MessageConstant.BULK_EDIT_ROLES_SUCCESS);
    return response;
  }

  private void checkIfExistsIdsOfRoles(List<UUID> rolesId) {
    rolesId.forEach(
        id -> {
          if (!roleRepository.existsById(id)) {
            throw new EntityNotFoundException(Role.class.getSimpleName(), id);
          }
        });
  }

  private void checkIfExistsIdsOfPermissions(List<RolePermission> rolePermissions) {
    rolePermissions.forEach(
        rolePermission -> {
          if (!permissionRepository.existsById(rolePermission.getPermissions().getId())) {
            throw new EntityNotFoundException(
                Permission.class.getSimpleName(), rolePermission.getPermissions().getId());
          }
        });
  }
}
