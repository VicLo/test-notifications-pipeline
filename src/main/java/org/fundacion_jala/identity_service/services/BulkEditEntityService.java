package org.fundacion_jala.identity_service.services;

import org.fundacion_jala.identity_service.utils.responses.BasicResponse;

public interface BulkEditEntityService<T> {

  BasicResponse editInBulk(T data);
}
