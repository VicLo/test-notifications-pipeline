package org.fundacion_jala.identity_service.services;

import java.util.Set;
import java.util.UUID;
import org.fundacion_jala.identity_service.models.RolePermission;

public interface RolePermissionService {

  RolePermission save(UUID roleId, RolePermission rolePermission);

  void saveAll(Set<RolePermission> actions);
}
