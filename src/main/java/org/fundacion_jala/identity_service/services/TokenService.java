package org.fundacion_jala.identity_service.services;

import java.util.Map;

public interface TokenService<T> {
  String generateToken(T data);

  boolean isValidToken(String token, T data);

  Map<String, Object> getPayload(String token);
}
