package org.fundacion_jala.identity_service.services;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;
import javax.validation.Valid;
import org.fundacion_jala.identity_service.file_management.UsersFileData;
import org.fundacion_jala.identity_service.file_management.UsersFileResponse;
import org.fundacion_jala.identity_service.models.Role;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.pagination.Pagination;
import org.fundacion_jala.identity_service.repositories.UserRepository;
import org.fundacion_jala.identity_service.utils.constants.EndPoint;
import org.fundacion_jala.identity_service.utils.constants.UserFields;
import org.fundacion_jala.identity_service.utils.constants.UserFileFields;
import org.fundacion_jala.identity_service.utils.constants.UserFileMessages;
import org.fundacion_jala.identity_service.utils.exceptions.EntityNotFoundException;
import org.fundacion_jala.identity_service.utils.exceptions.FieldEntityNotLocatedException;
import org.fundacion_jala.identity_service.utils.exceptions.InvalidUserPasswordFormatException;
import org.fundacion_jala.identity_service.utils.exceptions.UnsupportedMultipleRoleException;
import org.fundacion_jala.identity_service.utils.helpers.BodyGeneratorHelper;
import org.fundacion_jala.identity_service.utils.helpers.CSVHelper;
import org.fundacion_jala.identity_service.utils.helpers.PasswordGenerator;
import org.fundacion_jala.identity_service.utils.helpers.SortHelper;
import org.fundacion_jala.identity_service.utils.helpers.XLSXHelper;
import org.fundacion_jala.identity_service.utils.speficitations.user.SpecificationUtil;
import org.fundacion_jala.identity_service.utils.validations.PasswordValidationVerifier;
import org.fundacion_jala.identity_service.utils.validations.UserValidationVerifier;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

@Service
public class UserServiceImpl implements UserService {

  private static final String TYPE_USER = "USER";
  private static final String PASSWORD_UPDATED_MESSAGE = "The Password was reset";
  private static final String PASSWORD_ERROR_MESSAGE = "Password could not be generated";

  @Autowired private UserRepository userRepository;

  @Autowired private CSVHelper csvHelper;

  @Autowired private UserValidationVerifier userValidationVerifier;

  @Autowired private PasswordGenerator passwordGenerator;

  @Autowired private PasswordEncoder passwordEncoder;

  @Autowired private RestTemplate restTemplate;

  @Autowired private EntityService<Role> roleService;

  @Value("${notifications-service.url}")
  private String notificationsServiceUrl;

  @Autowired private BodyGeneratorHelper bodyGeneratorHelper;

  @Override
  public DataAndPagination getAll(
      String criteria,
      Integer pageNumber,
      Integer pageSize,
      String sort,
      String filterOption,
      String filterValue) {
    try {
      int pageNoCorrection = pageNumber - 1;
      Specification<User> filterSpec = SpecificationUtil.getFilter(filterOption, filterValue);
      Specification<User> searchSpec = SpecificationUtil.getSearch(criteria, UserFields.FIELD_LIST);
      Pageable pageable = PageRequest.of(pageNoCorrection, pageSize, SortHelper.sortSetter(sort));
      Page<User> page =
          userRepository.findAll(Specification.where(filterSpec.and(searchSpec)), pageable);
      int totalPages = page.getTotalPages();
      int nextPageNumber = page.hasNext() ? pageNumber + 1 : totalPages;
      int previousPageNumber = page.hasPrevious() ? pageNumber - 1 : 0;
      Pagination generatedPagination =
          Pagination.builder()
              .currentPage(pageNumber)
              .pageSize(pageSize)
              .totalPages(page.getTotalPages())
              .totalCount((int) page.getTotalElements())
              .hasPreviousPage(page.hasPrevious())
              .hasNextPage(page.hasNext())
              .nextPageNumber(nextPageNumber)
              .previousPageNumber(previousPageNumber)
              .criteria(criteria)
              .filterOption(filterOption)
              .filterValue(filterValue)
              .sort(sort)
              .build();
      if (page.hasContent()) {
        return DataAndPagination.builder()
            .pagination(generatedPagination)
            .data(page.getContent())
            .build();
      }
      return DataAndPagination.builder()
          .pagination(generatedPagination)
          .data(Collections.EMPTY_LIST)
          .build();
    } catch (IllegalArgumentException illegalArgumentException) {
      throw new FieldEntityNotLocatedException(filterOption);
    }
  }

  @Override
  public User save(@Valid User entity) {
    return userRepository.save(entity);
  }

  public boolean saveAndCheck(@Valid User entity) {
    User user = save(entity);
    return userRepository.existsById(user.getId());
  }

  @Override
  public UsersFileResponse buildFiles(List<MultipartFile> files) {
    ArrayList<UsersFileData> data = new ArrayList<>();
    String error = null;
    boolean success = false;

    for (MultipartFile file : files) {
      if (csvHelper.hasCSVFormat(file)) {
        data.add(importUsersByFile(file));
        success = true;
      } else {
        error = UserFileMessages.FILE_DATA_ERROR;
      }
    }

    UsersFileResponse fileStatus =
        UsersFileResponse.builder()
            .message(success ? UserFileMessages.MESSAGE_SUCCESS : UserFileMessages.MESSAGE_ERROR)
            .error(error)
            .data(data)
            .success(success)
            .build();
    return fileStatus;
  }

  public UsersFileData importUsersByFile(MultipartFile file) {
    ArrayList<User> validData = new ArrayList<>();
    ArrayList<String> dataErrors = new ArrayList<>();
    ArrayList<String> fileErrors = new ArrayList<>();
    int userRow = 0;
    try {
      List<User> users = new LinkedList<>(csvHelper.csvToUsers(buildInputByFileType(file)));
      while (userRow < users.size()) {
        User user = users.get(userRow);
        String generatedPassword = passwordGenerator.generateRandomPassword();
        if (userValidationVerifier.validateUser(user)) {
          User validUser = buildValidUser(user, passwordEncoder.encode(generatedPassword));
          if (sendPasswordByEmail(user.getEmail(), generatedPassword) && saveAndCheck(validUser)) {
            validData.add(validUser);
          } else {
            dataErrors.add(userValidationVerifier.dataError(userRow, PASSWORD_ERROR_MESSAGE));
          }
        } else {
          dataErrors.add(userValidationVerifier.buildInvalidUserMessage(user, userRow));
        }
        userRow++;
      }

    } catch (IOException exception) {
      exception.printStackTrace();
      fileErrors.add(UserFileMessages.FILE_ERROR);
    }

    return UsersFileData.builder()
        .fileName(file.getOriginalFilename())
        .size(file.getSize())
        .fileErrors(fileErrors)
        .dataErrors(dataErrors)
        .allowedValues(userValidationVerifier.getAllowedValues(dataErrors))
        .validData(validData)
        .total(userRow)
        .totalSuccess(validData.size())
        .totalError(dataErrors.size())
        .status(
            dataErrors.isEmpty() ? UserFileMessages.STATUS_SUCCESS : UserFileMessages.STATUS_ERROR)
        .build();
  }

  private InputStream buildInputByFileType(MultipartFile file) throws IOException {
    if (UserFileFields.XLSX_TYPE.equals(file.getContentType())) {
      return XLSXHelper.xlsxToCsv(file);
    } else {
      return file.getInputStream();
    }
  }

  private User buildValidUser(User user, String generatedPassword) {
    User validUser = new User();
    Set<Role> roles = new HashSet<>();
    roles.add(roleService.getByName(user.getRoleName()));
    validUser.setId(user.getId());
    validUser.setFirstName(user.getFirstName());
    validUser.setLastName(user.getLastName());
    validUser.setCi(user.getCi());
    validUser.setIssued(user.getIssued());
    validUser.setPhoneNumber(user.getPhoneNumber());
    validUser.setCurrentCity(user.getCurrentCity());
    validUser.setCountry(user.getCountry());
    validUser.setRoles(roles);
    validUser.setEmail(user.getEmail());
    validUser.setPassword(generatedPassword);
    return validUser;
  }

  private boolean sendPasswordByEmail(String email, String password) {
    try {
      ResponseEntity<Object> response =
          restTemplate.postForEntity(
              notificationsServiceUrl + EndPoint.EMAIL,
              bodyGeneratorHelper.generateBodyEmailDefaultPassword(password, new String[] {email}),
              Object.class);
      return response.getStatusCode().is2xxSuccessful();
    } catch (IllegalStateException | RestClientException exception) {
      return false;
    }
  }

  public User getByEmail(String email) {
    User userFound = userRepository.findByEmail(email);
    if (userFound == null || !userValidationVerifier.validateEmail(email)) {
      throw new EntityNotFoundException(User.class.getSimpleName(), "email", email);
    }
    return userFound;
  }

  public String updatePassword(UUID userId, String newPassword) {
    if (!PasswordValidationVerifier.validatePassword(newPassword)) {
      throw new InvalidUserPasswordFormatException();
    }
    Optional<User> actualUser = userRepository.findById(userId);
    if (!actualUser.isPresent()) {
      throw new EntityNotFoundException("User", userId);
    }
    User user = actualUser.get();
    user.setPassword(passwordEncoder.encode(newPassword));
    userRepository.save(user);
    return PASSWORD_UPDATED_MESSAGE;
  }

  @Override
  public User update(UUID userId, User entity) {
    if (!userValidationVerifier.validateUniqueRole(entity.getRoles())) {
      throw new UnsupportedMultipleRoleException(TYPE_USER, entity.getFirstName());
    }
    Optional<User> currentUser = userRepository.findById(userId);
    if (currentUser.isPresent()) {
      User updatedUser = currentUser.get();
      updatedUser.setEmail(entity.getEmail());
      updatedUser.setFirstName(entity.getFirstName());
      updatedUser.setLastName(entity.getLastName());
      updatedUser.setCi(entity.getCi());
      updatedUser.setCurrentCity(entity.getCurrentCity());
      updatedUser.setIssued(entity.getIssued());
      updatedUser.setPhoneNumber(entity.getPhoneNumber());
      updatedUser.setRoles(entity.getRoles());
      updatedUser.setCountry(entity.getCountry());
      return userRepository.save(updatedUser);
    }
    throw new EntityNotFoundException(TYPE_USER, userId);
  }

  @Override
  public User getById(UUID userId) {
    Optional<User> userFound = userRepository.findById(userId);
    if (!userFound.isPresent()) {
      throw new EntityNotFoundException(User.class.getSimpleName(), "id", userId.toString());
    }
    return userFound.get();
  }
}
