package org.fundacion_jala.identity_service.services;

import java.util.List;
import java.util.UUID;
import org.fundacion_jala.identity_service.file_management.UsersFileResponse;
import org.fundacion_jala.identity_service.models.User;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.springframework.web.multipart.MultipartFile;

public interface UserService {

  DataAndPagination getAll(
      String criteria,
      Integer pageNumber,
      Integer pageSize,
      String sort,
      String filterOption,
      String filterValue);

  User save(User entity);

  User update(UUID id, User entity);

  User getByEmail(String email);

  UsersFileResponse buildFiles(List<MultipartFile> files);

  User getById(UUID id);
}
