package org.fundacion_jala.identity_service.services;

import org.fundacion_jala.identity_service.utils.requests.ChangeUserPasswordRequest;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.fundacion_jala.identity_service.utils.responses.CheckEmailResponse;

public interface ResetPasswordService {
  CheckEmailResponse verifyEmail(String email);

  BasicResponse resetPassword(String token, ChangeUserPasswordRequest changeUserPasswordRequest);

  BasicResponse validateToken(String token);
}
