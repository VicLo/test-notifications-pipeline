package org.fundacion_jala.identity_service.services;

import java.util.UUID;
import org.apache.commons.lang.NotImplementedException;
import org.fundacion_jala.identity_service.models.Permission;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.repositories.PermissionRepository;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PermissionServiceImpl implements EntityService<Permission> {

  @Autowired private PermissionRepository permissionRepository;

  @Override
  public DataAndPagination getAllPagination(Integer pageNo, Integer pageSize, String sortBy) {
    throw new NotImplementedException();
  }

  @Override
  public Iterable getAll() {
    return permissionRepository.findAll();
  }

  @Override
  public Permission save(Permission entity) {
    return permissionRepository.save(entity);
  }

  @Override
  public Permission update(UUID id, Permission entity) {
    throw new NotImplementedException();
  }

  @Override
  public Permission getById(UUID id) {
    throw new NotImplementedException();
  }

  @Override
  public BasicResponse delete(UUID id) {
    throw new NotImplementedException();
  }

  @Override
  public Permission getByName(String name) {
    throw new NotImplementedException();
  }
}
