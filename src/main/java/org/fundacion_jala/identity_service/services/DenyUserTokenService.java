package org.fundacion_jala.identity_service.services;

import java.util.Date;
import java.util.UUID;
import javax.transaction.Transactional;
import org.fundacion_jala.identity_service.models.security.DenyUserToken;
import org.fundacion_jala.identity_service.repositories.DenyUserTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Transactional
public class DenyUserTokenService {
  @Autowired private DenyUserTokenRepository denyUserTokenRepository;

  public boolean existsById(UUID id) {
    return denyUserTokenRepository.existsById(id);
  }

  public DenyUserToken save(DenyUserToken denyUserToken) {
    return denyUserTokenRepository.save(denyUserToken);
  }

  public void deleteAll() {
    denyUserTokenRepository.deleteAllByExpirationTimeLessThanEqual(new Date());
  }
}
