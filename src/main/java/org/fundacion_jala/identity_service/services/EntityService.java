package org.fundacion_jala.identity_service.services;

import java.util.UUID;
import org.fundacion_jala.identity_service.pagination.DataAndPagination;
import org.fundacion_jala.identity_service.utils.responses.BasicResponse;

public interface EntityService<T> {

  DataAndPagination getAllPagination(Integer pageNo, Integer pageSize, String sortBy);

  Iterable<T> getAll();

  T getById(UUID id);

  T save(T entity);

  T update(UUID id, T entity);

  BasicResponse delete(UUID id);

  T getByName(String name);
}
