package org.fundacion_jala.identity_service.file_management;

import java.util.List;
import java.util.Objects;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Builder
public class UsersFileResponse {

  private String message;
  private String error;
  private List<UsersFileData> data;
  private boolean success;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UsersFileResponse that = (UsersFileResponse) o;
    return success == that.success
        && Objects.equals(message, that.message)
        && Objects.equals(error, that.error)
        && Objects.equals(data, that.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(message, error, data, success);
  }
}
