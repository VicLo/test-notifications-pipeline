package org.fundacion_jala.identity_service.file_management;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.identity_service.models.User;

@Setter
@Getter
@Builder
public class UsersFileData {

  private String fileName;
  private long size;
  private List<String> fileErrors;
  private List<String> dataErrors;
  private Set<String> allowedValues;
  private List<User> validData;
  private int total;
  private int totalSuccess;
  private int totalError;
  private String status;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    UsersFileData that = (UsersFileData) o;
    return size == that.size && Objects.equals(fileName, that.fileName);
  }

  @Override
  public int hashCode() {
    return Objects.hash(fileName, size);
  }
}
