package org.fundacion_jala.identity_service.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;
import org.fundacion_jala.identity_service.utils.validations.RoleValidator;
import org.hibernate.annotations.*;

@Getter
@Setter
@Entity
@Table(
    name = TableConstant.TABLE_NAME_ROLES,
    uniqueConstraints = {@UniqueConstraint(columnNames = {"name"})})
@SQLDelete(sql = TableConstant.QUERY_DELETE_SOFT)
@FilterDef(
    name = TableConstant.FILTER_NAME_SOFT_DELETE,
    parameters =
        @ParamDef(name = TableConstant.COLUMN_NAME_SOFT_DELETE, type = TableConstant.TYPE_BOOLEAN))
@Filter(name = TableConstant.FILTER_NAME_SOFT_DELETE, condition = "deleted = :deleted")
public class Role {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstant.TYPE_UUID)
  private UUID id;

  @NotBlank(message = RoleValidator.BLANK_NAME)
  @Size(min = 2, max = 255, message = RoleValidator.LENGTH_NAME)
  private String name;

  @NotBlank(message = RoleValidator.BLANK_DESCRIPTION)
  @Size(min = 2, max = 255, message = RoleValidator.LENGTH_DESCRIPTION)
  private String description;

  @Column(columnDefinition = TableConstant.COLUMN_DEFINITION_DELETED)
  private boolean deleted;

  @OneToMany(mappedBy = TableConstant.TABLE_NAME_ROLES)
  @JsonManagedReference
  private Set<RolePermission> actions;

  @ManyToMany(mappedBy = "roles")
  @JsonIgnore
  private Set<User> users;

  public Role() {}

  public Role(UUID id) {
    this.id = id;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Role role = (Role) o;
    return Objects.equals(id, role.id) && Objects.equals(name, role.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }
}
