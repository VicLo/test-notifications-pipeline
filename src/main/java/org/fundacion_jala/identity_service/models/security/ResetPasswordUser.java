package org.fundacion_jala.identity_service.models.security;

import java.util.Date;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;
import org.hibernate.annotations.ColumnTransformer;
import org.hibernate.annotations.Type;

@Getter
@Setter
@Entity
public class ResetPasswordUser {
  private static final String KEY_CODE = "'key secret'";
  private static final String DECRYPT_CODE = "AES_DECRYPT(UNHEX(code), " + KEY_CODE + ")";
  private static final String ENCRYPT_CODE = "HEX(AES_ENCRYPT(?, " + KEY_CODE + "))";

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstant.TYPE_UUID)
  private UUID id;

  @Column(nullable = false, updatable = false, columnDefinition = "TEXT")
  private String token;

  @Column(nullable = false)
  @ColumnTransformer(read = DECRYPT_CODE, write = ENCRYPT_CODE)
  private String code;

  @Column(nullable = false, updatable = false)
  @Type(type = TableConstant.TYPE_UUID)
  private UUID userId;

  @Column(nullable = false)
  private String email;

  @Column(nullable = false, updatable = false)
  private Date expirationTime;

  @Column(nullable = false)
  private boolean isUsed;
}
