package org.fundacion_jala.identity_service.models.security;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;
import org.hibernate.annotations.Type;

@Getter
@Setter
@Entity
@Table(name = TableConstant.TABLE_NAME_DENY_USER_TOKENS)
public class DenyUserToken {
  @Id
  @Type(type = TableConstant.TYPE_UUID)
  private UUID jwtId;

  @Column(nullable = false)
  private Date expirationTime;

  @Column(nullable = false)
  private String tokenType;

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    DenyUserToken denyUserToken = (DenyUserToken) o;
    return Objects.equals(jwtId, denyUserToken.jwtId)
        && Objects.equals(expirationTime, denyUserToken.expirationTime);
  }

  @Override
  public int hashCode() {
    return Objects.hash(jwtId, expirationTime);
  }
}
