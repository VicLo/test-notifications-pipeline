package org.fundacion_jala.identity_service.models.kafka;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@EqualsAndHashCode
public class EmailNotificationDTO {
  private String owner;
  private String email;
  private String subject;
  private boolean hasTemplate;
  private String body;
  private String type;
}
