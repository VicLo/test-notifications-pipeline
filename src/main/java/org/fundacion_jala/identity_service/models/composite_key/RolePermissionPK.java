package org.fundacion_jala.identity_service.models.composite_key;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;
import org.hibernate.annotations.Type;

@Embeddable
public class RolePermissionPK implements Serializable {

  @Column(name = TableConstant.COLUMN_ROLE_ID)
  @Type(type = TableConstant.TYPE_UUID)
  private UUID roleId;

  @Column(name = TableConstant.COLUMN_PERMISSION_ID)
  @Type(type = TableConstant.TYPE_UUID)
  private UUID permissionId;

  public RolePermissionPK() {}

  public RolePermissionPK(UUID roleId, UUID permissionId) {
    this.roleId = roleId;
    this.permissionId = permissionId;
  }

  public UUID getRoleId() {
    return roleId;
  }

  public void setRoleId(UUID roleId) {
    this.roleId = roleId;
  }

  public UUID getPermissionId() {
    return permissionId;
  }

  public void setPermissionId(UUID permissionId) {
    this.permissionId = permissionId;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RolePermissionPK that = (RolePermissionPK) o;
    return Objects.equals(roleId, that.roleId) && Objects.equals(permissionId, that.permissionId);
  }

  @Override
  public int hashCode() {
    return Objects.hash(roleId, permissionId);
  }
}
