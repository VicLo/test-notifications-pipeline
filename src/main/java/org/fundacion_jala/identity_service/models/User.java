package org.fundacion_jala.identity_service.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import java.util.Objects;
import java.util.Set;
import java.util.UUID;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import lombok.Getter;
import lombok.Setter;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;
import org.fundacion_jala.identity_service.utils.validations.UserValidatorMessages;
import org.hibernate.annotations.Type;

@Getter
@Setter
@Entity
@Table(
    name = TableConstant.TABLE_NAME_USERS,
    uniqueConstraints = {@UniqueConstraint(columnNames = {"email"})})
public class User {

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstant.TYPE_UUID)
  private UUID id;

  @NotBlank(message = UserValidatorMessages.BLANK_FIRST_NAME)
  @Size(min = 2, max = 255, message = UserValidatorMessages.LENGTH_NAME)
  private String firstName;

  @NotBlank(message = UserValidatorMessages.BLANK_LAST_NAME)
  @Size(min = 2, max = 255, message = UserValidatorMessages.LENGTH_NAME)
  private String lastName;

  @Pattern(
      regexp = UserValidatorMessages.EMAIL_PATTERN,
      message = UserValidatorMessages.INVALID_EMAIL)
  private String email;

  @JsonIgnore private String password;

  private long ci;

  private String issued;

  private long phoneNumber;

  private String currentCity;

  private String country;

  @ManyToMany(fetch = FetchType.EAGER)
  @JoinTable(
      name = "user_role",
      joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
      inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
  private Set<Role> roles;

  @JsonIgnore
  @Transient
  private String roleName;

  public User(
      String firstName,
      String lastName,
      String email,
      long ci,
      String issued,
      long phoneNumber,
      String currentCity,
      String country,
      Set<Role> roles) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.ci = ci;
    this.issued = issued;
    this.phoneNumber = phoneNumber;
    this.currentCity = currentCity;
    this.country = country;
    this.roles = roles;
  }

  public User() {}

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    User user = (User) o;
    return ci == user.ci && Objects.equals(id, user.id) && Objects.equals(email, user.email);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, email, ci);
  }
}
