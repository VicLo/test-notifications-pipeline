package org.fundacion_jala.identity_service.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import org.springframework.stereotype.Component;

@Getter
@Setter
@Component
public class TokenPermissions {

  private static final String PROGRAMS_ALIAS = "Programs";
  private static final String LOGS_ALIAS = "logs";
  private static final String SCHOLAR_EVENTS_ALIAS = "Scholar-events";
  private static final String SCHOLAR_EVALUATION_ALIAS = "Scholar-evaluation";
  private static final String SCHOLAR_PROFILE_ALIAS = "Scholar-profile";
  private static final String SCHOLAR_STATUS_ALIAS = "Scholar-status";
  private static final String SUBJECT_ALIAS = "Subject";

  @JsonProperty(PROGRAMS_ALIAS)
  private List<String> programs;

  @JsonProperty(LOGS_ALIAS)
  private List<String> logs;

  @JsonProperty(SCHOLAR_EVENTS_ALIAS)
  private List<String> scholarEvents;

  @JsonProperty(SCHOLAR_EVALUATION_ALIAS)
  private List<String> scholarEvaluation;

  @JsonProperty(SCHOLAR_PROFILE_ALIAS)
  private List<String> scholarProfile;

  @JsonProperty(SCHOLAR_STATUS_ALIAS)
  private List<String> scholarStatus;

  @JsonProperty(SUBJECT_ALIAS)
  private List<String> subject;
}
