package org.fundacion_jala.identity_service.models;

import java.util.Objects;
import java.util.UUID;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;
import org.fundacion_jala.identity_service.utils.validations.PermissionValidator;
import org.hibernate.annotations.Type;

@Entity
@Table(name = TableConstant.TABLE_NAME_PERMISSIONS)
public class Permission {
  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Type(type = TableConstant.TYPE_UUID)
  private UUID id;

  @NotBlank(message = PermissionValidator.BLANK_NAME)
  @Size(min = 2, max = 255, message = PermissionValidator.LENGTH_NAME)
  private String name;

  @NotBlank(message = PermissionValidator.BLANK_DESCRIPTION)
  @Size(min = 2, max = 255, message = PermissionValidator.LENGTH_DESCRIPTION)
  private String description;

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public Permission() {}

  public Permission(UUID id) {
    this.id = id;
  }

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Permission that = (Permission) o;
    return Objects.equals(id, that.id) && Objects.equals(name, that.name);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, name);
  }
}
