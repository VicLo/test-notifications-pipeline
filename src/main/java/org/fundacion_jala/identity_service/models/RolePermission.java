package org.fundacion_jala.identity_service.models;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.util.Objects;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import org.fundacion_jala.identity_service.models.composite_key.RolePermissionPK;
import org.fundacion_jala.identity_service.utils.constants.TableConstant;

@Entity
public class RolePermission {
  @EmbeddedId private RolePermissionPK id;

  private boolean canView;
  private boolean canAdd;
  private boolean canEdit;
  private boolean canDelete;

  public RolePermission() {}

  public RolePermissionPK getId() {
    return id;
  }

  public void setId(RolePermissionPK id) {
    this.id = id;
  }

  public boolean getCanView() {
    return canView;
  }

  public void setCanView(boolean canRead) {
    this.canView = canRead;
  }

  public boolean getCanAdd() {
    return canAdd;
  }

  public void setCanAdd(boolean canCreate) {
    this.canAdd = canCreate;
  }

  public boolean getCanEdit() {
    return canEdit;
  }

  public void setCanEdit(boolean canEdit) {
    this.canEdit = canEdit;
  }

  public boolean getCanDelete() {
    return canDelete;
  }

  public void setCanDelete(boolean canDelete) {
    this.canDelete = canDelete;
  }

  @ManyToOne
  @MapsId(TableConstant.MAP_ROLE_ID)
  @JoinColumn(name = TableConstant.COLUMN_ROLE_ID)
  private Role roles;

  @ManyToOne
  @MapsId(TableConstant.MAP_PERMISSION_ID)
  @JoinColumn(name = TableConstant.COLUMN_PERMISSION_ID)
  private Permission permissions;

  @JsonBackReference
  public Role getRoles() {
    return roles;
  }

  public void setRoles(Role roles) {
    this.roles = roles;
  }

  public Permission getPermissions() {
    return permissions;
  }

  public void setPermissions(Permission permissions) {
    this.permissions = permissions;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    RolePermission that = (RolePermission) o;
    return Objects.equals(id, that.id)
        && Objects.equals(roles, that.roles)
        && Objects.equals(permissions, that.permissions);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id, roles, permissions);
  }
}
