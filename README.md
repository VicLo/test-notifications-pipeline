# Tracking-Tool-Applicant-Tracking

### Identity Service

This service is responsible for the authentication and the management of users, roles and
permissions

a. authentication controller

- POST http://localhost:8081/api/v1/authentication/auth
- POST http://localhost:8081/api/v1/authentication/login
- POST http://localhost:8081/api/v1/authentication/logout

b. permission-controller

- GET http://localhost:8081/api/v1/permissions
- POST http://localhost:8081/api/v1/permissions

c. role-controller

- GET http://localhost:8081/api/v1/roles
- POST http://localhost:8081/api/v1/roles
- PUT http://localhost:8081/api/v1/roles/{id}
- DELETE http://localhost:8081/api/v1/roles/{id}
- GET http://localhost:8081/api/v1/roles/{roleId}
- PUT http://localhost:8081/api/v1/roles/bulk

d. user-controller

- GET http://localhost:8081/api/v1/users
- POST http://localhost:8081/api/v1/users
- GET http://localhost:8081/api/v1/users/{input}
- PUT http://localhost:8081/api/v1/users/{userId}
- PUT http://localhost:8081/api/v1/users/change-password
- GET http://localhost:8081/api/v1/users/check-email
- GET http://localhost:8081/api/v1/users/profile

go to http://localhost:8081/swagger-ui/#/user-controller for watch the documentation of the identity
service api

### Dependencies:

- Java 8
- Maven 3.8.1/3.8.3
- MySQL Server 8.0.27
- JUnit
- Spring Framework
- Swagger2
- JavaMail
- Jwt

### Technologies involved:

- Ubuntu 20.04
- Docker

#### Databases:

- identitydb
    - tables:
        - deny_user_tokens
        - permissions
        - reset_password_user
        - role_permission
        - roles
        - user_role
        - users

The database tables are built with the models, there is a script for the db identity-service, in
the path: src\main\resources\scripts\permissions\data.sql

#### References:

- https://docs.spring.io/spring-framework/docs/3.2.x/spring-framework-reference/html/mail.html