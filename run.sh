#!/bin/sh

getPort() {
    echo $1 | cut -d : -f 3 | xargs basename
}

echo "********************************************************"
echo "Waiting for the database server to start on port $MYSQL_PORT"
echo "********************************************************"
while ! `nc -z $MYSQL_HOST $MYSQL_PORT`; do sleep 3; done
echo "******** Database Server has started "

echo "********************************************************"
echo "Starting Identity Service"
echo "********************************************************"

java -Djava.security.egd=file:/dev/./urandom -jar /application.jar